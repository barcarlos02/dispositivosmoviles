/* eslint-disable */
const argv = require('minimist')(process.argv.slice(2));
const fs = require('fs');
const ejs = require('ejs');

const getEnvFile = function(type) {
  let envFile;
  switch (type) {
    case 'prod':
      envFile = '.env.production';
      break;
    default:
      envFile = '.env';
  }
  return envFile;
};

const envFile = getEnvFile(argv.e);
require('dotenv').config({ path: `./${envFile}` });

const propertiesFile = 'android/gradle.properties';

const keystoreName = process.env.KEYSTORE_NAME;
const keystoreAlias = process.env.KEYSTORE_ALIAS;
const keystorePass = process.env.KEYSTORE_PASS;

ejs.renderFile(`${propertiesFile}.ejs`, { keystoreName, keystoreAlias, keystorePass }, {}, function (
  err,
  str
) {
  if (err) {
    return console.log('err', err);
  }
  fs.writeFile(`${propertiesFile}`, str, function(error) {
    if (error) {
      return console.log(error);
    }
  });
});

console.log('\x1b[32m', 'Gradle Properties File generated');