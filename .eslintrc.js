module.exports = {
  "extends": [
    "airbnb",
    "prettier",
    "prettier/react"
  ],
  "plugins": [
    "react",
    "jsx-a11y",
    "import",
    "prettier",
    "babel"
  ],
  "parser": "babel-eslint",
  "globals": {
    "__DEV__": true
  },
  "rules": {
    "react/jsx-filename-extension": [
      1,
      {
        "extensions": [
          ".js",
          ".jsx"
        ]
      }
    ],
    "prettier/prettier": [
      "error",
      {
        "trailingComma": "es5",
        "singleQuote": true,
        "printWidth": 100,
        "endOfLine":"auto"
      }
    ],
    "no-unused-vars": [
      1,
      {
        "ignoreSiblings": true,
        "argsIgnorePattern": "action|dispatch|state"
      }
    ],
    "no-console": 0,
    "semi": "off",
    "no-underscore-dangle": "off",
    "space-before-function-paren": "off",
    "radix": "off",
    "max-len": "off",
    "generator-star-spacing": "off",
    "prefer-default-export": "off",
    "import/prefer-default-export": "off",
    "react/jsx-no-bind": "off",
    "react/jsx-tag-spacing": "off",
    "react/jsx-closing-bracket-location": "off",
    "global-require": "off",
    "object-curly-newline": "off",
    "no-param-reassign": "off",
    "camelcase": "off",
    "import/no-named-as-default-member": "off",
    "react/destructuring-assignment": "off",
    "class-methods-use-this": "off"
  }
};
