import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  currentProfileRequest: ['data'],
  currentProfileSuccess: ['payload'],
  currentProfileFailure: null,
});

export const CurrentProfileTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
});

/* ------------- Selectors ------------- */

export const CurrentProfileSelectors = {
  getPayload: ({ currentProfile }) => currentProfile.payload,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => state.merge({ fetching: true, data });

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  return state.merge({ fetching: false, error: null, payload });
};

// Something went wrong somewhere.
export const failure = state => state.merge({ fetching: false, error: true });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CURRENT_PROFILE_REQUEST]: request,
  [Types.CURRENT_PROFILE_SUCCESS]: success,
  [Types.CURRENT_PROFILE_FAILURE]: failure,
});
