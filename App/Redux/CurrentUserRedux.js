import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  currentUserRequest: ['data'],
  currentUserSuccess: ['payload'],
  currentUserFailure: null,
  currentUserCreate: null,
});

export const CurrentUserTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  creating: null,
});

/* ------------- Selectors ------------- */

export const CurrentUserSelectors = {
  getUser: ({ currentUser }) => currentUser.payload,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => state.merge({ data });

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  return state.merge({ fetching: false, error: null, payload, creating: false });
};

// Something went wrong somewhere.
export const failure = state => state.merge({ fetching: false, error: true, creating: false });

export const creating = state => state.merge({ creating: true });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CURRENT_USER_REQUEST]: request,
  [Types.CURRENT_USER_SUCCESS]: success,
  [Types.CURRENT_USER_FAILURE]: failure,
  [Types.CURRENT_USER_CREATE]: creating,
});
