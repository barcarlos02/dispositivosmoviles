import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  markerDetailRequest: ['data'],
  markerDetailSuccess: ['payload'],
  markerDetailFailure: null,
  cleanMarkerDetail: null,
});

export const MarkerDetailTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
});

/* ------------- Selectors ------------- */

export const MarkerDetailSelectors = {
  getData: state => state.data,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => {
  return state.merge({
    fetching: true,
    data,
    payload: null,
    error: false,
  });
};

// successful api lookup
export const success = (state, { payload }) => {
  return state.merge({
    fetching: false,
    payload,
    error: false,
  });
};

// Something went wrong somewhere.
export const failure = state => {
  return state.merge({
    fetching: false,
    payload: null,
    error: true,
  });
};

export const cleanPayload = state => state.merge({ fetching: null, payload: null, error: null });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.MARKER_DETAIL_REQUEST]: request,
  [Types.MARKER_DETAIL_SUCCESS]: success,
  [Types.MARKER_DETAIL_FAILURE]: failure,
  [Types.CLEAN_MARKER_DETAIL]: cleanPayload,
});
