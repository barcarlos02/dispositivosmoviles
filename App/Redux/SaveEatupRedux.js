import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  saveEatupRequest: ['data'],
});

export const SaveEatupTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: {},
});

/* ------------- Selectors ------------- */

export const SaveEatupSelectors = {
  getData: state => state.data,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => {
  const { profileId, eatupId, type } = data;
  return state.merge({
    data: {
      ...state.data,
      [profileId]: {
        ...state.data[profileId],
        [eatupId]: data,
      },
    },
  });
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SAVE_EATUP_REQUEST]: request,
  // [Types.SAVE_EATUP_ADDPHOTO]: addPhoto,
});
