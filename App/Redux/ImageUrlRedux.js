import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  ImageUrlRequest: ['data'],
  ImageUrlSuccess: ['id', 'payload'],
  ImageUrlFailure: ['id'],
});

export const ImageUrlTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: {},
  fetching: {},
  payload: {},
  error: {},
});

/* ------------- Selectors ------------- */

export const imageUrlSelectors = {
  getData: state => state.data,
};

/* ------------- Reducers ------------- */

// request the data from an api , this will receive an url and an id.
export const request = (state, { data }) => {
  const { id } = data;
  return state.merge({
    fetching: {
      ...state.fetching,
      [id]: true,
    },
    data: {
      ...state.data,
      [id]: data,
    },
    payload: {
      ...state.payload,
      [id]: null,
    },
    error: {
      ...state.error,
      [id]: false,
    },
  });
};

// successful api lookup
export const success = (state, { id, payload }) => {
  return state.merge({
    fetching: {
      ...state.fetching,
      [id]: false,
    },
    payload: {
      ...state.payload,
      [id]: payload,
    },
    error: {
      ...state.error,
      [id]: false,
    },
  });
};

// Something went wrong somewhere.
export const failure = (state, { id }) => {
  return state.merge({
    fetching: {
      ...state.fetching,
      [id]: false,
    },
    payload: {
      ...state.payload,
      [id]: null,
    },
    error: {
      ...state.error,
      [id]: true,
    },
  });
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.IMAGE_URL_REQUEST]: request,
  [Types.IMAGE_URL_SUCCESS]: success,
  [Types.IMAGE_URL_FAILURE]: failure,
});
