import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  placesClean: null,
  placesRequest: ['data'],
  placesSuccess: ['payload'],
  placesTokenSuccess: ['token'],
  placesFailure: null,
});

export const PlacesTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: {},
  error: null,
  token: null,
});

/* ------------- Selectors ------------- */

export const PlacesSelectors = {
  getData: ({ places }) => places.data,
  getToken: ({ places }) => places.token,
  getPayload: ({ places }) => places.payload,
};

/* ------------- Reducers ------------- */

// clean results
export const clean = () => INITIAL_STATE;

// request the data from an api
export const request = (state, { data }) => state.merge({ fetching: true, data });

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  return state.merge({
    fetching: false,
    error: null,
    payload: { ...state.payload, ...payload },
  });
};

// Something went wrong somewhere.
export const failure = state => state.merge({ fetching: false, error: true });

export const tokenSuccess = (state, { token }) => state.merge({ token });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PLACES_CLEAN]: clean,
  [Types.PLACES_TOKEN_SUCCESS]: tokenSuccess,
  [Types.PLACES_REQUEST]: request,
  [Types.PLACES_SUCCESS]: success,
  [Types.PLACES_FAILURE]: failure,
});
