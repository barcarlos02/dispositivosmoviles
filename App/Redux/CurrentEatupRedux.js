import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  currentEatupRequest: ['data'],
  cleanEatup: null,
});

export const CurrentEatupTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: {},
});

/* ------------- Selectors ------------- */

export const CurrentEatupSelectors = {
  getData: state => state.data,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => {
  return state.merge({
    data: {
      ...state.data,
      ...data,
    },
  });
};

export const cleanEatup = state => {
  return state.merge({
    data: {},
  });
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CURRENT_EATUP_REQUEST]: request,
  [Types.CLEAN_EATUP]: cleanEatup,
});
