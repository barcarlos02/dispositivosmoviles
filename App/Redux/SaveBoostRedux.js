import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  saveBoostRequest: ['data'],
});

export const SaveBoostTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: {},
});

/* ------------- Selectors ------------- */

export const SaveBoostSelectors = {
  getData: state => state.data,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => {
  const { profileId, boostId } = data;
  return state.merge({
    data: {
      ...state.data,
      [profileId]: {
        ...state.data[profileId],
        [boostId]: data,
      },
    },
  });
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SAVE_BOOST_REQUEST]: request,
  // [Types.SAVE_EATUP_ADDPHOTO]: addPhoto,
});
