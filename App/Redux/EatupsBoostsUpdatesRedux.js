import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  eatupsBoostsUpdatesRequest: null,
  eatupsBoostsUpdatesSuccess: ['payload'],
  eatupsBoostsUpdatesFailure: null,
  eatupsBoostsUpdatesSeen: null,
  eatupsBoostsUpdatesClose: null,
});

export const EatupsBoostsUpdatesTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
});

/* ------------- Selectors ------------- */

export const EatupsBoostsUpdatesSelectors = {
  getData: state => state.data,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = state => state.merge({ fetching: true, payload: false });

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  return state.merge({ fetching: false, error: null, payload });
};

// Something went wrong somewhere.
export const failure = state => state.merge({ fetching: false, error: true, payload: null });

export const seen = state => state.merge({ fetching: false, error: true, payload: false });

export const closeChannel = () => INITIAL_STATE;

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.EATUPS_BOOSTS_UPDATES_REQUEST]: request,
  [Types.EATUPS_BOOSTS_UPDATES_SUCCESS]: success,
  [Types.EATUPS_BOOSTS_UPDATES_FAILURE]: failure,
  [Types.EATUPS_BOOSTS_UPDATES_SEEN]: seen,
  [Types.EATUPS_BOOSTS_UPDATES_CLOSE]: closeChannel,
});
