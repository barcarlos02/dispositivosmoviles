import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  submitProfileUser: ['data', 'resolve', 'reject'],
  submitProfileUserSuccess: ['payload'],
  submitProfileUserFailure: null,
  submitProfileBusiness: ['data', 'resolve', 'reject'],
  submitProfileBusinessSuccess: ['payload'],
  submitProfileBusinessFailure: null,
});

export const SubmitProfileTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetchingUser: null,
  fetchingBusiness: null,
  payload: null,
  error: null,
});

/* ------------- Selectors ------------- */

export const SubmitProfileSelectors = {
  getData: state => state.data,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetchingUser: true, data, payload: null });

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  return state.merge({ fetchingUser: false, error: null, payload });
};

// request the data from an api
export const requestB = (state, { data }) =>
  state.merge({ fetchingBusiness: true, data, payload: null });

// successful api lookup
export const successB = (state, action) => {
  const { payload } = action;
  return state.merge({ fetchingBusiness: false, error: null, payload });
};

// Something went wrong somewhere.
export const failure = state => state.merge({ fetching: false, error: true, payload: null });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SUBMIT_PROFILE_USER]: request,
  [Types.SUBMIT_PROFILE_USER_SUCCESS]: success,
  [Types.SUBMIT_PROFILE_USER_FAILURE]: failure,
  [Types.SUBMIT_PROFILE_BUSINESS]: requestB,
  [Types.SUBMIT_PROFILE_BUSINESS_SUCCESS]: successB,
  [Types.SUBMIT_PROFILE_BUSINESS_FAILURE]: failure,
});
