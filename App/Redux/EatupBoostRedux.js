import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  eatupBoostRequest: ['data'],
  eatupBoostSuccess: ['id', 'payload'],
  eatupBoostFailure: ['id'],
  cleanEatupBoost: null,
});

export const EatupBoostTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: {},
  fetching: {},
  payload: {},
  error: {},
});

/* ------------- Selectors ------------- */

export const EatupBoostSelectors = {
  getData: state => state.data,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => {
  const { id } = data;
  return state.merge({
    fetching: {
      ...state.fetching,
      [id]: true,
    },
    data: {
      ...state.data,
      [id]: data,
    },
    payload: {
      ...state.payload,
      [id]: null,
    },
    error: {
      ...state.error,
      [id]: false,
    },
  });
};

// successful api lookup
export const success = (state, { id, payload }) => {
  return state.merge({
    fetching: {
      ...state.fetching,
      [id]: false,
    },
    payload: {
      ...state.payload,
      [id]: payload,
    },
    error: {
      ...state.error,
      [id]: false,
    },
  });
};

// Something went wrong somewhere.
export const failure = (state, { id }) => {
  return state.merge({
    fetching: {
      ...state.fetching,
      [id]: false,
    },
    payload: {
      ...state.payload,
      [id]: null,
    },
    error: {
      ...state.error,
      [id]: true,
    },
  });
};

export const cleanPayload = state => state.merge({ fetching: {}, payload: {}, error: {} });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.EATUP_BOOST_REQUEST]: request,
  [Types.EATUP_BOOST_SUCCESS]: success,
  [Types.EATUP_BOOST_FAILURE]: failure,
  [Types.CLEAN_EATUP_BOOST]: cleanPayload,
});
