import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  markersRequest: ['data'],
  markersSuccess: ['payload'],
  markersFailure: null,
  cleanMarkers: null,
});

export const MarkersTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
});

/* ------------- Selectors ------------- */

export const MarkersSelectors = {
  getData: state => state.data,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => {
  return state.merge({
    fetching: true,
    data, // delta,longitude, latitude
    error: false,
  });
};

// successful api lookup
export const success = (state, { payload }) => {
  return state.merge({
    fetching: false,
    payload,
    error: false,
  });
};

// Something went wrong somewhere.
export const failure = state => {
  return state.merge({
    fetching: false,
    error: true,
  });
};

export const cleanPayload = state => state.merge({ fetching: null, payload: null, error: null });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.MARKERS_REQUEST]: request,
  [Types.MARKERS_SUCCESS]: success,
  [Types.MARKERS_FAILURE]: failure,
  [Types.CLEAN_MARKERS]: cleanPayload,
});
