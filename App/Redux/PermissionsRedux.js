import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  permissionsCameraRequest: null,
  permissionsCameraSuccess: null,
  permissionsCameraFailure: null,
  permissionsLocationRequest: null,
  permissionsLocationSuccess: null,
  permissionsLocationFailure: null,
});

export const PermissionsTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  checking: false,
  grantedCameraPermissions: false,
  grantedLocationPermissions: false,
});

/* ------------- Selectors ------------- */

export const PermissionsSelectors = {
  getData: state => state.data,
  hasCameraPermissions: ({ permissions }) => permissions.grantedCameraPermissions,
  hasLocationPermissions: ({ permissions }) => permissions.grantedLocationPermissions,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const requestCameraPermissions = state => state.merge({ checking: true });

export const requestCameraSuccess = state =>
  state.merge({ checking: false, grantedCameraPermissions: true });

export const requestCameraFailure = state =>
  state.merge({ checking: false, grantedCameraPermissions: false });

export const requestLocationPermissions = state => state.merge({ checking: true });

export const requestLocationSuccess = state =>
  state.merge({ checking: false, grantedLocationPermissions: true });

export const requestLocationFailure = state =>
  state.merge({ checking: false, grantedLocationPermissions: false });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PERMISSIONS_CAMERA_REQUEST]: requestCameraPermissions,
  [Types.PERMISSIONS_CAMERA_SUCCESS]: requestCameraSuccess,
  [Types.PERMISSIONS_CAMERA_FAILURE]: requestCameraFailure,
  [Types.PERMISSIONS_LOCATION_REQUEST]: requestLocationPermissions,
  [Types.PERMISSIONS_LOCATION_SUCCESS]: requestLocationSuccess,
  [Types.PERMISSIONS_LOCATION_FAILURE]: requestLocationFailure,
});
