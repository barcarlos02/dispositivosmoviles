import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  setCameraPayload: ['payload'],
  cleanCameraPayload: null,
  requestPermissions: null,
  requestPermissionsSuccess: ['payload'],
  requestPermissionsFailure: null,
});

export const CameraTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  payload: null,
  checking: false,
  grantedPermissions: false,
});

/* ------------- Selectors ------------- */

export const CameraSelectors = {
  getPayload: ({ camera }) => camera.payload,
  hasPermissions: ({ camera }) => camera.grantedPermissions,
};

/* ------------- Reducers ------------- */

// successful api lookup
export const setPayload = (state, { payload }) => state.merge({ payload });

export const clenPayload = state => state.merge({ payload: null });

export const requestPermissions = state => state.merge({ checking: true });

export const requestSuccess = state => state.merge({ checking: false, grantedPermissions: true });

export const requestFailure = state => state.merge({ checking: false, grantedPermissions: false });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_CAMERA_PAYLOAD]: setPayload,
  [Types.CLEAN_CAMERA_PAYLOAD]: clenPayload,
  [Types.REQUEST_PERMISSIONS]: requestPermissions,
  [Types.REQUEST_PERMISSIONS_SUCCESS]: requestSuccess,
  [Types.REQUEST_PERMISSIONS_FAILURE]: requestFailure,
});
