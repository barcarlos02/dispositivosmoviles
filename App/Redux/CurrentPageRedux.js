import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  setCurrentPage: ['data'],
  cleanCurrentPage: null,
});

export const CurrentPageTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  page: 0,
});

/* ------------- Selectors ------------- */

export const CurrentPageSelectors = {
  getPagingInfo: ({ page }) => page,
};

/* ------------- Reducers ------------- */

export const setPage = (state, { data }) => state.merge({ page: data });

export const cleanPage = state => state.merge({ page: 0 });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_CURRENT_PAGE]: setPage,
  [Types.CLEAN_CURRENT_PAGE]: cleanPage,
});
