import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  firstTimeSuccess: ['payload'],
  firstTimeDisable: ['payload'],
});

export const FirstTimeTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  payload: {},
  error: null,
});

/* ------------- Selectors ------------- */

export const FirstTimeSelectors = {
  getPayload: ({ firstTime }) => firstTime.payload,
};

/* ------------- Reducers ------------- */

export const success = (state, id) => {
  return state.merge({ error: null, payload: { ...state.payload, [id]: true } });
};

// Something went wrong somewhere.
export const disable = (state, id) => {
  return state.merge({ payload: { [id]: false } });
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.FIRST_TIME_SUCCESS]: success,
  [Types.FIRST_TIME_DISABLE]: disable,
});
