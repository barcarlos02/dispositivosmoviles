import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  eatupsBoostsRequest: null,
  eatupsBoostsSuccess: ['payload'],
  eatupsBoostsFailure: null,
  eatupsBoostsClose: null,
  cleanEatupsBoosts: ['payload'],
  setHasMore: null,
});

export const EatupsBoostsTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: false,
  payload: null,
  error: null,
  hasMore: null,
});

/* ------------- Selectors ------------- */

export const EatupsBoostsSelectors = {
  getPayload: ({ eatupsBoosts }) => eatupsBoosts.payload,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = state => state.merge({ fetching: true });

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  return state.merge({
    fetching: false,
    error: null,
    payload: { ...state.payload, ...payload },
    hasMore: true,
  });
};

export const cleanSuccess = (state, action) => {
  const { payload } = action;
  return state.merge({ payload: null });
};

// Something went wrong somewhere.
export const failure = state => state.merge({ fetching: false, error: true });

export const closeChannel = () => INITIAL_STATE;

export const setHasMore = state => state.merge({ hasMore: false });
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.EATUPS_BOOSTS_REQUEST]: request,
  [Types.EATUPS_BOOSTS_SUCCESS]: success,
  [Types.EATUPS_BOOSTS_FAILURE]: failure,
  [Types.EATUPS_BOOSTS_CLOSE]: closeChannel,
  [Types.CLEAN_EATUPS_BOOSTS]: cleanSuccess,
  [Types.SET_HAS_MORE]: setHasMore,
});
