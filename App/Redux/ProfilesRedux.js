import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  profilesRequest: ['data'],
  profilesSuccess: ['id', 'payload'],
  profilesFailure: ['id'],
});

export const ProfilesTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: {},
  fetching: {},
  payload: {},
  error: {},
});

/* ------------- Selectors ------------- */

export const ProfilesSelectors = {
  getPayload: ({ profiles }) => profiles.payload,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data: id }) => {
  return state.merge({
    fetching: {
      ...state.fetching,
      [id]: true,
    },
    data: {
      ...state.data,
      [id]: id,
    },
    payload: {
      ...state.payload,
      [id]: null,
    },
    error: {
      ...state.error,
      [id]: false,
    },
  });
};

// successful api lookup
export const success = (state, { id, payload }) => {
  return state.merge({
    fetching: {
      ...state.fetching,
      [id]: false,
    },
    payload: {
      ...state.payload,
      [id]: payload,
    },
    error: {
      ...state.error,
      [id]: false,
    },
  });
};

// Something went wrong somewhere.
export const failure = (state, { id }) => {
  return state.merge({
    fetching: {
      ...state.fetching,
      [id]: false,
    },
    payload: {
      ...state.payload,
      [id]: null,
    },
    error: {
      ...state.error,
      [id]: true,
    },
  });
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PROFILES_REQUEST]: request,
  [Types.PROFILES_SUCCESS]: success,
  [Types.PROFILES_FAILURE]: failure,
});
