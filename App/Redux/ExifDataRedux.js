import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  exifDataRequest: ['data'],
  exifDataSuccess: ['payload'],
  exifDataFailure: null,
  cleanPayload: null,
});

export const ExifDataTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  firstTime: true,
});

/* ------------- Selectors ------------- */

export const ExifDataSelectors = {
  getFirstTime: ({ exifData }) => exifData.firstTime, // returns if this is the first time we get the location
};

/* ------------- Reducers ------------- */

export const request = (state, { data }) => state.merge({ fetching: true, data });

export const success = (state, { payload }) => {
  return state.merge({ fetching: false, error: null, payload, firstTime: false });
};

export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null, firstTime: true });

export const cleanPayload = state => INITIAL_STATE;

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.EXIF_DATA_REQUEST]: request,
  [Types.EXIF_DATA_SUCCESS]: success,
  [Types.EXIF_DATA_FAILURE]: failure,
  [Types.CLEAN_PAYLOAD]: cleanPayload,
});
