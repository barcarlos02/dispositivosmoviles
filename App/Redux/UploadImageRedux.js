import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  uploadImageRequest: ['data'],
  uploadImageSuccess: ['payload'],
  uploadImageFailure: null,
  uploadImageProgress: ['progress'],
});

export const UploadImageTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  progress: 0,
});

/* ------------- Selectors ------------- */

export const UploadImageSelectors = {
  getPayload: ({ uploadImage }) => uploadImage.payload,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null, progress: 0 });

// successful api lookup
export const success = (state, { payload }) =>
  state.merge({ fetching: false, error: null, payload, progress: 100 });

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null, progress: 0 });

export const updateProgress = (state, { progress }) => state.merge({ progress });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.UPLOAD_IMAGE_REQUEST]: request,
  [Types.UPLOAD_IMAGE_SUCCESS]: success,
  [Types.UPLOAD_IMAGE_FAILURE]: failure,
  [Types.UPLOAD_IMAGE_PROGRESS]: updateProgress,
});
