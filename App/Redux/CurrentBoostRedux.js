import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  currentBoostRequest: ['data'],
  cleanBoost: null,
});

export const CurrentBoostTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: {},
});

/* ------------- Selectors ------------- */

export const CurrentBoostSelectors = {
  getData: state => state.data,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => {
  return state.merge({
    data: {
      ...state.data,
      ...data,
    },
  });
};

export const cleanBoost = state => {
  return state.merge({
    data: {},
  });
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CURRENT_BOOST_REQUEST]: request,
  [Types.CLEAN_BOOST]: cleanBoost,
});
