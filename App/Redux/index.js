import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import configureStore from './CreateStore';
import rootSaga from '../Sagas';
import ReduxPersist from '../Config/ReduxPersist';

/* ------------- Assemble The Reducers ------------- */
export const reducers = combineReducers({
  nav: require('./NavigationRedux').reducer,
  terms: require('./TermsRedux').reducer,
  login: require('./LoginRedux').reducer,
  loginStatus: require('./LoginStatusRedux').reducer,
  logout: require('./LogoutRedux').reducer,
  currentUser: require('./CurrentUserRedux').reducer,
  profileStatus: require('./ProfileStatusRedux').reducer,
  auth: require('./AuthRedux').reducer,
  appStatus: require('./AppStatusRedux').reducer,
  submitProfile: require('./SubmitProfileRedux').reducer,
  currentProfile: require('./CurrentProfileRedux').reducer,
  userProfiles: require('./UserProfilesRedux').reducer,
  updateCurrentProfile: require('./UpdateCurrentProfileRedux').reducer,
  camera: require('./CameraRedux').reducer,
  createEatup: require('./CreateEatupRedux').reducer,
  uploadImage: require('./UploadImageRedux').reducer,
  submitBoost: require('./SubmitBoostRedux').reducer,
  eatupBoostFilter: require('./EatupBoostFilterRedux').reducer,
  eatupsBoosts: require('./EatupsBoostsRedux').reducer,
  eatupBoost: require('./EatupBoostRedux').reducer,
  users: require('./UsersRedux').reducer,
  profiles: require('./ProfilesRedux').reducer,
  page: require('./CurrentPageRedux').reducer,
  eatupsBoostsUpdates: require('./EatupsBoostsUpdatesRedux').reducer,
  uploadLogo: require('./UploadLogoRedux').reducer,
  imageUrl: require('./ImageUrlRedux').reducer,
  firstTime: require('./FirstTimeRedux').reducer,
  permissions: require('./PermissionsRedux').reducer,
  markers: require('./MarkersRedux').reducer,
  markerDetail: require('./MarkerDetailRedux').reducer,
  saveEatup: require('./SaveEatupRedux').reducer,
  currentEatup: require('./CurrentEatupRedux').reducer,
  exifData: require('./ExifDataRedux').reducer,
  places: require('./PlacesRedux').reducer,
  saveBoost: require('./SaveBoostRedux').reducer,
  currentBoost: require('./CurrentBoostRedux').reducer,
});

export default () => {
  let finalReducers = reducers;
  // If rehydration is on use persistReducer otherwise default combineReducers
  if (ReduxPersist.active) {
    const persistConfig = ReduxPersist.storeConfig;
    finalReducers = persistReducer(persistConfig, reducers);
  }

  let { store, sagasManager, sagaMiddleware } = configureStore(finalReducers, rootSaga);

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('./').reducers;
      store.replaceReducer(nextRootReducer);

      const newYieldedSagas = require('../Sagas').default;
      sagasManager.cancel();
      sagasManager.done.then(() => {
        sagasManager = sagaMiddleware.run(newYieldedSagas);
      });
    });
  }

  return store;
};
