import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import FILTER from '../Utils/EatupBoostFilter';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  setEatupBoostFilter: ['payload'],
  setFirst: null,
});

export const EatupBoostFilterTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  payload: FILTER[0].value,
  first: true,
});

/* ------------- Selectors ------------- */

export const EatupBoostFilterSelectors = {
  getPayload: ({ eatupBoostFilter }) => eatupBoostFilter.payload,
  getFirst: ({ eatupBoostFilter }) => eatupBoostFilter.first,
};

/* ------------- Reducers ------------- */

// set current filter
export const setFilter = (state, { payload }) => state.merge({ payload });

export const setFirst = state => state.merge({ first: false });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_EATUP_BOOST_FILTER]: setFilter,
  [Types.SET_FIRST]: setFirst,
});
