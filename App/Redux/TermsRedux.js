import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  termsRequest: null,
  termsSuccess: null,
  termsFailure: null,
});

export const TermsTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  payload: null,
});

/* ------------- Selectors ------------- */

export const TermsSelectors = {
  getPayload: ({ terms }) => terms.payload,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = state => state;

// successful api lookup
export const success = state => state.merge({ payload: true });

// Something went wrong somewhere.
export const failure = state => state.merge({ payload: false });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.TERMS_REQUEST]: request,
  [Types.TERMS_SUCCESS]: success,
  [Types.TERMS_FAILURE]: failure,
});
