import { googleLogin, getAuthChannel, logout } from './FirebaseApi/Auth';
import {
  updateUserStatus,
  getUserData,
  getUserDataAuth,
  updateUserCurrentProfile,
} from './FirebaseApi/User';
import {
  createEatup,
  getEatupsBoosts,
  getEatupBoost,
  addedEatupsBoosts,
} from './FirebaseApi/Eatup';
import {
  getUserProfiles,
  getProfileData,
  addFoodie,
  addBusiness,
  updateBusiness,
} from './FirebaseApi/Profile';
import { createBoost } from './FirebaseApi/Boost';
import { getUser } from './FirebaseApi/Users';
import { getUrl } from './FirebaseApi/Image';

export default {
  // Auth
  googleLogin,
  getAuthChannel,
  logout,
  // User
  updateUserStatus,
  getUserData,
  getUserDataAuth,
  updateUserCurrentProfile,
  // Profile
  getUserProfiles,
  addFoodie,
  addBusiness,
  getProfileData,
  updateBusiness,
  // Eatup
  createEatup,
  getEatupsBoosts,
  getEatupBoost,
  addedEatupsBoosts,
  // Boost
  createBoost,
  // Users
  getUser,
  // Image
  getUrl,
};
