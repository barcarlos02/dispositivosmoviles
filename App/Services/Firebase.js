import firebase from 'react-native-firebase';

export const auth = firebase.auth();
export const firestore = firebase.firestore();
export const storage = firebase.storage();
export const database = firebase.database().ref();
export const { GoogleAuthProvider } = firebase.auth;
export const storageTasks = firebase.storage.TaskState;
export const storageEvents = firebase.storage.TaskEvent;
