import { select } from 'redux-saga/effects';
import { firestore } from '../Firebase';
import { fsAddPromise } from '../FirebaseHelpers';
import { getCurrentUser } from './Auth';
import { CurrentProfileSelectors } from '../../Redux/CurrentProfileRedux';

export function* createBoost(data) {
  try {
    const user = getCurrentUser();
    const profile = yield select(CurrentProfileSelectors.getPayload);
    const timestamp = Date.now();
    const boost = {
      ...data,
      created_by: user.uid,
      created_at: timestamp,
      geolocation: profile.address.geometry,
    };
    const boostsCollection = firestore.collection('boosts');
    const { data: id } = yield fsAddPromise(boostsCollection, boost);
    const boostsStream = firestore.collection('boostsProfiles');
    yield fsAddPromise(boostsStream, {
      boost: id,
      profile: profile.uid,
      created_at: timestamp,
      geolocation: profile.address.geometry,
    });
    return { ok: true, data: boost };
  } catch (err) {
    return { ok: false };
  }
}
