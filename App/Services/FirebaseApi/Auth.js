import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import { auth, GoogleAuthProvider } from '../Firebase';
import { authChannel } from '../FirebaseHelpers';

const getCredentials = data => {
  return new Promise(async (resolve, reject) => {
    try {
      const credential = await GoogleAuthProvider.credential(data.idToken, data.accessToken);
      const resp = await auth.signInWithCredential(credential);
      resolve(resp);
    } catch (error) {
      reject(error);
    }
  });
};

export const getAuthChannel = () => authChannel();

// Calling this function will open Google for login.
export const googleLogin = async () => {
  try {
    await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });
    const data = await GoogleSignin.signIn();
    return getCredentials(data);
  } catch (error) {
    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      // user cancelled the login flow'
      console.tron.log('login cancelled');
    } else if (error.code === statusCodes.IN_PROGRESS) {
      // operation (f.e. sign in) is in progress already
      console.tron.log('sign in in progress');
    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      // play services not available or outdated
      console.tron.log('play services not available or outdated');
    } else {
      // some other error happened
      console.tron.log('error misterio');
      console.tron.error(error);
    }
    return null;
  }
};

export const getCurrentUser = () => {
  return auth.currentUser;
};

export const logout = () => auth.signOut();
