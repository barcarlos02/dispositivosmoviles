import { storage } from '../Firebase';
import { fsUpdatePromise, fsPromise, fsSetPromise } from '../FirebaseHelpers';

export const getUrl = async data => {
  try {
    const { uri, id } = data;
    const uriesc = escape(uri);
    const storageRef = storage.ref();
    const imageRef = storageRef.child(uriesc);
    const image = await imageRef.getDownloadURL();
    // We return the url from the storage
    const url = {
      imageURL: image,
    };
    return {
      payload: url,
      ok: true,
    };
  } catch (err) {
    return { ok: false, err: [err] };
  }
};
