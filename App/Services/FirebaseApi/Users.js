import { firestore } from '../Firebase';
import { fsPromise } from '../FirebaseHelpers';

export function getUser(uid) {
  const reference = firestore.collection('users').doc(uid);
  return fsPromise(reference);
}
