import { select, put, call } from 'redux-saga/effects';
import { firestore } from '../Firebase';
import { fsAddPromise, fsPromise, fsPagingPromise, fsChannel } from '../FirebaseHelpers';
import { getCurrentUser } from './Auth';
import { CurrentProfileSelectors } from '../../Redux/CurrentProfileRedux';
import CurrentPageActions, { CurrentPageSelectors } from '../../Redux/CurrentPageRedux';
import { FILTERS_OBJECT as filters } from '../../Utils/EatupBoostFilter';

let lastPage = null;
let firstPage = null;

function* getFilterReference(filter) {
  const { uid: profile } = yield select(CurrentProfileSelectors.getPayload);
  let reference;
  switch (filter) {
    case filters.WORLD_EATUPS.value:
      reference = firestore.collection('eatupsProfiles');
      break;
    case filters.MY_EATUPS.value:
      reference = firestore.collection('eatupsProfiles').where('profile', '==', profile);
      break;
    case filters.MY_BOOSTS.value:
      reference = firestore.collection('boostsProfiles').where('profile', '==', profile);
      break;
    default:
      reference = firestore.collection('eatupsProfiles');
  }
  return reference;
}

export function* createEatup(data) {
  try {
    const user = getCurrentUser();
    const profile = yield select(CurrentProfileSelectors.getPayload);
    const timestamp = Date.now();
    const eatup = {
      ...data,
      created_by: user.uid,
      created_at: timestamp,
    };
    const eatupsCollection = firestore.collection('eatups');
    const { data: id } = yield fsAddPromise(eatupsCollection, eatup);
    const eatupsStream = firestore.collection('eatupsProfiles');
    yield fsAddPromise(eatupsStream, {
      eatup: id,
      profile: profile.uid,
      created_at: timestamp,
      geolocation: { latitude: data.geolocation.latitude, longitude: data.geolocation.longitude },
    });
    return { ok: true, data: eatup };
  } catch (err) {
    return { ok: false };
  }
}

export function* addedEatupsBoosts(filter) {
  const reference = yield getFilterReference(filter);
  return fsChannel(reference.orderBy('created_at', 'desc').endBefore(firstPage), true);
}

export function* getEatupsBoosts(filter) {
  try {
    const { page } = yield select(CurrentPageSelectors.getPagingInfo);
    const reference = yield getFilterReference(filter);
    let resp = null;
    if (page === 0) {
      resp = yield fsPagingPromise(reference.orderBy('created_at', 'desc').limit(3), true);
      firstPage = resp.firstPageItem;
    } else {
      resp = yield fsPagingPromise(
        reference
          .orderBy('created_at', 'desc')
          .startAfter(lastPage)
          .limit(3),
        true
      );
    }
    lastPage = resp.lastPageItem;
    return { ...resp, ok: true };
  } catch (e) {
    console.tron.error(e);
    return { ok: false };
  }
}

export function getEatupBoost(data) {
  let target = '';
  switch (data.type) {
    case filters.MY_BOOSTS.value:
      target = 'boosts';
      break;
    default:
      target = 'eatups';
  }
  const reference = firestore.collection(target).doc(data.id);
  return fsPromise(reference);
}
