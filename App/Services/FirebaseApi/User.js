import { firestore } from '../Firebase';
import { fsChannel, fsUpdatePromise, fsPromise, fsSetPromise } from '../FirebaseHelpers';
import { getCurrentUser } from './Auth';

export const getUserData = () => {
  const user = getCurrentUser();
  const doc = firestore.doc(`users/${user.uid}`);
  return fsPromise(doc, false);
};

export const getUserDataAuth = () => {
  const user = getCurrentUser();
  const doc = firestore.doc(`users/${user.uid}`);
  return fsChannel(doc, false);
};

export const updateUserStatus = active => {
  const user = getCurrentUser();
  if (user) {
    const doc = firestore.doc(`users/${user.uid}`);
    return fsUpdatePromise(doc, { active });
  }
  return { ok: false };
};

export const updateUserCurrentProfile = profileUid  => {
  const user = getCurrentUser();
  const doc = firestore.doc(`users/${user.uid}`)
  return fsUpdatePromise(doc, { currentProfile: profileUid });
}