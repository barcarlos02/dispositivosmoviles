import { select } from 'redux-saga/effects';
import { firestore } from '../Firebase';
import { fsUpdatePromise, fsPromise, fsSetPromise } from '../FirebaseHelpers';
import { getUserData } from './User';
import { getCurrentUser } from './Auth';
import { CurrentProfileSelectors } from '../../Redux/CurrentProfileRedux';

export const getProfileData = uid => {
  const doc = firestore.collection('profiles').doc(uid);
  return fsPromise(doc);
};

export const getUserProfiles = userUid => {
  const reference = firestore.collection('profiles').where('owner', '==', userUid);
  return fsPromise(reference, true);
};

export const addFoodie = async foodie => {
  try {
    const doc = firestore.collection('profiles').doc();
    const newFoodie = { ...foodie, uid: doc.id };
    const resp = await fsSetPromise(doc, newFoodie);
    const user = await getUserData();
    let updatedField = user.data.profiles;
    const userDoc = firestore.doc(`users/${user.data.uid}`);
    updatedField = {
      ...updatedField,
      [doc.id]: true,
    };
    return fsUpdatePromise(userDoc, {
      profiles: updatedField,
      currentProfile: doc.id,
      accountSetup: true,
    });
  } catch (error) {
    return { ok: false };
  }
};

export const addBusiness = async business => {
  try {
    const doc = firestore.collection('profiles').doc();
    const newBusiness = { ...business, uid: doc.id };
    const resp = await fsSetPromise(doc, newBusiness);
    const user = await getUserData();
    let updatedField = user.data.profiles;
    const userDoc = firestore.doc(`users/${user.data.uid}`);
    updatedField = {
      ...updatedField,
      [doc.id]: true,
    };
    return fsUpdatePromise(userDoc, {
      profiles: updatedField,
      currentProfile: doc.id,
      accountSetup: true,
    });
  } catch (error) {
    return { ok: false };
  }
};

// Receives a businnes for being updated
export function* updateBusiness(data) {
  try {
    const profile = yield select(CurrentProfileSelectors.getPayload);
    const updatedBusiness = {
      ...profile,
      imageURL: data.image,
    };
    const profileCollection = firestore.collection('profiles').doc(profile.uid);
    yield fsUpdatePromise(profileCollection, updatedBusiness);
    return { ok: true, data: updatedBusiness };
  } catch (err) {
    return { ok: false };
  }
}
