import { eventChannel } from 'redux-saga';
import _ from 'lodash';
import { auth, storage, storageTasks, storageEvents } from './Firebase';

// Utils
export const firebaseArray = snapshot => _.map(snapshot, (item, uid) => ({ ...item, uid }));

export const querySnapshotArray = querySnapshot => {
  const snapObject = _.mapKeys(querySnapshot.docs, 'id');
  return _.map(snapObject, (item, uid) => ({ ...item.data(), uid }));
};

// Firebase Firestore
export const fsPromise = (reference, list = false) =>
  new Promise((resolve, reject) => {
    reference
      .get()
      .then(querySnapshot => {
        resolve({
          ok: querySnapshot.exists,
          data: list ? querySnapshotArray(querySnapshot) : querySnapshot.data(),
        });
      })
      .catch(err => {
        reject(err);
      });
  });

export const fsPagingPromise = (reference, list = false) =>
  new Promise((resolve, reject) => {
    reference
      .get()
      .then(querySnapshot => {
        resolve({
          ok: querySnapshot.exists,
          data: list ? querySnapshotArray(querySnapshot) : querySnapshot.data(),
          firstPageItem: querySnapshot.docs[0],
          lastPageItem: querySnapshot.docs[querySnapshot.docs.length - 1],
        });
      })
      .catch(err => {
        reject(err);
      });
  });

export const fsSetPromise = (reference, data) =>
  new Promise((resolve, reject) => {
    reference
      .set(data)
      .then(_ => {
        resolve({
          ok: true,
          data,
        });
      })
      .catch(err => {
        reject(err);
      });
  });

export const fsAddPromise = (reference, data) =>
  new Promise((resolve, reject) => {
    reference
      .add(data)
      .then(docRef => {
        resolve({
          ok: true,
          data: docRef.id,
        });
      })
      .catch(err => {
        reject(err);
      });
  });

export const fsRemovePromise = reference =>
  new Promise((resolve, reject) => {
    reference
      .delete()
      .then(_ => {
        resolve({
          ok: true,
          data: null,
        });
      })
      .catch(err => {
        reject(err);
      });
  });

export const fsUpdatePromise = (reference, updates) =>
  new Promise((resolve, reject) => {
    reference
      .update(updates)
      .then(_ => {
        resolve({
          ok: true,
          data: updates,
        });
      })
      .catch(err => {
        reject(err);
      });
  });

export const authChannel = () => {
  let unsubscribe = null;
  return eventChannel(emit => {
    const iv = setInterval(() => {
      unsubscribe = auth.onAuthStateChanged(user => {
        emit({
          ok: !!user,
          data: user,
        });
      });
      clearInterval(iv);
    }, 0);
    return () => {
      unsubscribe();
      clearInterval(iv);
    };
  });
};

export const fsChannel = (reference, list = false) => {
  let unsubscribe = null;
  return eventChannel(emit => {
    const iv = setInterval(() => {
      unsubscribe = reference.onSnapshot(
        querySnapshot => {
          emit({
            ok: list ? true : querySnapshot.exists,
            data: list ? querySnapshotArray(querySnapshot) : querySnapshot.data(),
          });
        },
        err => {
          emit({
            ok: false,
            data: err,
          });
        }
      );
      clearInterval(iv);
    }, 0);
    return () => {
      unsubscribe();
      clearInterval(iv);
    };
  });
};

export const fbChannel = (reference, list = false) => {
  let listener;
  return eventChannel(emit => {
    const iv = setInterval(() => {
      listener = reference.on(
        'value',
        snapshot => {
          emit({
            ok: list ? true : snapshot.exists(),
            data: list ? firebaseArray(snapshot.val()) : snapshot.val(),
          });
        },
        err => {
          emit({
            ok: false,
            data: err,
          });
        }
      );
      clearInterval(iv);
    }, 0);
    return () => {
      reference.off('value', listener);
      clearInterval(iv);
    };
  });
};

export const fbPush = (reference, data) =>
  new Promise((resolve, reject) => {
    reference
      .push(data)
      .then(() => {
        resolve({
          ok: true,
          data,
        });
      })
      .catch(err => {
        reject(err);
      });
  });

export const fbSetPromise = (reference, data) =>
  new Promise((resolve, reject) => {
    reference
      .set(data)
      .then(() => {
        resolve({
          ok: true,
          data,
        });
      })
      .catch(err => {
        reject(err);
      });
  });

export const fbPromise = reference =>
  new Promise((resolve, reject) => {
    reference
      .once('value')
      .then(snap => {
        const value = snap.val();
        resolve({
          ok: true,
          data: value,
        });
      })
      .catch(e => {
        reject(e);
      });
  });

export const storageChannel = (path, localFilePath) => {
  let unsubscribe = null;
  return eventChannel(emit => {
    const iv = setInterval(() => {
      const storageRef = storage.ref();
      const itemRef = storageRef.child(`${path}/${Date.now()}`);
      unsubscribe = itemRef.putFile(localFilePath).on(
        storageEvents.STATE_CHANGED,
        snapshot => {
          const { totalBytes, bytesTransferred } = snapshot;
          const progress = (bytesTransferred * 100) / totalBytes;
          let filePath = '';
          if (snapshot.state === storageTasks.SUCCESS) {
            filePath = snapshot.ref.path.replace('//', '');
          }
          emit({
            ok: true,
            done: snapshot.state === storageTasks.SUCCESS,
            status: snapshot.state,
            progress,
            filePath,
          });
        },
        () => {
          emit({
            ok: false,
            done: true,
          });
        }
      );
      clearInterval(iv);
    }, 0);
    return () => {
      unsubscribe();
      clearInterval(iv);
    };
  });
};
