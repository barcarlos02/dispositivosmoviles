export default function(distance) {
  const res = distance % 1000;
  if (res > 1) {
    return { distance: Math.trunc(distance), unity: 'km' };
  }
  return { distance: Math.trunc(distance * 1000), unity: 'm' };
}
