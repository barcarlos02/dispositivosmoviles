import moment from 'moment';

export default () => {
  const hours = moment().format('LT');
  const hour = hours.split(' ');
  if (hour[1] === 'AM') return 'Breakfast';
  const time = hour[0].split(':');
  if (Number(time[0]) >= 1 && Number(time[0]) < 6 && hour[1] === 'PM') return 'Lunch';
  return 'Dinner';
};
