export default (data, field = 'uid', replace = false) => {
  return data.reduce((result, current) => {
    if (replace) {
      current = {
        ...current,
        uid: current[field],
      };
    }
    result[current[field]] = current;
    return result;
  }, {});
};
