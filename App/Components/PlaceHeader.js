import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Icon } from 'react-native-elements';
import { Text, Container, Spinner, Touchable } from './UI';
import styles from './Styles/PlaceHeaderStyle';
import { Colors } from '../Themes';

const iconSize = 20;
const getName = place => {
  const { name } = place; // getting just the first item of places
  return name;
};

const getAdress = place => {
  const { vicinity } = place;
  const adress = vicinity.split(',');
  return adress[0]; // return short adress
};

const PlaceHeader = ({ places, fetching, onPress, selectedPlace }) => {
  if (fetching || fetching === null || places.length > 0)
    return (
      <Container style={[styles.container, styles.containerSpinner]}>
        <Spinner visible color={Colors.fire} />
      </Container>
    );
  let name;
  let adress;
  if (selectedPlace !== null) {
    // When we selected a place from modal
    name = getName(selectedPlace);
    adress = getAdress(selectedPlace);
  } else {
    console.tron.log(places);
    name = getName(places[0]);
    adress = getAdress(places[0]);
  }
  return (
    <Touchable style={styles.container} onPress={onPress}>
      <Container style={styles.leftContainer}>
        <Text style={styles.place}>{`at ${name}`}</Text>
      </Container>
      <Container style={styles.rightContainer}>
        <Text style={styles.adress}>{adress}</Text>
        <Icon name="location" type="evilicon" size={iconSize} style={styles.icon} />
      </Container>
    </Touchable>
  );
};

// Prop type warnings
PlaceHeader.propTypes = {
  places: PropTypes.instanceOf(Object),
  fetching: PropTypes.bool,
  onPress: PropTypes.func,
  selectedPlace: PropTypes.instanceOf(Object),
};

// Defaults for props
PlaceHeader.defaultProps = {
  places: [],
  fetching: true,
  onPress: () => {},
  selectedPlace: null,
};

export default PlaceHeader;
