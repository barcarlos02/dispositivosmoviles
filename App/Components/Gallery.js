import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, CameraRoll, FlatList } from 'react-native';
import { Image } from 'react-native-elements';
import { isEmpty } from 'lodash';
import { Colors, Metrics } from '../Themes';
import Modal from './Modal';
import { Touchable, Spinner, Container } from './UI';
import styles from './Styles/GalleryStyle';

const numColumns = 3;

export default class Gallery extends Component {
  // Prop type warnings
  static propTypes = {
    onGalleryOpened: PropTypes.func,
    onGalleryClosed: PropTypes.func,
    onPhotoSelected: PropTypes.func.isRequired,
  };

  // Defaults for props
  static defaultProps = {
    onGalleryOpened: () => {},
    onGalleryClosed: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      photos: [],
      galleryVisible: false,
      pageInfo: {},
      selectedPhoto: null,
    };
  }

  componentDidMount() {
    this.fetchPhotos();
  }

  async getPhotos() {
    const { pageInfo, photos, selectedPhoto } = this.state;
    const nextFetch = {};
    if (pageInfo.end_cursor) {
      nextFetch.after = pageInfo.end_cursor;
    }
    if (!isEmpty(pageInfo) && !pageInfo.has_next_page) {
      this.setState({
        loading: false,
      });
      return;
    }
    const response = await CameraRoll.getPhotos({
      first: numColumns * 20,
      mimeTypes: ['image/jpeg', 'image/png'],
      ...nextFetch,
    });
    this.setState({
      photos: [...photos, ...response.edges],
      loading: false,
      pageInfo: response.page_info,
    });
  }

  fetchPhotos() {
    this.setState({ loading: true }, this.getPhotos);
  }

  openGallery() {
    this.setState(
      {
        galleryVisible: true,
      },
      this.props.onGalleryOpened
    );
  }

  closeGallery(callback) {
    this.setState(
      {
        galleryVisible: false,
      },
      () => {
        this.props.onGalleryClosed();
        if (callback) {
          callback();
        }
      }
    );
  }

  _renderPhotos() {
    const { photos, loading, selectedPhoto } = this.state;
    if (loading) {
      return <Spinner size="small" color={Colors.snow} />;
    }
    if (photos.length === 0) {
      return null;
    }
    let selected = selectedPhoto;
    if (selectedPhoto === null) {
      // eslint-disable-next-line prefer-destructuring
      selected = photos[0];
    }
    return (
      <Touchable
        onPress={() => {
          this.setState({ galleryVisible: true });
        }}
        activeOpacity={0.8}
      >
        <Image source={{ uri: selected.node.image.uri }} style={styles.thumbnail} />
      </Touchable>
    );
  }

  _renderTrigger() {
    const width = Metrics.screenWidth;
    const size = Math.round(width * 0.25);
    const name = 'Hola';
    const { photos, loading, selectedPhoto } = this.state;
    if (loading) {
      return <Spinner size="small" color={Colors.snow} />;
    }
    if (photos.length === 0) {
      return null;
    }
    let selected = selectedPhoto;
    if (selectedPhoto === null) {
      // eslint-disable-next-line prefer-destructuring
      selected = photos[0];
    }
    const icon = this.props.trigger;
    return React.cloneElement(icon, {
      ...this.props.trigger,
      onPress: this.openGallery.bind(this),
      alignSelf: 'center',
    });
  }

  _keyExtractor(item) {
    try {
      return item.node.image.uri;
    } catch (e) {
      return item.key;
    }
  }

  _renderItem({ item }) {
    const size = (Metrics.screenWidth - 20) / numColumns;
    const { selectedPhoto } = this.state;
    let isSelected = false;
    try {
      isSelected = selectedPhoto.node.image.uri === item.node.image.uri;
    } catch (e) {
      isSelected = false;
    }
    return (
      <Touchable
        style={[styles.photoItemContainer, { height: size, width: size }]}
        onPress={this.selectPhoto.bind(this, item)}
      >
        <Image
          source={{ uri: item.node.image.uri }}
          style={[styles.photoItem, { height: size, width: size - 2 }]}
        />
        {isSelected && <View style={styles.selectedMarker} />}
      </Touchable>
    );
  }

  selectPhoto(item) {
    this.closeGallery(() => {
      this.props.onPhotoSelected(item);
      this.setState({
        selectedPhoto: item,
      });
    });
  }

  _renderGallery() {
    const { galleryVisible, photos, loading } = this.state;
    return (
      <Modal
        visible={galleryVisible}
        title="Camera Roll"
        top={100}
        onClose={this.closeGallery.bind(this)}
      >
        <FlatList
          data={photos}
          style={styles.listContainer}
          renderItem={this._renderItem.bind(this)}
          keyExtractor={this._keyExtractor}
          numColumns={numColumns}
          onEndReached={this.fetchPhotos.bind(this)}
        />
        {loading && (
          <View style={styles.loadingContainer}>
            <Spinner size="small" color={Colors.secondary} />
          </View>
        )}
      </Modal>
    );
  }

  render() {
    if (this.props.trigger == null) {
      return (
        <Container style={styles.thumbnailsContainer}>
          {this._renderPhotos()}
          {this._renderGallery()}
        </Container>
      );
    }

    return (
      <Container style={styles.logo}>
        {this._renderTrigger()}
        {this._renderGallery()}
      </Container>
    );
  }
}
