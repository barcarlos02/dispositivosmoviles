import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { upperCase } from 'lodash';
import { colors } from 'react-native-elements';
import Color from 'color';
import { Text, Container } from './UI';
import styles from './Styles/CommonBannerStyle';
import Icon from './CustomIcon';
import BOOST from '../Utils/BoostTypes';
import { Colors } from '../Themes';

const getBannerColor = (type = '') => {
  let color;
  switch (type) {
    case BOOST.ethical:
      color = Color(Colors.green).fade(0.2);
      break;
    case BOOST.eatmeup:
      color = Color(Colors.brown).fade(0.2);
      break;
    case BOOST.healthy:
      color = Color(Colors.blue).fade(0.2);
      break;
    default:
      color = '';
      break;
  }
  return color;
};

const CommonBanner = ({ name, owner, amount, discount, children, date, type }) => {
  const color = getBannerColor(type);
  return (
    <Container style={[styles.infoContainer, { backgroundColor: color }]}>
      <Container style={styles.container}>
        <View style={styles.headerContainer}>
          <View style={styles.titleContainer}>
            <Text style={[styles.text, styles.name]}>{upperCase(name)}</Text>
            {!!amount && (
              <View style={styles.amountContainer}>
                <Text style={[styles.text, styles.amount]}>{`$${amount}`}</Text>
                <View style={styles.iconContainer}>
                  <Icon name="ic_offer_label_business_discount" style={styles.icon} size={30} />
                </View>
                {/* {!!discount && <Text style={[styles.text, styles.discount]}>{`${discount}%`}</Text>} */}
              </View>
            )}
          </View>
          <View style={styles.ownerContainer}>
            <Text style={[styles.text, styles.owner]}>{owner}</Text>
          </View>
          <View>
            <Text style={[styles.text, styles.date]}>Today</Text>
            {!!date && <Text style={[styles.text, styles.date]}>Today</Text>}
          </View>
        </View>

        {children && <View style={styles.descriptionContainer}>{children}</View>}
      </Container>
    </Container>
  );
};

// Prop type warnings
CommonBanner.propTypes = {
  name: PropTypes.string.isRequired,
  amount: PropTypes.string,
  discount: PropTypes.string,
  owner: PropTypes.string.isRequired,
  children: PropTypes.node,
  date: PropTypes.string,
  likes: PropTypes.number,
  type: PropTypes.string,
};

// Defaults for props
CommonBanner.defaultProps = {
  amount: null,
  discount: null,
  children: null,
  date: null,
  likes: null,
  type: null,
};

export default CommonBanner;
