import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { isArray } from 'lodash';
import { Text, Container } from './UI';
import HeaderIcon from './HeaderIcon';
import styles from './Styles/CommonHeaderStyle';

function getOptions(scene) {
  try {
    return scene.descriptor.options;
  } catch (err) {
    return {};
  }
}

const CommonHeader = ({
  left,
  center,
  right,
  children,
  scene,
  onButtonBackPress,
  ...restProps
}) => {
  const screenOptions = getOptions(scene);
  let leftLength = 0;
  let rightLength = 0;

  const getLength = buttons => {
    return React.Children.count(buttons);
  };

  const renderButtons = (buttons, side, length) => {
    let items;
    let style;
    if (length === 0) return;
    if (!isArray(buttons)) {
      items = [buttons];
      style = {};
    } else {
      items = buttons;
      style = length === 1 ? {} : styles[side];
    }
    // eslint-disable-next-line consistent-return
    return React.Children.map(items, (child, idx) => {
      return (
        // eslint-disable-next-line react/no-array-index-key
        <View key={idx} style={[styles.headerItemContent, style]}>
          {child}
        </View>
      );
    });
  };

  const goBack = () => {
    onButtonBackPress();
    restProps.navigation.pop();
  };

  const dismiss = () => {
    restProps.navigation.dismiss();
  };

  const renderRight = () => {
    const { headerRight = null } = screenOptions;
    if (!right && !headerRight) return null;
    let items;
    if (headerRight) {
      items = [headerRight];
    } else if (right) {
      items = right;
    }
    rightLength = getLength(items);
    return renderButtons(items, 'rightButtons', rightLength);
  };

  const renderLeft = () => {
    const { index, scenes = [], navigation } = restProps;
    const { headerLeft = null } = screenOptions;
    const showBack = index !== 0 && scenes.length > 1;
    const showDismiss = index === 0 && navigation.dismissable;
    const buttons = [];
    if (showBack) {
      const backButton = (
        <HeaderIcon name="ic_chevron" onPress={goBack} style={styles.navIconWrapper} />
      );
      buttons.push(backButton);
    } else if (showDismiss) {
      const dismissButton = (
        <HeaderIcon
          name="close"
          type="font-awesome"
          custom={false}
          onPress={dismiss}
          style={styles.navIconWrapper}
        />
      );
      buttons.push(dismissButton);
    }
    if (headerLeft) {
      buttons.push(headerLeft);
    } else if (left) {
      buttons.push(left);
    }
    leftLength = buttons.length;
    return renderButtons(buttons, 'leftButtons', leftLength);
  };

  const renderCenter = () => {
    if (center === null) {
      const { title } = screenOptions;
      if (title !== null || title !== '') {
        return <Text style={styles.title}>{title}</Text>;
      }
      return null;
    }
    return center;
  };

  const flex = {
    left: {
      flex: leftLength === 0 ? 1 : leftLength,
    },
    right: {
      flex: rightLength === 0 ? 1 : rightLength,
    },
  };

  return (
    <Container style={[styles.header]}>
      <View style={[styles.headerItem, styles.left, flex.left]}>{renderLeft()}</View>
      <View style={[styles.headerItem, styles.center]}>{renderCenter()}</View>
      <View style={[styles.headerItem, styles.right, flex.right]}>{renderRight()}</View>
      {children}
    </Container>
  );
};

// Prop type warnings
CommonHeader.propTypes = {
  left: PropTypes.node,
  center: PropTypes.node,
  right: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  children: PropTypes.node,
  scene: PropTypes.instanceOf(Object),
  onButtonBackPress: PropTypes.func,
};

// Defaults for props
CommonHeader.defaultProps = {
  left: null,
  center: null,
  right: null,
  children: null,
  scene: {},
  onButtonBackPress: () => {
    console.tron.log('Valor por default');
  },
};

export default CommonHeader;
