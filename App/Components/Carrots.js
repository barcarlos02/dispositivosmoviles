import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { map } from 'lodash';
import { Text, Container } from './UI';
import styles from './Styles/CarrotsStyle';
import Icon from './CustomIcon';

const MAX = 5; // max number of carrots

const renderCarrots = number => {
  const integer = Math.trunc(number);
  const decimal = number - integer;
  const carrot = [];
  for (let i = 1; i <= integer; i++) {
    carrot.push(<Icon key={i} name="ic_rating_full" size={20} style={styles.icon} />);
  }
  if (decimal >= 0.5) {
    carrot.push(<Icon name="ic_rating_half" size={20} style={styles.icon} />);
  }
  for (let i = carrot.length; i < MAX; i++) {
    carrot.push(<Icon name="ic_ratings_empty" size={20} style={styles.icon} />);
  }
  return carrot;
};

const Carrots = ({ carrots }) => {
  return <Container style={styles.container}>{renderCarrots(carrots)}</Container>;
};

// Prop type warnings
Carrots.propTypes = {
  carrots: PropTypes.number,
};

// Defaults for props
Carrots.defaultProps = {
  carrots: 0,
};

export default Carrots;
