import React from 'react';
import PropTypes from 'prop-types';
import { map } from 'lodash';
import { Image } from 'react-native-elements';
import { Container } from './UI';
import { Images, Colors } from '../Themes';
import styles from './Styles/SelectedIngredientsStyle';

const renderIngredients = ingredients => {
  return map(ingredients, (ingredient, id) => {
    return <Image key={id} source={Images.ingredients[id]} style={[styles.img]} />;
  });
};

const SelectedIngredients = ({ ingredients }) => {
  return <Container style={styles.container}>{renderIngredients(ingredients)}</Container>;
};

// Prop type warnings
SelectedIngredients.propTypes = {
  ingredients: PropTypes.instanceOf(Object),
};

// Defaults for props
SelectedIngredients.defaultProps = {
  ingredients: {},
};

export default SelectedIngredients;
