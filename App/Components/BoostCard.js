import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Text, Button, Container } from './UI';
import styles from './Styles/BoostCardStyle';
import { Colors } from '../Themes';

const BoostCard = ({ title, description, buttonTitle, color, onPressed }) => {
  return (
    <Container style={[styles.header, { backgroundColor: color }]}>
      <Text style={[styles.text, styles.title]}>{title}</Text>
      <Text style={styles.text}>{description} </Text>
      <View style={{ paddingRight: 50, paddingLeft: 50, paddingTop: 20, flex: 1 }}>
        <Button
          title={buttonTitle}
          onPress={onPressed}
          containerStyle={{
            backgroundColor: Colors.background,
          }}
          buttonStyle={{
            backgroundColor: Colors.background,
          }}
          titleStyle={styles.submitText}
        />
      </View>
    </Container>
  );
};

BoostCard.propTypes = {
  color: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  buttonTitle: PropTypes.string,
  onPressed: PropTypes.func,
};

BoostCard.defaultProps = {
  // style: {},
  color: 'red',
  title: '',
  description: '',
  buttonTitle: 'BOOST NOW',
  onPressed: () => {},
};

export default BoostCard;
