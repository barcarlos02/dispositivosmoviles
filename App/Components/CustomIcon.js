import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../Config/selection.json';

export default createIconSetFromIcoMoon(icoMoonConfig);
