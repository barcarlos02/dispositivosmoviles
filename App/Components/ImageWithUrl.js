import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'react-native-elements';

import imageUrl from '../HOCs/ImageUrl';

const Imagew = ({ id, path, imageUrl, styles }) => {
  return <Image source={imageUrl} style={styles} />;
};

// Prop type warnings
Imagew.propTypes = {
  id: PropTypes.string,
  path: PropTypes.string,
  imageUrl: PropTypes.oneOfType([PropTypes.instanceOf(Object), PropTypes.number]), // Allow us multiple types
  styles: PropTypes.instanceOf(Object),
};

// Defaults for props
Imagew.defaultProps = {
  id: '',
  path: '',
  imageUrl: {},
  styles: {},
};

export default imageUrl(Imagew, propTypes => {
  return {
    ...propTypes,
    id: propTypes.id,
    path: propTypes.path,
  };
});
