import { StyleSheet } from 'react-native';
import { Colors, Metrics, Fonts } from '../../Themes';

const height = Metrics.screenHeight;
const width = Metrics.screenWidth;
const { navBarHeight } = Metrics;

export default StyleSheet.create({
  selected: {
    backgroundColor: Colors.darkMain,
  },
  iconSelected: {
    backgroundColor: Colors.accent,
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  profileItem: {
    justifyContent: 'flex-start',
    padding: 0,
  },
  profileContainer: {
    flex: 1,
    flexDirection: 'row',
    height: navBarHeight,
  },
  profileAvatar: {
    width: 75,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 0,
  },
  text: {
    alignSelf: 'center',
    fontSize: Fonts.size.h5,
    fontFamily: Fonts.type.base,
  },
  profileText: {
    color: Colors.windowTint,
    textAlign: 'left',
  },
  filterItem: {
    flex: 1,
    justifyContent: 'center',
    padding: 0,
  },
  filterContainer: {
    flex: 1,
    // justifyContent: 'center',
    // height: navBarHeight * 2,
  },
  filterText: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.customFonts.medium,
  },
  optionsItem: {
    flex: 1,
    justifyContent: 'flex-start',
    height: 50,
  },
  optionsContainer: {
    flex: 1,
    backgroundColor: Colors.transparent,
    paddingLeft: 50,
  },
  optionsText: {
    textAlign: 'left',
    alignSelf: 'flex-start',
    color: Colors.windowTint,
    fontFamily: Fonts.customFonts.semiBold,
  },
  iconContainer: {
    backgroundColor: Colors.darkBackground,
    justifyContent: 'center',
  },
});
