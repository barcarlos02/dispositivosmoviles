import { StyleSheet } from 'react-native';
import { Colors, Fonts, Metrics } from '../../Themes';

export default StyleSheet.create({
  mainContainer: {
    margin: 2,
    height: Metrics.screenWidth * 0.9,
  },
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    borderRadius: 15,
  },
  rowContainer: {
    flexDirection: 'row',
    // backgroundColor: 'red',
  },
  row: {
    height: 80,
    borderBottomWidth: 1,
    borderBottomColor: Colors.accent,
  },
  text: {
    textAlign: 'center',
  },
  rightText: {
    textAlign: 'right',
  },
  leftText: {
    flex: 1,
    textAlign: 'left',
  },
  tinyText: {
    flex: 1,
    fontSize: Fonts.size.small,
  },
  modal: {
    margin: 10,
  },
});
