import { StyleSheet } from 'react-native';
import Color from 'color';
import { Colors, Fonts } from '../../Themes';

const size = 30;
export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 2,
    paddingTop: 0,
  },
  infoContainer: {
    alignSelf: 'stretch',
    flex: 2,
    backgroundColor: Color(Colors.brown).fade(0.2),
    backgroundColor: Color(Colors.green).fade(0.2),
    backgroundColor: Color(Colors.blue).fade(0.2),
  },
  headerContainer: {
    // justifyContent: 'center',
    alignItems: 'flex-start',
  },
  descriptionContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  ownerContainer: {
    padding: 0,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  amountContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  text: {
    color: Colors.snow,
    fontFamily: Fonts.customFonts.regular,
  },
  name: {
    fontSize: 36,
  },
  amount: {
    alignSelf: 'flex-start',
    fontSize: 15,
    fontFamily: Fonts.customFonts.bold,
    marginRight: 5,
    marginTop: 5,
  },
  discount: {
    marginLeft: 5,
    alignSelf: 'flex-start',
    fontSize: 15,
    fontFamily: Fonts.customFonts.semiBold,
    backgroundColor: Colors.main,
    color: Colors.accent,
    borderRadius: 5,
    paddingHorizontal: 5,
  },
  owner: {
    fontSize: Fonts.size.h6,
    fontFamily: Fonts.customFonts.semiBold,
  },
  date: {
    fontSize: Fonts.size.h7,
    fontFamily: Fonts.customFonts.regular,
  },
  icon: {
    color: Colors.snow,
  },
  iconContainer: {
    borderRadius: size / 2,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: size + 2,
    width: size + 2,
    backgroundColor: Colors.accent,
    borderColor: Colors.accent,
    marginRight: 5,
    marginTop: 5,
  },
});
