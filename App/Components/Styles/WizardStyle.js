import { StyleSheet } from 'react-native';
import { Colors, ApplicationStyles } from '../../Themes';

const DOT_SIZE = 15;

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {
    flex: 1,
  },
  stepContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  stepButtonContainer: {
    justifyContent: 'center',
  },
  stepButton: {
    flex: 0,
  },
  dotsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  dot: {
    width: DOT_SIZE,
    height: DOT_SIZE,
    borderRadius: DOT_SIZE,
    borderColor: Colors.accent,
    borderWidth: 1,
    backgroundColor: Colors.transparent,
    margin: 5,
  },
  dotSelected: {
    backgroundColor: Colors.accent,
  },
  stepDescription: {
    ...ApplicationStyles.screen.text,
    fontSize: 14,
    textAlign: 'center',
    marginVertical: 10,
    paddingBottom: 10,
  },
  childrenContainer: {
    flex: 1,
    marginTop: 20,
  },
  icon: {
    transform: [{ rotate: '180deg' }],
  },
});
