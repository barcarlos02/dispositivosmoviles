import { StyleSheet } from 'react-native';
import { Colors, Fonts } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.darkMain,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
    paddingVertical: 15,
    paddingHorizontal: 5,
  },
  title: {
    ...Fonts.style.normal,
    color: Colors.secondary,
    fontWeight: 'bold',
  },
});
