import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  formContainer: {
    marginVertical: 30,
  },
});
