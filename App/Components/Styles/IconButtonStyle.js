import { StyleSheet } from 'react-native';

import { Colors, Metrics, Fonts } from '../../Themes';

export default StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.transparent, // check
  },
  icon: {
    color: Colors.accent,
  },
  title: {
    marginTop: 10,
    color: Colors.main,
    fontFamily: Fonts.customFonts.semiBold,
    fontSize: Fonts.size.medium,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
});
