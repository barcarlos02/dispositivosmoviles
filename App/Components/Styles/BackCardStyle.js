import { StyleSheet } from 'react-native';
import { Fonts, ApplicationStyles, Colors } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    margin: 6,
  },
  left: {
    flex: 1,
    backgroundColor: Colors.green,
    justifyContent: 'center',
  },
  right: {
    flex: 1,
    backgroundColor: Colors.accent,
    justifyContent: 'center',
  },
  iconEdit: {
    color: Colors.darkMain,
    textAlign: 'center',
  },
  iconDelete: {
    color: 'white',
    textAlign: 'center',
  },
});
