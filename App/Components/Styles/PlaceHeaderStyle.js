import { StyleSheet } from 'react-native';
import { Colors, Fonts } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: Colors.fire,
    padding: 5,
    flexDirection: 'row',
    margin: 5,
    marginTop: 10,
  },
  containerSpinner: {
    justifyContent: 'center',
  },
  leftContainer: {
    flex: 1,
    textAlign: 'left',
  },
  rightContainer: {
    flex: 1,
    textAlign: 'right',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    marginTop: 10, // Put the text down
  },
  place: {
    fontFamily: Fonts.customFonts.light,
    fontSize: Fonts.size.h5,
    color: Colors.fire,
  },
  adress: {
    fontFamily: Fonts.customFonts.light,
    fontSize: Fonts.size.small,
    color: Colors.fire,
  },
  icon: {
    color: Colors.windowTint,
  },
});
