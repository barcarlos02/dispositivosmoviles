import { StyleSheet } from 'react-native';
import { Metrics, Colors, Fonts } from '../../Themes';

export default StyleSheet.create({
  header: {
    backgroundColor: Colors.main,
    height: Metrics.navBarHeight,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    shadowColor: Colors.panther,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  headerItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: Metrics.navBarHeight,
    flex: 1,
  },
  headerItemContent: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  center: {
    flex: 5,
    justifyContent: 'center',
  },
  right: {
    justifyContent: 'flex-end',
  },
  left: {
    justifyContent: 'flex-start',
  },
  rightButtons: {
    paddingLeft: 5,
  },
  leftButtons: {
    paddingRight: 5,
  },
  title: {
    color: Colors.accent,
    fontSize: Fonts.size.h4,
    fontFamily: Fonts.customFonts.semiBold,
  },
  navIconWrapper: {
    paddingRight: 15,
  },
});
