import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexWrap: 'nowrap',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  img: {
    resizeMode: 'contain',
    width: 40,
    height: 40,
    marginHorizontal: 5,
  },
});
