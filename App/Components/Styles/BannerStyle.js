import { StyleSheet } from 'react-native';
import { Fonts, ApplicationStyles } from '../../Themes';

export default StyleSheet.create({
  container: {
    backgroundColor: '#546966',
    flex: 1,
    paddingHorizontal: 5,
    paddingVertical: 25,
  },
  title: {
    ...ApplicationStyles.screen.text,
    textAlign: 'center',
    marginBottom: 15,
    fontSize: Fonts.size.h5,
    fontWeight: 'bold',
  },
});
