import { StyleSheet } from 'react-native';
import { Metrics, Colors, Fonts, ApplicationStyles } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 5,
  },
  formContainer: {
    marginVertical: 10,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    height: Metrics.screenHeight * 0.4,
    borderRadius: 5,
    marginBottom: 10,
  },
  mindfulnessButton: {
    flex: 1,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: Colors.fire,
    flexDirection: 'row',
    margin: 0,
    backgroundColor: Colors.background,
  },
  buttonContainer: {
    marginRight: 80,
    marginLeft: 80,
    marginBottom: 10,
  },
  buttonTitle: {
    fontFamily: Fonts.customFonts.regular,
  },
  cameraButton: {
    position: 'absolute',
    bottom: 0,
    right: Metrics.screenWidth / 2.3,
  },
  levelTitleContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  levelTitle: {
    ...ApplicationStyles.screen.text,
    fontSize: Fonts.size.h5,
    textAlign: 'center',
    marginBottom: 10,
  },
  error: {
    color: Colors.fire,
  },
});
