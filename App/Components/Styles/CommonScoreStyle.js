import { StyleSheet } from 'react-native';
import { Metrics, Colors, Fonts } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  text: {
    ...Fonts.style.normal,
    color: Colors.snow,
  },
  mindfulText: {
    fontFamily: Fonts.customFonts.light,
  },
  icon: {
    color: Colors.snow,
    marginHorizontal: 0,
  },
  likesContainer: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'flex-end',
  },
  typeContainer: {},
  carrotsContainer: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'flex-end',
  },
});
