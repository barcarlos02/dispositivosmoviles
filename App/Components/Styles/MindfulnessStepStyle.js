import { StyleSheet } from 'react-native';
import { Colors, Fonts } from '../../Themes';

export default StyleSheet.create({
  container: {
    // flex: 1,
  },
  titleContainer: {
    justifyContent: 'center',
    flexDirection: 'row',
  },
  titleSelected: {
    borderColor: Colors.fire,
    borderBottomWidth: 5,
  },
  title: {
    ...Fonts.style.normal,
    fontSize: Fonts.size.h4,
    textAlign: 'center',
  },
  description: {
    ...Fonts.style.normal,
    fontSize: Fonts.size.h5,
    color: Colors.secondary,
    textAlign: 'center',
    marginTop: 20,
  },
  childrenContainer: {
    flex: 1,
    marginTop: 20,
  },
});
