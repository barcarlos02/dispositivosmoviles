import { StyleSheet } from 'react-native';
import { ApplicationStyles, Fonts } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 5,
  },
  text: {
    ...ApplicationStyles.screen.text,
    fontSize: Fonts.size.regular,
    textAlign: 'center',
  },
  circle: {
    marginBottom: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
