import { StyleSheet } from 'react-native';
import { Metrics, Fonts, Colors } from '../../Themes';

const gridWidth = Metrics.screenWidth * 0.75;
const gridItem = gridWidth / 3;

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ingredientsContainer: {
    width: gridWidth,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  ingredientItem: {
    width: gridItem,
    height: gridItem,
    padding: 3,
  },
  ingredientItemContent: {
    flex: 1,
    alignItems: 'center',
  },
  ingredientItemImageWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ingredientItemImage: {
    width: '70%',
    height: '70%',
    resizeMode: 'contain',
  },
  ingredientItemLabel: {
    ...Fonts.style.normal,
    fontSize: Fonts.size.small,
    color: Colors.secondary,
  },
  ingredientItemTouchable: {
    flex: 1,
    width: '100%',
    backgroundColor: Colors.transparent,
    borderRadius: 10,
  },
  ingredientItemSelected: {
    backgroundColor: Colors.accent,
  },
});
