import { StyleSheet } from 'react-native';
import { ApplicationStyles, Fonts } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  levelTitleContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  levelTitle: {
    ...ApplicationStyles.screen.text,
    fontSize: Fonts.size.h5,
    textAlign: 'center',
  },
  modalContainer: {
    margin: 10,
  },
  modal: {
    padding: 0,
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
});
