import { StyleSheet } from 'react-native';
import { Colors, Metrics } from '../../Themes';

const height = Metrics.screenHeight;
const width = Metrics.screenWidth;

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  rightMenu: {
    // position: 'absolute',
    // right: 0,
    // bottom: 0,
  },
  rightMenuOptions: {
    flex: 1,
    width: (width / 3) * 2,
    marginLeft: width / 3 - 10,
    backgroundColor: Colors.main,
    padding: 0,
    paddingVertical: 15,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 5,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 5,
  },
  centerMenu: {},
  centerMenuOptions: {
    flex: 1,
    width,
    backgroundColor: Colors.main,
    padding: 0,
    paddingVertical: 10,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 5,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 5,
  },
  leftMenu: {
    maxHeight: height,
    height,
  },
  leftMenuOptions: {
    flex: 1,
    width,
    backgroundColor: Colors.main,
    padding: 0,
    paddingVertical: 15,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 5,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 5,
    maxWidth: width * 0.9,
  },
  scrollContainer: {
    maxHeight: height * 0.5,
    backgroundColor: Colors.transparent,
  },
  mainText: {
    textAlign: 'center',
    margin: 10,
  },
});

export const triggerStyles = {
  triggerOuterWrapper: {
    display: 'none',
  },
};
