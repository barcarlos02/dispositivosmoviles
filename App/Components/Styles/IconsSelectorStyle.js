import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  option: {
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 15,
  },
  iconWrapper: {
    marginBottom: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
