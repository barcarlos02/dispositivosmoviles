import { StyleSheet } from 'react-native';
import Color from 'color';
import { Colors } from '../../Themes';

const thumbSize = 60;

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  thumbnailsContainer: {
    borderRadius: 5,
    backgroundColor: Colors.windowTint,
    position: 'absolute',
    bottom: 20,
    left: 20,
    justifyContent: 'center',
    alignItems: 'center',
    width: thumbSize,
    height: thumbSize,
  },
  thumbnail: {
    width: thumbSize,
    height: thumbSize,
    borderRadius: 5,
  },
  listContainer: {
    flex: 1,
  },
  photoItemContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    position: 'relative',
  },
  selectedMarker: {
    flex: 1,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: Color(Colors.success).alpha(0.4),
  },
  photoItem: {
    flex: 1,
  },
  loadingContainer: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    flex: 1,
    justifyContent: 'center',
  },
});
