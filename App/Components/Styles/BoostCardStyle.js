import { StyleSheet } from 'react-native';
import { Colors, Metrics, Fonts } from '../../Themes';

export default StyleSheet.create({
  header: {
    textAlign: 'center',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    marginTop: 5,
    marginHorizontal: 5,
    padding: 15,
    paddingBottom: 13,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  title: {
    textAlign: 'center',
    fontFamily: Fonts.customFonts.bold,
    fontSize: Fonts.size.h6,
  },
  text: {
    padding: 5,
    color: 'white',
    fontFamily: Fonts.customFonts.light,
  },
  submit: {
    marginRight: 50,
    marginLeft: 50,
    marginBottom: 10,
    marginTop: 20,
    backgroundColor: 'white',
    /* borderRadius: 30,
    borderWidth: 1,
    borderColor: '#fff', */
    height: 30,
  },
  submitText: {
    textAlign: 'center',
    color: 'red',
    padding: 15,
    fontFamily: Fonts.customFonts.bold,
    fontSize: Fonts.size.regular,
  },
  bottom: {
    /*
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 1,
    */
  },
});
