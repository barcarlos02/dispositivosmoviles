import { StyleSheet, Platform } from 'react-native';
import { Colors, Fonts, Metrics } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.darkMain,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
    paddingVertical: 15,
    paddingHorizontal: 5,
  },
  title: {
    ...Fonts.style.normal,
    color: Colors.secondary,
    fontWeight: 'bold',
  },
  modal: {
    height: Metrics.screenHeight / 2,
    position: 'absolute',
    top: Platform.OS === 'ios' ? Metrics.navBarHeight * 2 : Metrics.navBarHeight,
    left: 0,
    right: 0,
    margin: 10,
    borderRadius: 15,
    padding: 10,
    alignContent: 'stretch',
  },
  modalHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 5,
  },
  modalTitle: {
    fontSize: Fonts.size.h4,
    fontFamily: Fonts.type.bold,
    color: Colors.secondary,
  },
  closeBtn: {
    flex: 0,
  },
});
