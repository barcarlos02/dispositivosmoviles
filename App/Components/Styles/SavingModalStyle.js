import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  modal: {
    margin: 20,
  },
  text: {
    textAlign: 'center',
  },
  buttonContainer: {
    margin: 30,
  },
});
