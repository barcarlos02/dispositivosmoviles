import { StyleSheet } from 'react-native';
import { Colors } from '../../Themes';

export default StyleSheet.create({
  container: {
    position: 'absolute',
    top: 10,
    left: 10,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.transparent,
  },
  btn: {
    backgroundColor: Colors.darkBackground,
  },
  btnTitle: {
    color: Colors.accent,
  },
});
