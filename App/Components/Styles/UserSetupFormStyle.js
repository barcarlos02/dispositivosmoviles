import { StyleSheet } from 'react-native';
import formStyles from '../Forms/Styles';

export default StyleSheet.create({
  ...formStyles,
  formContainer: {
    marginVertical: 30,
    marginBottom: 0,
    flex: 1,
  },
  formInput: {
    flex: 1,
    alignItems: 'stretch',
  },
  avatarContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 25,
  },
  avatarText: {
    paddingBottom: 5,
  },
});
