import { StyleSheet } from 'react-native';
import { Colors, Fonts } from '../../Themes';

export default StyleSheet.create({
  modalContainer: {
    margin: 0,
  },
  modalContentSheet: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    margin: 0,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    padding: 10,
  },
  modalContent: {
    padding: 10,
  },
  modalHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 5,
  },
  modalTitle: {
    fontSize: Fonts.size.h4,
    fontFamily: Fonts.type.bold,
    color: Colors.secondary,
  },
  closeBtn: {
    flex: 0,
  },
});
