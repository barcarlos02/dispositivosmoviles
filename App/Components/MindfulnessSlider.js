import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Animated, Easing } from 'react-native';
import { orderBy } from 'lodash';
import { Text, Container } from './UI';
import CircularSlider from './CircularSlider';
import { Metrics } from '../Themes';
import styles from './Styles/MindfulnessSliderStyle';

const SIZE = Metrics.screenWidth * 0.75;
export default class MindfulnessSlider extends Component {
  // Prop type warnings
  static propTypes = {
    options: PropTypes.instanceOf(Array).isRequired,
    value: PropTypes.number.isRequired,
    onSelected: PropTypes.func.isRequired,
  };

  // Defaults for props
  static defaultProps = {};

  constructor(props) {
    super(props);
    this.animatedValue = new Animated.Value(0);
    this.state = {
      currentOption: props.options.find(o => o.value === props.value),
    };
  }

  componentDidMount() {
    this.setOption(this.state.currentOption);
  }

  componentDidUpdate(prevProps) {
    if (this.props.value !== prevProps.value) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        currentOption: this.props.options.find(o => o.value === this.props.value),
      });
    }
  }

  onValueChange(value) {
    const options = this.orderOptions();
    const percentage = Math.abs((value * 100) / 360);
    const i = options.reduce((result, option, idx) => {
      const { percentage: p } = option;
      if (percentage >= p) {
        result = idx;
      }
      return result;
    }, 0);
    const currentOption = options[i];
    if (currentOption.value !== this.state.currentOption.value) {
      this.setOption(currentOption);
    }
  }

  setOption(currentOption) {
    this.setState({ currentOption }, () => {
      this.props.onSelected(currentOption);
      this.animate();
    });
  }

  getRealValue() {
    const { value } = this.props;
    const options = this.orderOptions();
    const index = options.findIndex(o => o.value === value);
    const { percentage } = options[index];
    let to = 99;
    if (index !== options.length - 1) {
      to = options[index + 1].percentage - 1;
    }
    // We get the middle point between the selected option and the next one
    const center = (to - percentage) / 2;
    return ((percentage + center) * 360) / 100;
  }

  orderOptions() {
    const { options: _options } = this.props;
    // We do this in case the options are in disordered
    return orderBy(_options, ['percentage'], ['asc']);
  }

  animate() {
    this.animatedValue.setValue(0);
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 500,
      easing: Easing.linear,
    }).start();
  }

  _renderOption() {
    const { currentOption } = this.state;
    const movement = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [-10, 0],
    });
    const aValue = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
    });
    return (
      <Animated.View
        style={{ opacity: aValue, transform: [{ translateY: movement }, { scale: aValue }] }}
      >
        <Text>{currentOption.label}</Text>
      </Animated.View>
    );
  }

  render() {
    // const { initialValue } = this.state;
    const initialValue = this.getRealValue();
    return (
      <Container style={styles.container}>
        <CircularSlider
          width={SIZE}
          height={SIZE}
          value={initialValue}
          onValueChange={this.onValueChange.bind(this)}
        >
          {this._renderOption()}
        </CircularSlider>
      </Container>
    );
  }
}
