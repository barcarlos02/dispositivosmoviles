import React from 'react';
import { View } from 'react-native';
import { Formik, Field } from 'formik';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { Text, Button } from './UI';
import { TextInput, IconsSelector } from './Forms';
import { Colors, Metrics } from '../Themes';
import Avatar from '../Containers/Avatar';

import styles from './Styles/UserSetupFormStyle';

const Width = Metrics.screenWidth;

const options = [
  { image: 'ic_heart', label: 'healthier', value: 'healthier' },
  { image: 'ic_rating_full', label: 'more sustainably', value: 'moreSustainably' },
];

const UserSetupForm = ({ actualName, imgUrl, onSubmit }) => {
  return (
    <Formik
      initialValues={{ displayName: actualName, options: [] }}
      onSubmit={(values, actions) => {
        onSubmit(values).catch(e => {
          actions.setSubmitting(false);
        });
      }}
      validationSchema={Yup.object().shape({
        displayName: Yup.string().required('You need to add your foodie name!'),
        options: Yup.array().required('You need to select at least one option!'),
      })}
      render={({ handleSubmit, isSubmitting, isValid }) => (
        <React.Fragment>
          <View style={styles.formContainer}>
            <View style={styles.formInput}>
              <Field
                component={TextInput}
                name="displayName"
                placeholder={actualName}
                title="This will be your special mindful foodie name:"
              />
            </View>
            <View style={styles.avatarContainer}>
              <Text style={[styles.avatarText, styles.title, styles.text]}>Your eatup face:</Text>
              <Avatar url={imgUrl} />
            </View>
            <View style={styles.formInput}>
              <Field
                component={IconsSelector}
                name="options"
                title="I want to eat:"
                options={options}
                size={50}
              />
            </View>
          </View>
          <Button
            title="DONE"
            titleColor={Colors.accent}
            disabled={isSubmitting}
            onPress={handleSubmit}
          />
        </React.Fragment>
      )}
    />
  );
};

UserSetupForm.propTypes = {
  actualName: PropTypes.string,
  imgUrl: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
};

UserSetupForm.defaultProps = {
  actualName: '',
  imgUrl: null,
};

export default UserSetupForm;
