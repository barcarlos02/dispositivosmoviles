import React from 'react';
import { FlatList } from 'react-native';
import PropTypes from 'prop-types';
import { Container, Content, Spinner, Marker as CustomMarker } from './UI';
import Modal from './Modal';
import CommonCard from './CommonCard';
// Styles
import stylesCard from '../Containers/Styles/EatupBoostCardStyle';
import styles from './Styles/MarkerModalStyle';

import { Colors, Metrics } from '../Themes';

const MarkerModal = ({ isVisible, dataSource, onClose, fetching, markerType }) => {
  let elements = null;
  if (dataSource) {
    const { eatups } = dataSource;
    const { boosts } = dataSource;
    elements = [].concat(eatups, boosts);
    for (let i = 0; i < elements.length; i++) {
      if (elements[i] === undefined) {
        elements.splice(i, 1);
      }
    }
  }
  const size = Metrics.screenHeight / 2;
  // if (elements) if (elements.length > 1) size = Metrics.screenHeight / 1.5;
  return (
    <Modal
      visible={isVisible}
      onClose={onClose}
      modalProps={{
        animationIn: 'slideInDown',
        propagateSwipe: true,
        animationOut: 'slideOutUp',
        animationInTiming: 100,
        animationOutTiming: 100,
        hasBackdrop: true,
        onBackdropPress: onClose,
        backdropOpacity: 0,
      }}
      title="Place Name"
      type="modal"
      style={[styles.modal, { height: size }]}
    >
      <Content>
        {!!fetching && <Spinner visible size="large" color={Colors.accent} />}
        {!!elements && (
          <FlatList
            data={elements}
            renderItem={({ item, index }) => {
              if (!item.discount)
                return (
                  <Container style={stylesCard.cardWrapper}>
                    <CommonCard
                      owner="Username"
                      name="Eatup"
                      description={item.description}
                      color={Colors.darkMain}
                      id={String(Math.random())}
                      image={item.image}
                    />
                  </Container>
                );
              return (
                <Container style={stylesCard.cardWrapper}>
                  <CommonCard
                    owner="Username"
                    name="Boost"
                    description={item.description}
                    color={Colors.darkMain}
                    id={String(Math.random())}
                    image={item.image}
                    amount={String(item.amount)}
                    discount={String(item.discount)}
                  />
                </Container>
              );
            }}
            horizontal={false}
            onEndThreshold={0}
            keyExtractor={item => ''.concat(Math.random())}
          />
        )}
      </Content>
      <CustomMarker
        marker={markerType}
        position={{
          position: 'absolute',
          top: Metrics.screenHeight / 2 - Metrics.navBarHeight / 2,
          left: Metrics.screenWidth / 2 - Metrics.navBarHeight * 0.8,
        }}
      />
    </Modal>
  );
};

// Prop type warnings
MarkerModal.propTypes = {
  isVisible: PropTypes.bool,
  dataSource: PropTypes.instanceOf(Object),
  onClose: PropTypes.func,
  fetching: PropTypes.bool,
  markerType: PropTypes.instanceOf(Object),
};

// Defaults for props
MarkerModal.defaultProps = {
  dataSource: {},
  isVisible: false,
  onClose: () => {},
  fetching: false,
  markerType: { type: 'boost' },
};

export default MarkerModal;
