import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { Container } from './UI';
import { Colors, Metrics } from '../Themes';
import Wizard from './Wizard';
import MindfulnessIngredients from './MindfulnessIngredients';
import MindfulnessSlider from './MindfulnessSlider';
import styles from './Styles/MindfulnessWizardStyle';
import CurrentEatupActions from '../Redux/CurrentEatupRedux';

const sourceOptions = [
  { value: 1, label: 'Source Option 1', percentage: 15 },
  { value: 2, label: 'Source Option 2', percentage: 25 },
  { value: 3, label: 'Source Option 3', percentage: 50 },
  { value: 4, label: 'Source Option 4', percentage: 75 },
];
const wasteOptions = [
  { value: 1, label: 'Waste Option 1', percentage: 0 },
  { value: 2, label: 'Waste Option 2', percentage: 25 },
  { value: 3, label: 'Waste Option 3', percentage: 50 },
  { value: 4, label: 'Waste Option 4', percentage: 75 },
];

class MindfulnessWizard extends Component {
  // Prop type warnings
  static propTypes = {
    editable: PropTypes.bool,
    initialValues: PropTypes.instanceOf(Object).isRequired,
    onOpen: PropTypes.func,
    onDone: PropTypes.func,
  };

  // Defaults for props
  static defaultProps = {
    editable: true,
    onOpen: () => {},
    onDone: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      ingredients: props.initialValues.ingredients || {},
      source: props.initialValues.source || sourceOptions[0].value,
      waste: props.initialValues.waste || wasteOptions[0].value,
      currentCategory: 0,
    };
  }

  componentWillUnmount() {
    const { saveMindfulnessData } = this.props;
    const { source, waste, ingredients } = this.state;
    saveMindfulnessData({ ingredients, source, waste });
  }

  onIngredientsSelected(ingredients) {
    this.setState({ ingredients });
  }

  onSourceSelected({ value }) {
    this.setState({ source: value });
  }

  onWasteSelected({ value }) {
    this.setState({ waste: value });
  }

  openWizard() {
    this.setState({ visible: true }, this.props.onOpen);
  }

  closeWizard() {
    this.setState({ visible: false }, () => {
      const { ingredients, source, waste } = this.state;
      this.props.onClose({ ingredients, source, waste });
    });
  }

  onStepChanged(step) {
    this.setState({ currentCategory: step });
  }

  render() {
    const { editable } = this.props;
    const { visible, ingredients, source, waste, currentCategory } = this.state;
    const steps = [
      {
        component: (
          <MindfulnessIngredients
            onSelected={this.onIngredientsSelected.bind(this)}
            ingredients={ingredients}
          />
        ),
        description:
          'This is list is organised by avergage environmental impact, which may correlate with wellbeing. The higher the row, the better!',
      },
      {
        component: (
          <MindfulnessSlider
            value={source}
            options={sourceOptions}
            onSelected={this.onSourceSelected.bind(this)}
          />
        ),
        description:
          'Simple rule: the easier it is to track the sources, the better it is for your health and for the world!',
      },
    ];
    return (
      <Container style={styles.container}>
        <View style={{ height: Metrics.screenHeight * 0.7 }}>
          <Wizard
            steps={steps}
            onStepChanged={this.onStepChanged.bind(this)}
            currentStep={currentCategory}
            onDone={this.props.onDone}
          />
        </View>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    saveMindfulnessData: data => dispatch(CurrentEatupActions.currentEatupRequest(data)),
  };
};

export default connect(
  null,
  mapDispatchToProps
)(MindfulnessWizard);
