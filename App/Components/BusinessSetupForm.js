import React from 'react';
import { View } from 'react-native';
import { Formik, Field } from 'formik';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { Button, Container } from './UI';
import { TextInput, IconsSelector, AddressInput } from './Forms';

import styles from './Styles/BusinessSetupFormStyle';

const options = [
  { image: 'ic_logo_eatups', label: 'takeaway', value: 'takeaway' },
  { image: 'ic_logo_eatups', label: 'eat in', value: 'eatIn' },
];

const BusinessSetupForm = ({ onSubmit }) => {
  return (
    <Formik
      initialValues={{ name: '', address: '', options: [] }}
      onSubmit={(values, actions) => {
        onSubmit(values).catch(e => {
          actions.setSubmitting(false);
        });
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string().required('You need to add your business name!'),
        address: Yup.string().required('You need to add your business address!'),
        options: Yup.array().required('You need to select at least one option!'),
      })}
      render={({ handleSubmit, isSubmitting, isValid }) => (
        <React.Fragment>
          <Container style={styles.formContainer}>
            <Field
              component={TextInput}
              name="name"
              placeholder="Business name"
              hint="This is how people will recognize you"
            />
            <Field
              component={AddressInput}
              name="address"
              placeholder="Business address"
              hint="So people can find you nice and easy"
            />
            <Field
              component={IconsSelector}
              name="options"
              title="Eating options:"
              options={options}
            />
          </Container>
          <Button title="DONE" disabled={isSubmitting} onPress={handleSubmit} />
        </React.Fragment>
      )}
    />
  );
};

BusinessSetupForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default BusinessSetupForm;
