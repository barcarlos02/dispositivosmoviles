import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import styles from './Styles/ContainerStyle';

const Container = ({ children, style, ...rest }) => {
  return (
    <View {...rest} style={[styles.container, style]}>
      {children}
    </View>
  );
};

// Prop type warnings
Container.propTypes = {
  children: PropTypes.node.isRequired,
  style: PropTypes.instanceOf(Object),
};

// Defaults for props
Container.defaultProps = {
  style: {},
};

export default Container;
