import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import Icon from '../CustomIcon';
import styles from './Styles/MarkerStyle';
import { Metrics, Colors } from '../../Themes';

const markerSize = Metrics.navBarHeight * 0.9;
const iconSize = markerSize * 0.7;

export default class Marker extends Component {
  // // Prop type warnings
  static propTypes = {
    marker: PropTypes.instanceOf(Object).isRequired,
    position: PropTypes.instanceOf(Object),
  };

  // // Defaults for props
  static defaultProps = {
    position: {},
  };

  getMarkerColor() {
    const { marker } = this.props;
    return marker.type === 'eatup' ? Colors.main : Colors.boostMarker;
  }

  getMarkerBackgroundColor() {
    const { marker } = this.props;
    return marker.type === 'eatup' ? Colors.accent : Colors.boostMarkerBackground;
  }

  getBorderColor() {
    const { marker } = this.props;
    return marker.type === 'eatup' ? Colors.accent : Colors.boostMarkerBackground;
  }

  getIconColor() {
    const { marker } = this.props;
    return marker.type === 'eatup' ? Colors.accent : Colors.main;
  }

  getIcon() {
    const { marker } = this.props;
    if (marker.type === 'eatups') {
      return 'ic_logo_eatups';
    }
    if (!!marker.options && marker.options.takeout) {
      return 'ic_rating_full';
    }
    if (!!marker.options && marker.options.eatIn) {
      return 'ic_ratings_empty';
    }
    return 'ic_logo_eatups';
  }

  render() {
    const { position } = this.props;
    return (
      <View style={[styles.markerContainer, position]}>
        <View style={styles.background}>
          <Icon
            name="ic_map_pointer_background"
            size={markerSize}
            style={[
              {
                color: this.getMarkerBackgroundColor(),
              },
            ]}
          />
        </View>
        <View
          style={[
            styles.iconContainer,
            { backgroundColor: this.getMarkerColor(), borderColor: this.getBorderColor() },
          ]}
        >
          <Icon name={this.getIcon()} size={iconSize} style={{ color: this.getIconColor() }} />
        </View>
      </View>
    );
  }

  //   render() {
  //     return (
  //       <View
  //         style={[
  //           styles.container,
  //           {
  //             height: markerSize,
  //             width: markerSize,
  //             borderRadius: markerSize / 2,
  //             backgroundColor: this.getMarkerColor(),
  //             borderColor: this.getBorderColor(),
  //           },
  //         ]}
  //       >
  //         <Icon name={this.getIcon()} size={iconSize} style={{ color: this.getIconColor() }} />
  //       </View>
  //     );
  //   }
}
