import React from 'react';
import PropTypes from 'prop-types';
import { Button as RNE_Button } from 'react-native-elements';
import { values } from 'lodash';
import { Colors } from '../../Themes';
import RNE_Theme from '../../Themes/RNE_Theme';
import styles from './Styles/ButtonStyle';

const VARIANTS = {
  transparent: 'transparent',
  success: 'success',
  danger: 'danger',
  warning: 'warning',
  info: 'info',
  primary: 'primary',
  secondary: 'secondary',
};

const getColor = variant => {
  switch (variant) {
    case VARIANTS.transparent:
      return Colors.transparent;
    case VARIANTS.success:
      return Colors.success;
    case VARIANTS.danger:
      return Colors.error;
    case VARIANTS.warning:
      return Colors.warning;
    case VARIANTS.info:
      return Colors.secondary;
    case VARIANTS.primary:
      return Colors.fire;
    case VARIANTS.secondary:
      return Colors.main;
    default:
      return Colors.main;
  }
};

const getStyle = (textColor, variant, style, rest) => {
  const { type } = rest;
  const baseStyles = RNE_Theme.Button;
  let color = getColor(variant);
  let outlineStyle = {};
  if (type === 'clear') {
    color = Colors.transparent;
  }
  if (type === 'outline') {
    outlineStyle = {
      backgroundColor: Colors.background,
      borderWidth: 1,
      borderColor: color,
    };
  }
  let buttonStyle = {
    ...baseStyles.buttonStyle,
    backgroundColor: color,
    ...style,
    ...outlineStyle,
  };
  let containerStyle = { ...baseStyles.containerStyle };
  let titleStyle = { ...baseStyles.titleStyle, color: textColor };
  let loadingProps = { ...baseStyles.loadingProps };
  if (rest.buttonStyle) {
    buttonStyle = { ...buttonStyle, ...rest.buttonStyle };
  }
  if (rest.containerStyle) {
    containerStyle = { ...containerStyle, ...rest.containerStyle };
  }
  if (rest.titleStyle) {
    titleStyle = { ...titleStyle, ...rest.titleStyle };
  }
  if (rest.loadingProps) {
    loadingProps = { ...loadingProps, ...rest.loadingProps };
  }
  return {
    ...baseStyles,
    ...rest,
    buttonStyle,
    containerStyle,
    titleStyle,
    loadingProps,
  };
};

const Button = ({ onPress, title, variant, textColor, style, upperCase, ...rest }) => {
  const button = getStyle(textColor, variant, style, rest);
  return (
    // eslint-disable-next-line react/jsx-pascal-case
    <RNE_Button title={upperCase ? title.toUpperCase() : title} onPress={onPress} {...button} />
  );
};

// Prop type warnings
Button.propTypes = {
  onPress: PropTypes.func.isRequired,
  title: PropTypes.string,
  variant: PropTypes.oneOf(values(VARIANTS)),
  textColor: PropTypes.string,
  style: PropTypes.instanceOf(Object),
  upperCase: PropTypes.bool,
};

// Defaults for props
Button.defaultProps = {
  title: '',
  variant: VARIANTS.primary,
  textColor: Colors.background,
  style: {},
  upperCase: false,
};

export default Button;
