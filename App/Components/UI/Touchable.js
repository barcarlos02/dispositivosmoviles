import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, TouchableNativeFeedback, Platform, View } from 'react-native';
import { values } from 'lodash';
import styles from './Styles/TouchableStyle';

const TYPES = {
  native: 'native',
  opacity: 'opacity',
};

const TouchableComponent = Platform.OS === 'ios' ? TouchableOpacity : TouchableNativeFeedback;

const Touchable = ({ children, style, onPress, disabled, type, ...rest }) => {
  const handleOnPress = () => {
    if (!disabled) {
      onPress();
    }
  };
  const Touch = type === TYPES.native ? TouchableComponent : TouchableOpacity;

  const touchProps = {
    ...rest,
    onPress: handleOnPress,
    ...Platform.select({
      ios: {
        activeOpacity: 0.7,
        style: [styles.button, style],
      },
      android: {},
    }),
  };

  const realProps =
    type === TYPES.opacity
      ? {
          activeOpacity: 0.7,
          ...touchProps,
        }
      : touchProps;

  return (
    <Touch {...realProps}>
      {Platform.OS === 'ios' ? children : <View style={[styles.button, style]}>{children}</View>}
    </Touch>
  );
};

// Prop type warnings
Touchable.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  onPress: PropTypes.func.isRequired,
  style: PropTypes.instanceOf(Object),
  disabled: PropTypes.bool,
  type: PropTypes.oneOf(values(TYPES)),
};

// Defaults for props
Touchable.defaultProps = {
  children: null,
  style: {},
  disabled: false,
  type: TYPES.native,
};

export default Touchable;
