import { StyleSheet } from 'react-native';
import { ApplicationStyles } from '../../../Themes';

export default StyleSheet.create({
  container: {
    ...ApplicationStyles.screen.mainContainer,
  },
  contentPadded: {
    ...ApplicationStyles.screen.contentPadded,
  },
});
