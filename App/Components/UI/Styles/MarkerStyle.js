import { StyleSheet } from 'react-native';
import { Metrics, Colors } from '../../../Themes';

const size = Metrics.navBarHeight * 0.9;
const iconSize = size * 0.88;

export default StyleSheet.create({
  markerContainer: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: size,
    width: size,
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    alignItems: 'center',
    justifyContent: 'center',
    height: size,
    width: size,
  },
  iconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: iconSize,
    width: iconSize,
    borderRadius: iconSize / 2,
    borderWidth: 1,
    borderColor: Colors.main,
  },
});
