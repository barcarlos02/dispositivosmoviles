import React from 'react';
import PropTypes from 'prop-types';
import { ActivityIndicator } from 'react-native';
import styles from './Styles/SpinnerStyle';
import { Colors } from '../../Themes';

const Spinner = ({ color, size, style, visible }) => {
  if (!visible) {
    return null;
  }
  return <ActivityIndicator color={color} size={size} style={style} />;
};

// Prop type warnings
Spinner.propTypes = {
  color: PropTypes.string,
  size: PropTypes.string,
  style: PropTypes.instanceOf(Object),
  visible: PropTypes.bool,
};

// Defaults for props
Spinner.defaultProps = {
  color: Colors.main,
  size: 'small',
  style: {},
  visible: true,
};

export default Spinner;
