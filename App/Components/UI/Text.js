import React from 'react';
import PropTypes from 'prop-types';
import { Text as RNText } from 'react-native';
import styles from './Styles/TextStyle';
import Fonts from '../../Themes/Fonts';

const sizes = Object.keys(Fonts.size);

const Text = ({ children, style, size, ...rest }) => {
  const fontSize = Fonts.size[size];
  return (
    <RNText {...rest} style={[styles.text, { fontSize }, style]}>
      {children}
    </RNText>
  );
};

// Prop type warnings
Text.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.node]).isRequired,
  style: PropTypes.instanceOf(Object),
  size: PropTypes.oneOf(sizes),
};

// Defaults for props
Text.defaultProps = {
  style: {},
  size: 'regular',
};

export default Text;
