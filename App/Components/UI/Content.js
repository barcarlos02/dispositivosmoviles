import React from 'react';
import PropTypes from 'prop-types';
import { ScrollView } from 'react-native';
import styles from './Styles/ContentStyle';

const Content = ({ children, style, padding, ...rest }) => {
  return (
    <ScrollView {...rest} style={[styles.container, padding && styles.contentPadded, style]}>
      {children}
    </ScrollView>
  );
};

// Prop type warnings
Content.propTypes = {
  children: PropTypes.node.isRequired,
  style: PropTypes.instanceOf(Object),
  padding: PropTypes.bool,
};

// Defaults for props
Content.defaultProps = {
  style: {},
  padding: false,
};

export default Content;
