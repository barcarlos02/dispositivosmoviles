import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { BackHandler } from 'react-native';
import Camera from '../Containers/Camera';

import styles from './Styles/PhotoScreenStyle';
import { Container } from './UI';

export default class PhotoScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.goBack.bind(this);
  }

  componentDidMount() {
    this.handler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.handleBackButtonClick();
      return true;
    });
  }

  componentWillUnmount() {
    this.handler.remove();
  }

  onPictureTaken() {
    const targetScreen = this.props.navigation.getParam('targetScreen');
    targetScreen ? this.props.navigation.navigate(targetScreen) : this.props.navigation.goBack();
  }

  goBack() {
    const dismissTarget = this.props.navigation.getParam('dismissTarget');
    if (dismissTarget) this.props.navigation.navigate(dismissTarget);
    else {
      const dismissable = this.props.navigation.getParam('dismissable');
      dismissable ? this.props.navigation.dismiss() : this.props.navigation.goBack();
    }
  }

  render() {
    return (
      <Container style={styles.container}>
        <Camera onPictureTaken={this.onPictureTaken.bind(this)} onClose={this.goBack.bind(this)} />
      </Container>
    );
  }
}
