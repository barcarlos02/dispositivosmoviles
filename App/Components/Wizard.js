import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Icon } from 'react-native-elements';
import { Text, Touchable, Container } from './UI';
import { Colors } from '../Themes';
import styles from './Styles/WizardStyle';
import MindfulnessStep from './MindfulnessStep';
import IconButton from './IconButton';

const categories = ['Ingredients', 'Presence'];

export default class Wizard extends Component {
  // Prop type warnings
  static propTypes = {
    steps: PropTypes.arrayOf(PropTypes.instanceOf(Object)).isRequired,
    onNext: PropTypes.func,
    onPrev: PropTypes.func,
    onStepChanged: PropTypes.func,
    currentStep: PropTypes.number,
    onDone: PropTypes.func,
  };

  // Defaults for props
  static defaultProps = {
    onNext: () => {},
    onPrev: () => {},
    onStepChanged: () => {},
    currentStep: 0,
    onDone: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      step: props.currentStep,
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.currentStep !== prevProps.currentStep) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        step: this.props.currentStep,
      });
    }
  }

  _renderStep() {
    const { step } = this.state;
    const { steps } = this.props;
    return steps[step].component;
  }

  goPrev() {
    // Abort if we are on the first step
    const { step } = this.state;
    if (step === 0) return;
    this.props.onPrev();
    this.setState(
      {
        step: step - 1,
      },
      () => {
        this.props.onStepChanged(this.state.step);
      }
    );
  }

  goNext() {
    // Abort if we are on the last step
    const { step } = this.state;
    const { steps } = this.props;
    if (step === steps.length - 1) return;
    this.props.onNext();
    this.setState(
      {
        step: step + 1,
      },
      () => {
        this.props.onStepChanged(this.state.step);
      }
    );
  }

  _renderDescription() {
    const { step } = this.state;
    const { steps } = this.props;
    const currentStep = steps[step];
    if (!currentStep.description || currentStep.description === '') {
      return null;
    }
    return <Text style={styles.stepDescription}>{currentStep.description}</Text>;
  }

  _renderDots() {
    const { steps } = this.props;
    const { step } = this.state;
    const dots = steps.map((_, index) => {
      const isSelected = step === index;
      // eslint-disable-next-line react/no-array-index-key
      return <View style={[styles.dot, isSelected && styles.dotSelected]} key={index} />;
    });
    return <View style={styles.dotsContainer}>{dots}</View>;
  }

  onCategorySelected(step) {
    this.setState({ step });
  }

  render() {
    const { step } = this.state;
    const { steps } = this.props;
    const notFirstStep = step !== 0;
    const notLastStep = step !== steps.length - 1;
    return (
      <Container style={styles.container}>
        <View style={styles.wrapper}>
          <MindfulnessStep
            title="Waste"
            description="What did you meal have"
            category={categories}
            categorySelected={this.state.step}
            onCategorySelected={this.onCategorySelected.bind(this)}
          />
          <View style={styles.stepContainer}>{this._renderStep()}</View>
        </View>
        {this._renderDescription()}
        <View style={styles.stepButtonContainer}>
          {notLastStep && (
            <Container style={styles.stepButton}>
              <IconButton
                icon="ic_chevron"
                onPress={this.goNext.bind(this)}
                iconStyle={{ color: Colors.background }}
                size={40}
                containerStyle={[{ backgroundColor: Colors.text }, styles.icon]}
              />
            </Container>
          )}
          {!notLastStep && (
            <Container style={styles.stepButton}>
              <IconButton
                icon="ic_check_circle"
                onPress={this.props.onDone}
                iconStyle={{ color: Colors.text }}
                size={70}
              />
            </Container>
          )}
        </View>
        {this._renderDots()}
      </Container>
    );
  }
}
