import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Icon } from 'react-native-elements';
import RNModal from 'react-native-modal';
import { Text, Touchable, Container } from './UI';
import { Colors } from '../Themes';
import styles from './Styles/ModalStyle';

const types = {
  modal: 'modal',
  sheet: 'sheet',
};

export default class Modal extends Component {
  // Prop type warnings
  static propTypes = {
    visible: PropTypes.bool,
    modalProps: PropTypes.instanceOf(Object),
    children: PropTypes.node.isRequired,
    onClose: PropTypes.func.isRequired,
    closeOnBackdrop: PropTypes.bool,
    backgroundColor: PropTypes.string,
    title: PropTypes.string,
    top: PropTypes.number,
    type: PropTypes.string,
    containerStyle: PropTypes.instanceOf(Object),
    style: PropTypes.instanceOf(Object),
    showHeader: PropTypes.bool,
  };

  // Defaults for props
  static defaultProps = {
    visible: false,
    modalProps: {},
    closeOnBackdrop: true,
    backgroundColor: Colors.main,
    title: '',
    top: null,
    type: types.sheet,
    containerStyle: {},
    style: {},
    showHeader: true,
  };

  constructor(props) {
    super(props);
    this.state = {
      visible: props.visible,
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.visible !== prevProps.visible) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        visible: this.props.visible,
      });
    }
  }

  closeModal() {
    this.setState({ visible: false });
  }

  handleClose() {
    const { closeOnBackdrop } = this.props;
    if (!closeOnBackdrop) return null;
    return this.closeModal();
  }

  _renderTitle() {
    const { title } = this.props;
    return (
      <View style={styles.modalHeader}>
        <Text style={styles.modalTitle}>{title || ''}</Text>
        <Touchable onPress={this.closeModal.bind(this)} style={styles.closeBtn}>
          <Icon name="close" type="font-awesome" />
        </Touchable>
      </View>
    );
  }

  render() {
    const {
      modalProps,
      children,
      onClose,
      backgroundColor,
      top,
      type,
      style,
      containerStyle,
      showHeader,
    } = this.props;
    const { visible } = this.state;
    const styleType =
      type === types.sheet
        ? [styles.modalContentSheet, { top: top || 'auto', backgroundColor }]
        : styles.modalContent;
    return (
      <RNModal
        backdropOpacity={0.6}
        {...modalProps}
        isVisible={visible}
        onModalHide={onClose}
        onBackdropPress={this.handleClose.bind(this)}
        style={[styles.modalContainer, containerStyle]}
      >
        <Container style={[styleType, { backgroundColor }, style]}>
          {showHeader && this._renderTitle()}
          {children}
        </Container>
      </RNModal>
    );
  }
}
