import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { upperCase } from 'lodash';
import { Text, Container } from './UI';
import styles from './Styles/CommonScoreStyle';
import Icon from './CustomIcon';
import Carrots from './Carrots';

const CommonScore = ({ carrots, likes, type }) => {
  return (
    // <Container style={{ flex: 1, alignItems: 'flex-end' }}>
    <Container style={styles.container}>
      {!!type && (
        <Container style={styles.typeContainer}>
          <Text>3</Text>
          <Icon name="ic_heart" size={50} style={styles.icon} />
        </Container>
      )}
      <Container style={styles.likesContainer}>
        <Text style={[styles.text, { marginLeft: 5 }]}>24</Text>
        <Icon name="ic_heart" size={25} style={[styles.icon, { marginLeft: 5 }]} />
      </Container>
      <Container style={styles.carrotsContainer}>
        <Text style={[styles.text, styles.mindfulText]}>Mindful score</Text>
        <Carrots carrots={0.5} />
      </Container>
    </Container>
    // </Container>
  );
};

// Prop type warnings
CommonScore.propTypes = {
  likes: PropTypes.number,
  carrots: PropTypes.number,
  type: PropTypes.string,
};

// Defaults for props
CommonScore.defaultProps = {
  carrots: 0,
  likes: null,
  type: null,
};

export default CommonScore;
