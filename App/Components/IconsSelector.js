import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Text, Touchable, Container } from './UI';
import Icon from './CustomIcon';
import styles from './Styles/IconsSelectorStyle';
import { Colors } from '../Themes';

export default class IconsSelector extends Component {
  // Prop type warnings
  static propTypes = {
    options: PropTypes.instanceOf(Object).isRequired,
    selected: PropTypes.instanceOf(Array),
    size: PropTypes.number,
    color: PropTypes.string,
    textColor: PropTypes.string,
    selectedColor: PropTypes.string,
    onChange: PropTypes.func,
    inverse: PropTypes.bool,
  };

  // Default Props
  static defaultProps = {
    selected: [],
    size: 65,
    color: Colors.main,
    textColor: Colors.text,
    selectedColor: Colors.accent,
    onChange: () => {},
    inverse: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      selected: props.selected,
    };
  }

  _toggleOption(option) {
    const { selected: _selected } = this.state;
    const selected = [..._selected];
    const idx = selected.findIndex(i => i.value === option.value);
    if (idx > -1) {
      // remove item
      selected.splice(idx, 1);
    } else {
      selected.push(option);
    }
    this.setState(
      {
        selected,
      },
      () => {
        this.props.onChange(selected);
      }
    );
  }

  _renderIcons() {
    const { options, size, color, textColor, selectedColor, inverse } = this.props;
    const { selected } = this.state;
    return options.map(option => {
      const isSelected = selected.findIndex(i => i.value === option.value) > -1;
      const iconWrapperStyle = {
        width: size,
        height: size,
        borderRadius: size / 2,
        backgroundColor: isSelected && !inverse ? selectedColor : color,
      };
      const textStyle = {
        color: isSelected ? selectedColor : textColor,
      };
      return (
        <Touchable
          type="opacity"
          style={[styles.option]}
          key={option.value}
          onPress={this._toggleOption.bind(this, option)}
        >
          <View style={[styles.iconWrapper, iconWrapperStyle]}>
            <Icon
              name={option.image}
              size={Math.round(size * 0.55)}
              style={{ color: isSelected && inverse ? selectedColor : Colors.background }}
            />
          </View>
          <Text style={textStyle}>{option.label}</Text>
        </Touchable>
      );
    });
  }

  render() {
    return <Container style={styles.container}>{this._renderIcons()}</Container>;
  }
}
