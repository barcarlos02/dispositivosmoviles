import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'react-native-elements';
import styles from './Styles/HeaderIconStyle';
import { Touchable } from './UI';
import { Metrics } from '../Themes';
import CustomIcon from './CustomIcon';

const iconSize = Metrics.navBarHeight / 2.5;

const HeaderIcon = ({ onPress, name, style, custom, ...iconProps }) => {
  const IconComponent = custom ? CustomIcon : Icon;
  return (
    <Touchable style={[styles.navbarTouchable, style]} onPress={onPress}>
      <IconComponent {...iconProps} name={name} size={iconSize} style={styles.icon} />
    </Touchable>
  );
};

// Prop type warnings
HeaderIcon.propTypes = {
  onPress: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  custom: PropTypes.bool,
  style: PropTypes.instanceOf(Object),
};

// Defaults for props
HeaderIcon.defaultProps = {
  custom: true,
  style: {},
};

export default HeaderIcon;
