import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, TouchableNativeFeedback, Platform } from 'react-native';
import styles from './Styles/TouchableStyle';

const TouchableComponent = Platform.OS === 'ios' ? TouchableOpacity : TouchableNativeFeedback;

const Touchable = ({ children, style, onPress, disabled, ...rest }) => {
  const handleOnPress = () => {
    if (!disabled) {
      onPress();
    }
  };

  return (
    <TouchableComponent {...rest} onPress={handleOnPress} style={[styles.button, style]}>
      <View style={[{ flex: 1 }, style]}>{children}</View>
    </TouchableComponent>
  );
};

// Prop type warnings
Touchable.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  onPress: PropTypes.func.isRequired,
  style: PropTypes.instanceOf(Object),
  disabled: PropTypes.bool,
};

// Defaults for props
Touchable.defaultProps = {
  children: null,
  style: {},
  disabled: false,
};

export default Touchable;
