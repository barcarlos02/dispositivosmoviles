import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { MenuOption } from 'react-native-popup-menu';
import { Text, Container } from './UI';
import Avatar from '../Containers/Avatar';
import Icon from './CustomIcon';
import { Colors, Metrics } from '../Themes';
// import IconButton from './IconButton';

import styles from './Styles/ItemPopupMenuStyle';

const height = Metrics.navBarHeight;
const iconSize = height * 0.4;
const imageSize = height * 0.65;

export default class ItemPopupMenu extends Component {
  // // Prop type warnings
  static propTypes = {
    // If you need text with an image use, icon and image prop, but if you want an IconButton use isIconButton and iconType
    onPress: PropTypes.func,
    type: PropTypes.string,
    value: PropTypes.string,
    selected: PropTypes.bool,
    title: PropTypes.string,
    icon: PropTypes.bool,
    image: PropTypes.string,
    profileType: PropTypes.string,
    isIconButton: PropTypes.bool,
    iconType: PropTypes.string,
  };

  //
  // // Defaults for props
  static defaultProps = {
    onPress: () => {},
    type: 'profile',
    value: null,
    selected: false,
    title: 'World eatups',
    icon: false,
    image: null,
    profileType: 'foodie',
    isIconButton: false,
    iconType: '',
  };

  _renderImage() {
    const { type, icon, image, profileType, value } = this.props;
    if (!!type && type === 'profile') {
      if (icon) {
        return (
          <View style={styles.profileAvatar}>
            <Icon name="ic_fab_plus" size={iconSize} style={{ color: Colors.windowTint }} />
          </View>
        );
      }
      return (
        <View style={styles.profileAvatar}>
          <Avatar url={image} size={imageSize} type={profileType} id={value} />
        </View>
      );
    }
    return null;
  }

  _renderIcon(selected) {
    const { iconType } = this.props;
    const size = 40;
    return (
      <Container style={{ width: 100, alignItems: 'center' }}>
        <Container
          style={[
            styles.iconContainer,
            { height: size + 8, width: size + 8, borderRadius: size },
            selected && styles.iconSelected,
          ]}
        >
          <Icon style={{ alignSelf: 'center' }} name={iconType} size={size} color={Colors.main} />
        </Container>
      </Container>
    );
  }

  render() {
    const { onPress, title, type, selected, value, isIconButton } = this.props;
    let itemStyle = {};
    switch (type) {
      case 'filter':
        itemStyle = {
          optionContainer: 'filterItem',
          container: 'filterContainer',
          text: 'filterText',
        };
        break;
      case 'options':
        itemStyle = {
          optionContainer: 'optionsItem',
          container: 'optionsContainer',
          text: 'optionsText',
        };
        break;
      default:
        itemStyle = {
          optionContainer: 'profileItem',
          container: 'profileContainer',
          text: 'profileText',
        };
    }
    if (type === 'filter')
      // If it is an iconbutton(eatups filter)
      return (
        <MenuOption
          value={value}
          onSelect={() => {
            onPress(value);
          }}
          style={[styles.item, styles[itemStyle.optionContainer]]}
        >
          <View style={[styles[itemStyle.container]]}>
            {this._renderIcon(selected)}
            <Text style={[styles.text, styles[itemStyle.text]]}>{title}</Text>
          </View>
        </MenuOption>
      );

    return (
      <MenuOption
        value={value}
        onSelect={() => {
          onPress(value);
        }}
        style={[styles.item, styles[itemStyle.optionContainer]]}
      >
        <View style={[styles[itemStyle.container], selected && styles.selected]}>
          {this._renderImage()}
          <Text style={[styles.text, styles[itemStyle.text]]}>{title}</Text>
        </View>
      </MenuOption>
    );
  }
}
