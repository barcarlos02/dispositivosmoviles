import React from 'react';
import PropTypes from 'prop-types';
import { View, ImageBackground } from 'react-native';
import Color from 'color';
import { Text, Spinner, Container } from './UI';
import Icon from './CustomIcon';
import IconButton from './IconButton';
import CommonBanner from './CommonBanner';
import CommonScore from './CommonScore';
import imageUrl from '../HOCs/ImageUrl';

// Styles
import { Colors, Metrics } from '../Themes';
import styles from './Styles/CommonCardStyle';

const CommonCard = ({
  label,
  showCamera,
  children,
  form,
  amount,
  discount,
  owner,
  name,
  color,
  loading,
  description,
  image,
  imageUrl,
  type,
  atributes,
}) => {
  const darkColor = Color(color).darken(0.5).s;
  return (
    <Container style={[styles.container, form && styles.inForm, { backgroundColor: color }]}>
      {loading ? (
        <View style={styles.loadingWrapper}>
          <Spinner color={`${darkColor}`} />
        </View>
      ) : (
        <ImageBackground source={imageUrl} style={styles.imageContainer} imageStyle={styles.image}>
          <View style={styles.imageContent}>
            {!!label && <Icon name={label} size={40} style={styles.icon} />}
            <CommonBanner
              name={name}
              owner={owner}
              amount={amount}
              discount={discount}
              type={type}
            />
            <View style={styles.content}>{children}</View>

            <CommonScore />
            {description !== '' && (
              <View style={styles.description}>
                <Text style={styles.descpriptionText}>{description}</Text>
              </View>
            )}
            {/* {showCamera && (
              <View style={[styles.btnContainer]}>
                <IconButton
                  icon="ic_camera_simplified"
                  size={Metrics.screenWidth / 4.5}
                  title="it works much better with a pretty photo"
                />
              </View>
            )} */}
          </View>
        </ImageBackground>
      )}
    </Container>
  );
};

// Prop type warnings
CommonCard.propTypes = {
  label: PropTypes.string,
  showCamera: PropTypes.bool,
  children: PropTypes.node,
  form: PropTypes.bool,
  amount: PropTypes.string,
  discount: PropTypes.string,
  owner: PropTypes.string,
  name: PropTypes.string,
  color: PropTypes.string,
  loading: PropTypes.bool,
  description: PropTypes.string,
  image: PropTypes.string,
  id: PropTypes.string.isRequired,
  imageUrl: PropTypes.oneOfType([PropTypes.instanceOf(Object), PropTypes.number]), // Allow us multiple types
  type: PropTypes.string,
  atributes: PropTypes.instanceOf(Object),
};

// Defaults for props
CommonCard.defaultProps = {
  label: null,
  showCamera: false,
  children: null,
  form: false,
  amount: null,
  discount: null,
  owner: '',
  name: '',
  color: Colors.secondary,
  loading: false,
  description: '',
  imageUrl: {},
  image: '',
  type: null,
  atributes: {},
};

export default imageUrl(CommonCard, propTypes => {
  return {
    ...propTypes,
    id: propTypes.id,
    path: propTypes.image,
  };
});
