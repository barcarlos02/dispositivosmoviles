import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Image, Icon } from 'react-native-elements';
import { Formik, Field, FastField } from 'formik';
import * as Yup from 'yup';
import { Spinner, Button, Container, Touchable, Text } from './UI';
import { TextInput, MindfulnessWizard } from './Forms';
import KAV from '../HOCs/KeyboardAvoidingView';
import styles from './Styles/EatupFormStyle';
import IconButton from './IconButton';
import { Colors } from '../Themes';
import SelectedIngredients from './SelectedIngredients';

const EatupForm = ({
  onSubmit,
  images,
  onPhotoAdded,
  onChange,
  initialValues,
  onMindfulnessButtonPressed,
  onSave,
  uploading,
}) => {
  const _renderImage = () => {
    return images.map(image => {
      return (
        <Image
          key={Math.random()}
          style={styles.image}
          source={image ? { uri: image } : require('../Images/boostCardImg.jpg')}
          PlaceholderContent={<Spinner />}
        />
      );
    });
  };
  return (
    <Formik
      initialValues={{ ...initialValues }}
      enableReinitialize
      onSubmit={(values, actions) => {
        onSubmit(values);
        actions.setSubmitting(false);
      }}
      validationSchema={Yup.object().shape({
        description: Yup.string().required('You need to add a description to your awesome Eatup!'),
      })}
      render={({ handleSubmit, isSubmitting, isValid, errors }) => (
        <Container style={styles.container}>
          <View style={[styles.formContainer, { marginBottom: 0 }]}>
            <Field
              formGroupStyle={{ marginBottom: 0 }}
              component={TextInput}
              name="description"
              placeholder="Tell us about your delicious Eatup"
              onChange={onChange}
            />
            {!!errors.description && <Text style={styles.error}>{errors.description}</Text>}
          </View>
          <View style={styles.formContainer}>
            {_renderImage()}
            <Container style={styles.cameraButton}>
              <IconButton
                icon="ic_camera_simplified"
                onPress={onPhotoAdded}
                iconStyle={{ color: Colors.background }}
                containerStyle={{ backgroundColor: Colors.accent }}
              />
            </Container>
          </View>

          <View style={[styles.formContainer, { marginBottom: 0 }]}>
            {!initialValues.ingredients && (
              <Button
                title="HOW MINDFUL WAS THIS?"
                disabled={isSubmitting}
                onPress={onMindfulnessButtonPressed}
                variant="secondary"
                type="outline"
                textColor={Colors.fire}
                containerStyle={styles.buttonContainer}
              />
            )}
            {!!initialValues.ingredients && (
              <Touchable style={styles.levelTitleContainer} onPress={onMindfulnessButtonPressed}>
                <Text style={styles.levelTitle}>This meal's mindfulness level</Text>
                <Icon name="pencil" type="font-awesome" color={Colors.secondary} />
              </Touchable>
            )}
          </View>
          <Container>
            <SelectedIngredients ingredients={initialValues.ingredients} />
          </Container>

          <Container style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
            <IconButton
              icon="ic_check_circle"
              onPress={handleSubmit}
              iconStyle={{ color: Colors.darkBackground }}
              size={100}
              loading={uploading}
            />
          </Container>
        </Container>
      )}
    />
  );
};

EatupForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  images: PropTypes.arrayOf(PropTypes.string),
  onPhotoAdded: PropTypes.func.isRequired,
  onChange: PropTypes.func,
  initialValues: PropTypes.instanceOf(Object),
  onMindfulnessButtonPressed: PropTypes.func,
  onSave: PropTypes.func.isRequired,
  uploading: PropTypes.bool,
};

EatupForm.defaultProps = {
  onChange: () => {},
  initialValues: {},
  images: [],
  onMindfulnessButtonPressed: () => {},
  uploading: false,
};

export default KAV(EatupForm);
