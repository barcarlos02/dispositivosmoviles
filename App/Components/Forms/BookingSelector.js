import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Text, Button } from '../UI';

import { Colors, Fonts, Metrics } from '../../Themes';
import styles from './Styles';

export default class BookingSelector extends Component {
  // Prop Types
  static propTypes = {
    title: PropTypes.string,
    form: PropTypes.instanceOf(Object).isRequired,
    field: PropTypes.instanceOf(Object).isRequired,
    formGroupStyle: PropTypes.instanceOf(Object),
  };

  // Default Props
  static defaultProps = {
    title: '',
    formGroupStyle: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      value: false,
    };
  }

  shouldComponentUpdate(prevState, actualState) {
    return prevState.value !== actualState.value;
  }

  _onChange(value) {
    const {
      form: { setFieldValue },
      field: { name },
    } = this.props;

    this.setState({ value }, () => {
      setFieldValue(name, value);
    });
  }

  render() {
    const { title, form, field, formGroupStyle, ...rest } = this.props;
    const { value } = this.state;
    const { touched, errors } = form;
    const errorMsg = touched[field.name] && errors[field.name];
    return (
      <View style={[styles.formGroup, formGroupStyle]}>
        {!!title && <Text style={[styles.text, styles.title]}>{title}</Text>}
        <Button
          {...rest}
          title={value ? 'YES' : 'NO'}
          onPress={() => this._onChange(!value)}
          raised={false}
          containerStyle={{
            width: Metrics.screenWidth / 3.5,
            height: Metrics.navBarHeight * 0.85,
            backgroundColor: Colors.darkMain,
          }}
          buttonStyle={{
            borderRadius: 5,
            borderWidth: 1,
            borderColor: Colors.darkMain,
          }}
          titleStyle={{
            fontFamily: Fonts.type.base,
            textAlignVertical: 'center',
            textAlign: 'center',
            color: Colors.text,
          }}
          variant="secondary"
        />
        {!!errorMsg && <Text style={[styles.text, styles.error]}>{errors[field.name]}</Text>}
      </View>
    );
  }
}
