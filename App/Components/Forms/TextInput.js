import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Input } from 'react-native-elements';
import { Text } from '../UI';

import styles from './Styles';

export default class TextInput extends PureComponent {
  // Prop Types
  static propTypes = {
    title: PropTypes.string,
    hint: PropTypes.string,
    form: PropTypes.instanceOf(Object).isRequired,
    field: PropTypes.instanceOf(Object).isRequired,
    formGroupStyle: PropTypes.instanceOf(Object),
  };

  // Default Props
  static defaultProps = {
    title: '',
    hint: '',
    formGroupStyle: {},
  };

  _onChange(value) {
    const {
      form: { setFieldValue },
      field: { name },
    } = this.props;
    setFieldValue(name, value);
  }

  _onFocus() {
    const {
      form: { setFieldTouched },
      field: { name },
    } = this.props;
    setFieldTouched(name, true);
  }

  render() {
    const { title, hint, form, field, formGroupStyle, ...rest } = this.props;
    const { touched, errors } = form;
    const errorMsg = touched[field.name] && errors[field.name];
    return (
      <View style={[styles.formGroup, formGroupStyle]}>
        {!!title && <Text style={[styles.text, styles.title]}>{title}</Text>}
        <Input
          {...rest}
          onChangeText={this._onChange.bind(this)}
          onBlur={this._onFocus.bind(this)}
          value={field.value}
        />
        {!!hint && <Text style={[styles.text, styles.hint]}>{hint}</Text>}
      </View>
    );
  }
}
