import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { isEmpty } from 'lodash';
import { Text } from '../UI';
import CommonCard from '../../Containers/CommonCard';
import styles from './Styles';

export default class TextInput extends PureComponent {
  // Prop Types
  static propTypes = {
    form: PropTypes.instanceOf(Object).isRequired,
    field: PropTypes.instanceOf(Object).isRequired,
    formGroupStyle: PropTypes.instanceOf(Object),
  };

  // Default Props
  static defaultProps = {
    formGroupStyle: {},
  };

  handleOnPress = () => {
    const { onPress } = this.props;
    this._onFocus();
    onPress();
  };

  _onChange(value) {
    const {
      form: { setFieldValue, setFieldError },
      field: { name },
    } = this.props;
    if (isEmpty(value)) {
      setFieldError(name, 'Need to add a photo');
    } else {
      setFieldValue(name, value);
    }
  }

  _onFocus() {
    const {
      form: { setFieldTouched, setFieldValue },
      field: { name },
    } = this.props;
    setFieldTouched(name, true);
  }

  render() {
    const { form, field, formGroupStyle, boostName, ...rest } = this.props;
    const { touched, errors } = form;
    const errorMsg = touched[field.name] && errors[field.name];
    return (
      <View style={[styles.formGroup, formGroupStyle]}>
        <CommonCard
          {...rest}
          form
          showCamera
          name={boostName}
          onCameraPress={this.handleOnPress.bind(this)}
          onCameraPressCallback={this._onChange.bind(this)}
        />
        {!!errorMsg && <Text style={[styles.error]}>{errors[field.name]}</Text>}
      </View>
    );
  }
}
