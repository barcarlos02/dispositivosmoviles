import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Text } from '../UI';
import Icons from '../IconsSelector';

import styles from './Styles';

export default class IconsSelector extends PureComponent {
  // Prop Types
  static propTypes = {
    form: PropTypes.instanceOf(Object).isRequired,
    field: PropTypes.instanceOf(Object).isRequired,
    title: PropTypes.string,
    formGroupStyle: PropTypes.instanceOf(Object),
  };

  // Default Props
  static defaultProps = {
    title: '',
    formGroupStyle: {},
  };

  _onChange(value) {
    const {
      form: { setFieldValue, setFieldTouched },
      field: { name },
    } = this.props;
    setFieldValue(name, value);
    setFieldTouched(name, true);
  }

  render() {
    const { form, field, title, formGroupStyle, ...rest } = this.props;
    const { touched, errors } = form;
    const errorMsg = touched[field.name] && errors[field.name];
    return (
      <View style={[styles.formGroup, formGroupStyle]}>
        {!!title && (
          <Text style={[styles.text, styles.textCenter, styles.iconsTitle]}>{title}</Text>
        )}
        <Icons {...rest} onChange={this._onChange.bind(this)} />
        {errorMsg && <Text style={styles.error}>{errors[field.name]}</Text>}
      </View>
    );
  }
}
