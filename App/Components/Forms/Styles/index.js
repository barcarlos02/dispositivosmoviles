import { StyleSheet } from 'react-native';
import { Fonts, ApplicationStyles } from '../../../Themes';
import RNE_Theme from '../../../Themes/RNE_Theme';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  formGroup: {
    marginBottom: 35,
  },
  title: {
    textAlign: 'center',
    fontSize: Fonts.size.medium,
    marginBottom: 5,
  },
  hint: {
    textAlign: 'center',
    fontSize: Fonts.size.medium,
    marginTop: 5,
  },
  iconsTitle: {
    marginBottom: 10,
    fontSize: Fonts.size.input,
  },
  error: {
    ...RNE_Theme.Input.errorStyle,
    marginTop: 5,
  },
});
