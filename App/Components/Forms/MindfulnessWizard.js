import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { isEmpty } from 'lodash';
import { Text } from '../UI';
import MindfulnessWizard from '../MindfulnessWizard';
import styles from './Styles';

export default class TextInput extends PureComponent {
  // Prop Types
  static propTypes = {
    form: PropTypes.instanceOf(Object).isRequired,
    field: PropTypes.instanceOf(Object).isRequired,
    formGroupStyle: PropTypes.instanceOf(Object),
    onChange: PropTypes.func,
  };

  // Default Props
  static defaultProps = {
    formGroupStyle: {},
    onChange: () => {},
  };

  _onChange(value) {
    const {
      form: { setFieldValue, setFieldError },
      field: { name },
      onChange,
    } = this.props;
    if (isEmpty(value.ingredients)) {
      setFieldError(name, 'Ingredients Missing!');
    } else {
      setFieldValue(name, value);
      onChange(value.ingredients); // callback
    }
  }

  _onFocus() {
    const {
      form: { setFieldTouched, setFieldValue },
      field: { name },
    } = this.props;
    setFieldValue(name, {});
    setFieldTouched(name, true);
  }

  render() {
    const { form, field, formGroupStyle, ...rest } = this.props;
    const { touched, errors } = form;
    const errorMsg = touched[field.name] && errors[field.name];
    const value = form.values[field.name] || {};
    return (
      <View style={[styles.formGroup, formGroupStyle]}>
        <MindfulnessWizard
          {...rest}
          initialValues={value}
          onOpen={this._onFocus.bind(this)}
          onClose={this._onChange.bind(this)}
        />
        {!!errorMsg && <Text style={[styles.error]}>{errors[field.name]}</Text>}
      </View>
    );
  }
}
