import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

import Secrets from 'react-native-config';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { Text } from '../UI';
import { Colors, Fonts } from '../../Themes';
import RNE_Theme from '../../Themes/RNE_Theme';

import styles from './Styles';

const inputStyles = {
  container: {
    ...RNE_Theme.Input.inputContainerStyle,
    flex: 1,
    borderRadius: 5,
  },
  textInputContainer: {
    borderTopWidth: 0,
    borderBottomWidth: 0,
    height: 'auto',
    borderRadius: 5,
  },
  textInput: {
    ...RNE_Theme.Input.inputStyle,
    marginTop: 0,
    marginBottom: 0,
    marginLeft: 0,
    marginRight: 0,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: Fonts.size.input,
    height: 'auto',
  },
  description: { ...Fonts.style.description },
  predefinedPlacesDescription: {
    color: '#1faadb',
  },
  listView: {
    backgroundColor: '#fff',
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    marginTop: -5,
  },
  separator: {
    height: 2,
    backgroundColor: Colors.accent,
  },
};

export default class AddressInput extends PureComponent {
  // Prop Types
  static propTypes = {
    placeholder: PropTypes.string,
    hint: PropTypes.string,
    form: PropTypes.instanceOf(Object).isRequired,
    field: PropTypes.instanceOf(Object).isRequired,
  };

  static defaultProps = {
    placeholder: 'address',
    hint: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      showResults: false,
    };
  }

  _onChange(value) {
    const {
      form: { setFieldValue },
      field: { name },
    } = this.props;
    setFieldValue(name, value);
  }

  _onFocus() {
    const {
      form: { setFieldTouched },
      field: { name },
    } = this.props;
    setFieldTouched(name, true);
  }

  render() {
    const { form, field, placeholder, hint } = this.props;
    const { touched, errors } = form;
    const errorMsg = touched[field.name] && errors[field.name];
    return (
      <View style={styles.formGroup}>
        <GooglePlacesAutocomplete
          placeholder={placeholder}
          minLength={2}
          autoFocus={false}
          fetchDetails
          listViewDisplayed={this.state.showResults}
          query={{
            // available options: https://developers.google.com/places/web-service/autocomplete
            key: Secrets.GOOGLE_MAPS_API_KEY,
            language: 'en', // language of the results
            types: 'address',
          }}
          debounce={100}
          onPress={(data, details = null) => {
            this.setState({ showResults: false });
            this._onChange(details);
          }}
          getDefaultValue={() => {
            return ''; // text input default value
          }}
          textInputProps={{
            onChangeText: data => {
              this.setState({ showResults: true });
            },
            onBlur: data => {
              this._onFocus(data);
            },
          }}
          enablePoweredByContainer={false}
          disableScroll={false}
          placeholderTextColor={Colors.windowTint}
          styles={inputStyles}
        />
        {!!errorMsg && <Text style={[styles.error]}>{errors[field.name]}</Text>}
        {!!hint && <Text style={[styles.text, styles.hint]}>{hint}</Text>}
      </View>
    );
  }
}
