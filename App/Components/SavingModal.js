import React from 'react';
import PropTypes from 'prop-types';
import { Text, Container, Button } from './UI';
import Modal from './Modal';
import styles from './Styles/SavingModalStyle';

const SavingModal = ({ visible, onSave, onExit, onCancel, disableSave, disableExit, text }) => {
  return (
    <Modal
      visible={visible}
      top={100}
      type="modal"
      onClose={onCancel}
      containerStyle={styles.modal}
    >
      <Text style={styles.text}>{text}</Text>
      <Container style={styles.buttonContainer}>
        {!disableSave && (
          <Button onPress={onSave} title="Save" containerStyle={styles.buttonContainer} />
        )}
        {!disableExit && (
          <Button onPress={onExit} title="Exit" containerStyle={styles.buttonContainer} />
        )}
      </Container>
    </Modal>
  );
};

// Prop type warnings
SavingModal.propTypes = {
  visible: PropTypes.bool,
  onSave: PropTypes.func,
  onExit: PropTypes.func,
  onCancel: PropTypes.func,
  disableExit: PropTypes.bool,
  disableSave: PropTypes.bool,
  text: PropTypes.string,
};

// Defaults for props
SavingModal.defaultProps = {
  visible: false,
  onSave: () => {},
  onExit: () => {},
  onCancel: () => {},
  disableExit: false,
  disableSave: false,
  text: '',
};

export default SavingModal;
