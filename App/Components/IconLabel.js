import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Text, Container } from './UI';
import styles from './Styles/IconLabelStyle';
import { Colors } from '../Themes';
import Icon from './CustomIcon';

const IconLabel = ({ icon, label, iconColor, color, textColor, size }) => {
  return (
    <Container style={styles.container}>
      <View
        style={[
          styles.circle,
          { backgroundColor: color, width: size, height: size, borderRadius: size / 2 },
        ]}
      >
        <Icon name={icon} size={Math.round(size * 0.55)} style={{ color: iconColor }} />
      </View>
      <Text style={[styles.text, { color: textColor }]}>{label}</Text>
    </Container>
  );
};

IconLabel.propTypes = {
  icon: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  iconColor: PropTypes.string,
  color: PropTypes.string,
  textColor: PropTypes.string,
  size: PropTypes.number,
};

IconLabel.defaultProps = {
  color: Colors.main,
  textColor: Colors.text,
  iconColor: Colors.accent,
  size: 200,
};

export default IconLabel;
