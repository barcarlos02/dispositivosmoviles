import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Text, Container } from './UI';
import styles from './Styles/BannerStyle';
import { Colors } from '../Themes';

const Banner = ({ children, style, color, title, titleColor }) => {
  return (
    <Container style={[styles.container, style, { backgroundColor: color }]}>
      {!!title && <Text style={[styles.title, { color: titleColor }]}>{title}</Text>}
      {children}
    </Container>
  );
};

Banner.propTypes = {
  children: PropTypes.element.isRequired,
  style: PropTypes.instanceOf(Object),
  color: PropTypes.string,
  title: PropTypes.string,
  titleColor: PropTypes.string,
};

Banner.defaultProps = {
  style: {},
  color: Colors.main,
  title: '',
  titleColor: Colors.main,
};

export default Banner;
