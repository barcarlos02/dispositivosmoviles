import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Animated } from 'react-native';
import styles from './Styles/FadeStyle';

export default class Fade extends Component {
  // Prop type warnings
  static propTypes = {
    visible: PropTypes.bool.isRequired,
    children: PropTypes.node.isRequired,
    style: PropTypes.instanceOf(Object),
  };

  // Defaults for props
  static defaultProps = {
    style: {},
  };

  constructor(props) {
    super(props);
    this._visibility = new Animated.Value(props.visible ? 1 : 0);
    this.state = {
      visible: props.visible,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.visible !== prevState.visible) {
      return { visible: nextProps.visible };
    }
    return null;
  }

  componentDidUpdate() {
    const { visible } = this.state;
    Animated.timing(this._visibility, {
      toValue: visible ? 1 : 0,
      duration: 300,
    }).start();
  }

  render() {
    const { style, children, ...rest } = this.props;
    const { visible } = this.state;
    const containerStyle = {
      opacity: this._visibility.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1],
      }),
    };
    const combinedStyle = [containerStyle, style];
    return (
      <Animated.View style={visible ? combinedStyle : containerStyle} {...rest}>
        {children}
      </Animated.View>
    );
  }
}
