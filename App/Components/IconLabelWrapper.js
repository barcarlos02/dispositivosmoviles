import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import styles from './Styles/IconLabelWrapperStyle';
import IconLabel from './IconLabel';
import { Metrics } from '../Themes';
import { Container } from './UI';

const IconLabelWrapper = ({ icons, ...props }) => {
  const size = Metrics.screenWidth / icons.length - 30;
  return (
    <Container style={styles.container}>
      {icons.map((i, idx) => (
        <IconLabel key={idx} {...i} {...props} size={size} />
      ))}
    </Container>
  );
};

IconLabelWrapper.propTypes = {
  icons: PropTypes.instanceOf(Array).isRequired,
};

export default IconLabelWrapper;
