import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Text, Touchable, Container, Spinner } from './UI';
import Icon from './CustomIcon';

import styles from './Styles/IconButtonStyle';

const getIconSize = size => {
  return size * 0.55;
};

const IconButton = ({ icon, size, title, onPress, iconStyle, containerStyle, loading }) => {
  let render;
  if (loading) render = <Spinner visible style={styles.icon} size="large" />;
  else render = <Icon name={icon} size={getIconSize(size)} style={[styles.icon, iconStyle]} />;
  return (
    <Container style={styles.container}>
      <Touchable
        type="opacity"
        onPress={() => {
          onPress();
        }}
      >
        <View
          style={[
            {
              height: size,
              width: size,
              borderRadius: size / 2,
            },
            styles.btnContainer,
            containerStyle,
          ]}
        >
          {render}
        </View>
      </Touchable>
      {title && <Text style={styles.title}>{title}</Text>}
    </Container>
  );
};

IconButton.propTypes = {
  // icon: PropTypes.string.isRequired,
  title: PropTypes.string,
  size: PropTypes.number,
  onPress: PropTypes.func.isRequired,
  iconStyle: PropTypes.instanceOf(Object),
  containerStyle: PropTypes.instanceOf(Object),
  loading: PropTypes.bool,
};

IconButton.defaultProps = {
  size: 50,
  title: null,
  iconStyle: {},
  containerStyle: {},
  loading: false,
};

export default IconButton;
