import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Text, Container, Touchable } from './UI';
import styles from './Styles/BackCardStyle';
import Icon from './CustomIcon';

const BackCard = ({ onDelete, onEdit }) => {
  return (
    <Container style={styles.container}>
      <Touchable style={styles.left} onPress={onDelete}>
        <Icon name="ic_edit" size={40} style={styles.iconEdit} />
      </Touchable>
      <Container style={{ flex: 1 }} />
      <Touchable style={styles.right} onPress={onEdit}>
        <Icon name="ic_trash" size={40} style={styles.iconDelete} />
      </Touchable>
    </Container>
  );
};

BackCard.propTypes = {
  onDelete: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
};

BackCard.defaultProps = {};

export default BackCard;
