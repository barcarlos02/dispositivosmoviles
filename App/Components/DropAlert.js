import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import DropdownAlert from 'react-native-dropdownalert';
import DropdownAlertHolder from '../Utils/DropdownAlertHolder';
import { DropdownAlertProvider } from '../Context/DropdownAlert';
import styles from './Styles/DropAlertStyle';
import { Colors } from '../Themes';
import { Container } from './UI';

const DropAlert = ({ children }) => {
  const setDropdownAlertRef = ref => {
    DropdownAlertHolder.setDropDown(ref);
  };

  const showAlert = (type, title, message) => {
    DropdownAlertHolder.alert(type, title, message);
  };

  const context = {
    showAlert,
  };

  return (
    <Container style={styles.container}>
      <DropdownAlertProvider value={context}>
        {children}
        <DropdownAlert
          ref={setDropdownAlertRef}
          inactiveStatusBarBackgroundColor={Colors.darkBackground}
        />
      </DropdownAlertProvider>
    </Container>
  );
};

// Prop type warnings
DropAlert.propTypes = {
  children: PropTypes.node.isRequired,
};

// Defaults for props
DropAlert.defaultProps = {};

export default DropAlert;
