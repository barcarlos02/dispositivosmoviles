import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Text, Container, Touchable } from './UI';
import styles from './Styles/MindfulnessStepStyle';

const renderHeader = (titles, selectedTitle, onCategorySelected) => {
  // render ingredients,source category
  return titles.map((category, key) => {
    if (selectedTitle === key)
      return (
        <Container key={Math.random()} style={[styles.titleSelected, { marginLeft: 10 }]}>
          <Touchable onPress={() => onCategorySelected(key)}>
            <Text style={styles.category}>{category.toUpperCase()}</Text>
          </Touchable>
        </Container>
      );
    return (
      <Container key={Math.random()} style={{ marginLeft: 10 }}>
        <Touchable onPress={() => onCategorySelected(key)}>
          <Text style={styles.category}>{category.toUpperCase()}</Text>
        </Touchable>
      </Container>
    );
  });
};

const MindfulnessStep = ({ category, description, categorySelected, onCategorySelected }) => {
  return (
    <Container style={styles.container}>
      <Container style={styles.titleContainer}>
        {renderHeader(category, categorySelected, onCategorySelected)}
      </Container>
      {description !== '' && <Text style={styles.description}>{description}</Text>}
    </Container>
  );
};

// Prop type warnings
MindfulnessStep.propTypes = {
  category: PropTypes.arrayOf(PropTypes.string),
  description: PropTypes.string,
  categorySelected: PropTypes.number,
  onCategorySelected: PropTypes.func,
};

// Defaults for props
MindfulnessStep.defaultProps = {
  description: '',
  category: [],
  categorySelected: 0,
  onCategorySelected: () => {},
};

export default MindfulnessStep;
