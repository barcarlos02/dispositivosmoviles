import React from 'react';
import PropTypes from 'prop-types';
import { map } from 'lodash';
import { FlatList } from 'react-native';
import { Text, Container, Spinner, Touchable, Content } from './UI';
import styles from './Styles/PlacesModalStyle';
import { Colors } from '../Themes';
import Modal from './Modal';
import Convert from '../Transforms/ConvertDistanceToKm';

const renderPlaces = (places, onSelect) => {
  return map(places, (place, id) => {
    const distance = Convert(Number(place.distance));
    return (
      <Touchable
        style={styles.row}
        onPress={() => {
          onSelect(id);
        }}
        key={id}
      >
        <Container style={{ flex: 1 }}>
          <Container style={styles.rowContainer}>
            <Text style={styles.leftText}>{place.name}</Text>
            <Text style={styles.rightText}>{`${distance.distance} ${distance.unity}`}</Text>
          </Container>
          <Text style={styles.tinyText}>{place.types[0]}</Text>
        </Container>
      </Touchable>
    );
  });
};

const PlaceHeader = ({ places, fetching, visible, onClose, onSelect }) => {
  return (
    <Modal
      visible={visible}
      top={100}
      type="modal"
      onClose={onClose}
      style={styles.modal}
      title="Select a place"
    >
      <Container style={styles.mainContainer}>
        <Content style={styles.container}>
          {!!fetching && <Container>{renderPlaces(places, onSelect)}</Container>}
        </Content>
      </Container>
    </Modal>
  );
};

// Prop type warnings
PlaceHeader.propTypes = {
  places: PropTypes.instanceOf(Object),
  fetching: PropTypes.bool,
  visible: PropTypes.bool,
  onClose: PropTypes.func,
  onSelect: PropTypes.func,
};

// Defaults for props
PlaceHeader.defaultProps = {
  places: [],
  fetching: true,
  visible: false,
  onClose: () => {},
  onSelect: () => {},
};

export default PlaceHeader;
