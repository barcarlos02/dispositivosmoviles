import React from 'react';
import PropTypes from 'prop-types';
import Menu, { MenuOptions, MenuTrigger, MenuProvider } from 'react-native-popup-menu';
import { map } from 'lodash';
import { Image } from 'react-native-elements';
import { FlatList } from 'react-native';
import PopupItem from './ItemPopupMenu';
import PopupMenuAnimated from './PopupMenuAnimated';
import styles, { triggerStyles } from './Styles/HeaderMenuStyle';
import { Content, Text, Container } from './UI';

function getConfig(position) {
  let config;
  switch (position) {
    case 'left':
      config = {
        type: 'profile',
        menu: 'leftMenu',
        options: 'leftMenuOptions',
      };
      break;
    case 'center':
      config = {
        type: 'filter',
        menu: 'centerMenu',
        options: 'centerMenuOptions',
      };
      break;
    default:
      config = {
        type: 'options',
        menu: 'rightMenu',
        options: 'rightMenuOptions',
      };
  }
  return config;
}

function renderItems(position, items, menuConfig) {
  let renderItem;
  switch (position) {
    case 'center':
      renderItem = (
        <Container>
          <Container>
            <Text style={styles.mainText}>Show me:</Text>
            <FlatList
              horizontal
              keyExtractor={item => ''.concat(Math.random())}
              data={items}
              renderItem={({ item }) => <PopupItem type={menuConfig.type} {...item} />}
            />
          </Container>
        </Container>
      );
      break;
    case 'left':
      renderItem = (
        <Container style={styles.container}>
          <Container style={styles.container}>
            {map(items, (item, key) => (
              <PopupItem key={key} type={menuConfig.type} {...item} />
            ))}
          </Container>
          {/* <Container>
            {/* <Image
              source={require('../Images/boostCardImg.jpg')}
              style={{ width: 100, height: 100 }}
            /> */}
          {/* </Container>  */}
        </Container>
      );
      break;
    case 'right':
      renderItem = map(items, (item, key) => (
        <PopupItem key={key} type={menuConfig.type} {...item} />
      ));
      break;
    default:
      renderItem = null;
      break;
  }
  return renderItem;
}

const HeaderMenu = ({ position, items, onRef }) => {
  const menuConfig = getConfig(position);
  const renderItem = renderItems(position, items, menuConfig);
  return (
    <Menu ref={onRef} renderer={props => <PopupMenuAnimated {...props} type={menuConfig.type} />}>
      <MenuTrigger text="Select option" disabled customStyles={triggerStyles} />
      <MenuOptions optionsContainerStyle={[styles[menuConfig.options]]}>
        <Content style={[styles.scrollContainer, styles[menuConfig.menu]]}>{renderItem}</Content>
      </MenuOptions>
    </Menu>
  );
};

// Prop type warnings
HeaderMenu.propTypes = {
  position: PropTypes.string.isRequired,
  items: PropTypes.instanceOf(Array).isRequired,
  onRef: PropTypes.func.isRequired,
};

// Defaults for props
HeaderMenu.defaultProps = {};

export default HeaderMenu;
