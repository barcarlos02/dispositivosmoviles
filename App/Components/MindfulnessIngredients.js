import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image } from 'react-native';
import { map, isEqual } from 'lodash';
import { Text, Touchable, Container } from './UI';
import { Images } from '../Themes';
import styles from './Styles/MindfulnessIngredientsStyle';

const ingredients = {
  plants: {
    img: Images.ingredients.plants,
    value: 'plants',
    label: 'plants',
  },
  grains: {
    img: Images.ingredients.grains,
    value: 'grains',
    label: 'grains',
  },
  eggs: {
    img: Images.ingredients.eggs,
    value: 'eggs',
    label: 'eggs',
  },
  birds: {
    img: Images.ingredients.birds,
    value: 'birds',
    label: 'birds',
  },
  fish: {
    img: Images.ingredients.fish,
    value: 'fish',
    label: 'fish',
  },
  pork: {
    img: Images.ingredients.pork,
    value: 'pork',
    label: 'pork',
  },
  diary: {
    img: Images.ingredients.dairy,
    value: 'dairy',
    label: 'dairy',
  },
  beef: {
    img: Images.ingredients.beef,
    value: 'beef',
    label: 'beef',
  },
  lamb: {
    img: Images.ingredients.lamb,
    value: 'lamb',
    label: 'lamb',
  },
};

export default class MindfulnessIngredients extends Component {
  // Prop type warnings
  static propTypes = {
    onSelected: PropTypes.func.isRequired,
    ingredients: PropTypes.instanceOf(Object),
  };

  // Defaults for props
  static defaultProps = {
    ingredients: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      selected: props.ingredients,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (!isEqual(nextProps.ingredients, prevState.selected)) {
      return {
        selected: nextProps.ingredients,
      };
    }
    return null;
  }

  toggleIngredient(value) {
    let { selected } = this.state;
    if (selected[value]) {
      delete selected[value];
    } else {
      selected = {
        ...selected,
        [value]: true,
      };
    }
    this.setState({ selected }, this.props.onSelected.bind(null, selected));
  }

  _renderIngredients() {
    const { selected } = this.state;
    const ingredientItems = map(ingredients, i => {
      const isSelected = selected[i.value] === true;
      return (
        <View style={styles.ingredientItem} key={i.value}>
          <View style={styles.ingredientItemContent}>
            <Touchable
              style={[styles.ingredientItemTouchable, isSelected && styles.ingredientItemSelected]}
              onPress={this.toggleIngredient.bind(this, i.value)}
            >
              <View style={styles.ingredientItemImageWrapper}>
                <Image source={i.img} style={styles.ingredientItemImage} />
              </View>
            </Touchable>
            <Text style={styles.ingredientItemLabel}>{i.label}</Text>
          </View>
        </View>
      );
    });
    return <View style={styles.ingredientsContainer}>{ingredientItems}</View>;
  }

  render() {
    return <Container style={styles.container}>{this._renderIngredients()}</Container>;
  }
}
