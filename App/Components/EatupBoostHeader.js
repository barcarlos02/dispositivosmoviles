import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';
import { Container } from './UI';
import { Metrics } from '../Themes';
import HeaderIcon from './HeaderIcon';
import getMeal from '../Transforms/HourToMealTime';

import styles from './Styles/CommonHeaderStyle';

const renderCenter = title => {
  return <Text style={styles.title}>{title}</Text>;
};

const renderLeft = onBackButtonPress => {
  return (
    <HeaderIcon
      name="close"
      type="font-awesome"
      custom={false}
      onPress={onBackButtonPress}
      style={styles.navIconWrapper}
    />
  );
};
const EatupBoostHeader = ({ onPress, onBackButtonPress, title, mealTime }) => {
  let text;
  if (mealTime) text = getMeal();
  else text = title;
  return (
    <Container style={[styles.header]}>
      <Container style={[styles.headerItem, styles.left]}>
        {renderLeft(onBackButtonPress)}
      </Container>
      <Container style={[styles.headerItem, styles.center]}>{renderCenter(text)}</Container>
      <Container style={[styles.headerItem, styles.right]} />
    </Container>
  );
};

// Prop type warnings
EatupBoostHeader.propTypes = {
  onPress: PropTypes.func.isRequired,
  onBackButtonPress: PropTypes.func,
  title: PropTypes.string,
  mealTime: PropTypes.bool,
};

// Defaults for props
EatupBoostHeader.defaultProps = {
  onBackButtonPress: () => {},
  title: '',
  mealTime: false,
};

export default EatupBoostHeader;
