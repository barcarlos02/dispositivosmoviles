import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Text, Container } from './UI';
import styles from './Styles/JumbotronStyle';

const Jumbotron = ({ title, titleStyles, containerStyles }) => {
  return (
    <Container style={[styles.container, containerStyles]}>
      <Text style={[styles.title, titleStyles]}>{title}</Text>
    </Container>
  );
};

// Prop type warnings
Jumbotron.propTypes = {
  title: PropTypes.string,
  titleStyles: PropTypes.instanceOf(Object),
  containerStyles: PropTypes.instanceOf(Object),
};

// Defaults for props
Jumbotron.defaultProps = {
  title: 'Ups... There was an error',
  titleStyles: {},
  containerStyles: {},
};

export default Jumbotron;
