import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { Animated, Easing } from 'react-native';
import { Metrics } from '../Themes';
import styles from './Styles/PopupMenuAnimatedStyle';

export default class PopupMenuAnimated extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slide: new Animated.Value(0),
      fade: new Animated.Value(0),
    };
  }

  componentDidMount() {
    Animated.parallel(
      [
        Animated.timing(this.state.slide, {
          duration: 400,
          toValue: 1,
          easing: Easing.in(Easing.cubic),
          useNativeDriver: true,
        }),
        Animated.timing(this.state.fade, {
          duration: 600,
          toValue: 1,
          easing: Easing.in(Easing.cubic),
          useNativeDriver: true,
        }),
      ],
      { stopTogether: false }
    ).start();
  }

  componentWillUnmount() {}

  getAnimation() {
    const { type } = this.props;
    if (type === 'profile') {
      return {
        transform: [
          {
            translateX: this.state.slide.interpolate({
              inputRange: [0, 1],
              outputRange: [-Metrics.screenWidth, 0],
            }),
          },
        ],
        opacity: this.state.fade,
      };
    }
    if (type === 'filter') {
      return {
        transform: [
          {
            translateY: this.state.slide.interpolate({
              inputRange: [0, 1],
              outputRange: [-Metrics.navBarHeight, 0],
            }),
          },
        ],
        opacity: this.state.fade,
      };
    }
    return {
      transform: [
        {
          translateX: this.state.slide.interpolate({
            inputRange: [0, 1],
            outputRange: [Metrics.screenWidth, 0],
          }),
        },
      ],
      opacity: this.state.fade,
    };
  }

  close() {
    return new Promise(resolve => {
      Animated.parallel(
        [
          Animated.timing(this.state.slide, {
            duration: 400,
            toValue: 0,
            easing: Easing.out(Easing.cubic),
            useNativeDriver: true,
          }),
          Animated.timing(this.state.fade, {
            duration: 600,
            toValue: 0,
            easing: Easing.out(Easing.cubic),
            useNativeDriver: true,
          }),
        ],
        { stopTogether: false }
      ).start(resolve);
    });
  }

  render() {
    const { style, children, layouts, type, ...other } = this.props;
    const animation = this.getAnimation();
    const position =
      type === 'profile' ? { top: 0, left: 0 } : { top: Metrics.navBarHeight, left: 0 };
    return (
      <Animated.View style={[styles.options, style, animation, position]} {...other}>
        {children}
      </Animated.View>
    );
  }
}
