import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { PanResponder, View } from 'react-native';
import Svg, { Circle, G } from 'react-native-svg';
import Color from 'color';
import { Colors } from '../Themes';
import { Container } from './UI';
import styles from './Styles/CircularSliderStyle';

const alphaWhite = Color('#FFF')
  .alpha(0.3)
  .toString();

export default class CircularSlider extends Component {
  // Prop type warnings
  static propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    onValueChange: PropTypes.func,
    meterColor: PropTypes.string,
    dotColor: PropTypes.string,
    value: PropTypes.number,
    children: PropTypes.node,
  };

  // Defaults for props
  static defaultProps = {
    width: 200,
    height: 200,
    onValueChange: () => {},
    meterColor: Colors.leaf,
    dotColor: Colors.accent,
    value: 0,
    children: null,
  };

  constructor(props) {
    super(props);
    this.handlePanResponderMove = this.handlePanResponderMove.bind(this);
    this.cartesianToPolar = this.cartesianToPolar.bind(this);
    this.polarToCartesian = this.polarToCartesian.bind(this);
    const { width, height, value } = props;
    const smallestSide = Math.min(width, height);
    this.state = {
      cx: width / 2,
      cy: height / 2,
      r: (smallestSide / 2) * 0.85,
      currentValue: value,
    };
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponder: () => true,
      onPanResponderMove: this.handlePanResponderMove,
    });
  }

  componentDidMount() {
    this.props.onValueChange(this.state.currentValue);
  }

  componentDidUpdate(prevProps) {
    if (this.props.value !== prevProps.value) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState(
        {
          currentValue: this.props.value,
        },
        () => this.props.onValueChange(this.state.currentValue)
      );
    }
  }

  polarToCartesian(angle) {
    const { cx, cy, r } = this.state;
    const a = ((angle - 270) * Math.PI) / 180.0;
    const x = cx + r * Math.cos(a);
    const y = cy + r * Math.sin(a);
    return { x, y };
  }

  cartesianToPolar(x, y) {
    const { cx, cy } = this.state;
    return Math.round(Math.atan((y - cy) / (x - cx)) / (Math.PI / 180) + (x > cx ? 270 : 90));
  }

  handlePanResponderMove({ nativeEvent: { locationX, locationY } }) {
    const currentValue = this.cartesianToPolar(locationX, locationY);
    this.setState({ currentValue }, () => this.props.onValueChange(currentValue));
  }

  render() {
    const { width, height, meterColor, dotColor, children } = this.props;
    const { cx, cy, r, currentValue } = this.state;
    const endCoord = this.polarToCartesian(currentValue);
    const childSize = {
      width: width / 2,
      height: height / 2,
    };
    return (
      <Container style={styles.container}>
        <Svg onLayout={this.onLayout} width={width} height={height}>
          <Circle cx={cx} cy={cy} r={r} stroke={meterColor} strokeWidth={10} fill="none" />
          <G x={endCoord.x - 7.5} y={endCoord.y - 7.5}>
            <Circle cx={7.5} cy={7.5} r={15} fill={dotColor} {...this._panResponder.panHandlers} />
            <Circle cx={14} cy={5} r={5} fill={alphaWhite} />
          </G>
        </Svg>
        <View
          style={[
            styles.childrenContainer,
            {
              width: childSize.width,
              height: childSize.height,
              transform: [
                { translateX: (childSize.width / 2) * -1 },
                { translateY: (childSize.height / 2) * -1 },
              ],
            },
          ]}
        >
          {children}
        </View>
      </Container>
    );
  }
}
