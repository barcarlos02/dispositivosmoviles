import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as Animatable from 'react-native-animatable';
import { Container, Text, Button } from './UI';
import styles from './Styles/VanishingButtonStyle';
import CustomIcon from './CustomIcon';

export default class VanishingButton extends Component {
  // // Prop type warnings
  static propTypes = {
    vanish: PropTypes.func.isRequired,
    onPress: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    shouldAnimate: PropTypes.bool.isRequired,
  };

  //
  // // Defaults for props
  // static defaultProps = {
  //   itemName: 'Eatups',
  // };

  componentDidMount() {
    const { vanish } = this.props;
    this.view.fadeInLeft(800).then(() => {
      this.outTimer = setTimeout(() => {
        if (this.props.shouldAnimate) {
          this.view.fadeOutLeft(800).then(() => {
            vanish();
          });
        }
      }, 3500);
    });
  }

  render() {
    const { title, onPress, vanish } = this.props;
    return (
      <Animatable.View
        ref={ref => {
          this.view = ref;
        }}
        style={styles.container}
      >
        <Button
          title={title}
          titleStyle={styles.btnLeft}
          buttonStyle={styles.btn}
          raised
          onPress={() => {
            clearTimeout(this.outTimer);
            vanish();
            onPress();
          }}
        />
      </Animatable.View>
    );
  }
}
