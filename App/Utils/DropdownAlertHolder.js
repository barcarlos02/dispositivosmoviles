export default class DropDownHolder {
  static dropDown;

  static setDropDown(ref) {
    this.dropDown = ref;
  }

  static alert(type, title, message) {
    this.dropDown.alertWithType(type, title, message);
  }
}
