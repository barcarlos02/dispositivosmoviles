const FILTERS = [
  {
    title: 'Worlds',
    icon: false, // 'ic_logo_eatups',
    iconType: 'ic_logo_eatups',
    isIconButton: true,
    value: 'WORLD_EATUPS',
    key: 'world_eatups',
    field: 'eatup',
    newItemsAlert: 'New Eatups Added',
  },
  {
    title: 'Mine',
    icon: false, // 'ic_offer_label_business_eatups',
    iconType: 'ic_avatar_business_default',
    value: 'MY_EATUPS',
    key: 'my_eatups',
    field: 'eatup',
    newItemsAlert: 'New Eatups Added',
  },
  {
    title: 'Offers',
    icon: false, // 'ic_offer_label_business_eatups',
    iconType: 'ic_offer_label_business_eatups',
    value: 'MY_BOOSTS',
    key: 'my_boosts',
    field: 'boost',
    newItemsAlert: 'New Boosts Added',
  },
  {
    title: 'Saved',
    icon: false, // 'ic_offer_label_business_eatups',
    iconType: 'ic_offer_label_business_eatups',
    value: 'MY_BOOSTS',
    key: 'my_boosts',
    field: 'boost',
    newItemsAlert: 'New Boosts Added',
  },
];

export const FILTERS_OBJECT = FILTERS.reduce((result, current) => {
  result[current.value] = current;
  return result;
}, {});

export default FILTERS;
