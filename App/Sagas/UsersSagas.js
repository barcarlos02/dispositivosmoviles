/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { call, put, select } from 'redux-saga/effects';
import UsersActions, { UsersSelectors } from '../Redux/UsersRedux';

export function* getUser(api, { data: id }) {
  // get current data from Store
  const users = yield select(UsersSelectors.getPayload);
  let userData = {};
  if (users[id]) {
    userData = users[id];
    yield put(UsersActions.usersSuccess(id, users[id]));
  } else {
    // make the call to the api
    const response = yield call(api.getUser, id);
    // success?
    if (response.ok) {
      userData = response.data;
      yield put(UsersActions.usersSuccess(id, response.data));
    } else {
      yield put(UsersActions.usersFailure(id));
    }
  }
  return userData;
}
