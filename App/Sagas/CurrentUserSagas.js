/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { call, put, take } from 'redux-saga/effects';
import { NavigationActions } from 'react-navigation';
import CurrentUserActions from '../Redux/CurrentUserRedux';
import ProfileStatusActions from '../Redux/ProfileStatusRedux';

export function* getCurrentUser(api, action) {
  yield put(CurrentUserActions.currentUserCreate());
  const newChannel = yield call(api.getUserDataAuth);
  while (true) {
    try {
      const response = yield take(newChannel);
      if (response.ok) {
        yield put(CurrentUserActions.currentUserSuccess(response.data));
        yield put(ProfileStatusActions.profileStatusRequest(response.data));
      } else {
        throw new Error('Something went wrong');
      }
    } catch (error) {
      yield put(NavigationActions.navigate({ routeName: 'IntroScreen' }));
      yield put(CurrentUserActions.currentUserFailure());
    }
  }
}
