/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { take, put, race } from 'redux-saga/effects';
import CameraActions from '../Redux/CameraRedux';
import { SubmitBoostTypes } from '../Redux/SubmitBoostRedux';
import { CreateEatupTypes } from '../Redux/CreateEatupRedux';
// import { CameraCacheSelectors } from '../Redux/CameraCacheRedux'

export function* cleanCache() {
  const response = yield race({
    boostSuccess: take(SubmitBoostTypes.SUBMIT_BOOST_SUCCESS),
    boostFailure: take(SubmitBoostTypes.SUBMIT_BOOST_FAILURE),
    eatupSuccess: take(CreateEatupTypes.CREATE_EATUP_SUCCESS),
    eatupFailure: take(CreateEatupTypes.CREATE_EATUP_FAILURE),
  });
  if (response) {
    yield put(CameraActions.cleanCameraPayload());
  }
}
