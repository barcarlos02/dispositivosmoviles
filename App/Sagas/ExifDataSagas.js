/* ***********************************************************
 This Saga will extract the geolocation of an image chosen from gallery or will get 
 the device geolocation in cases when the image is taken from the RNCamera because  geolocation is not attached
 in this case  
 ************************************************************ */

import { put, select } from 'redux-saga/effects';
import Geolocation from 'react-native-geolocation-service';
import Exif from 'react-native-exif';
import ExifDataActions, { ExifDataSelectors } from '../Redux/ExifDataRedux';
import DropdownAlertHolder from '../Utils/DropdownAlertHolder';
import PlacesActions from '../Redux/PlacesRedux';

const getLocationFromDevice = () => {
  return new Promise((resolve, reject) => {
    Geolocation.getCurrentPosition(
      position => {
        const { coords } = position;
        resolve({ latitude: coords.latitude, longitude: coords.longitude, error: false });
      },
      error => {
        reject({ error: true, type: error.code });
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  });
};

const coorConverter = (coordenate, direction) => {
  // coordente is in a format:  21/1,54/1,116436/10000
  const dms = coordenate.split(',');
  let degrees = dms[0];
  let minutes = dms[1];
  let seconds = dms[2];
  degrees = degrees.split('/');
  minutes = minutes.split('/');
  seconds = seconds.split('/');
  let dd =
    Number(degrees[0]) / Number(degrees[1]) +
    Number(minutes[0]) / Number(minutes[1]) / 60 +
    Number(seconds[0]) / Number(seconds[1]) / 3600;
  if (direction === 'S' || direction === 'W') {
    dd *= -1;
  }
  return dd;
};

const extractGeolocationFromImage = uri => {
  return new Promise((resolve, reject) => {
    Exif.getExif(uri)
      .then(msg => {
        const { GPSLatitude, GPSLongitude, GPSLongitudeRef, GPSLatitudeRef } = msg.exif;
        if (GPSLatitude && GPSLongitude) {
          const latitude = coorConverter(GPSLatitude, GPSLatitudeRef);
          const longitude = coorConverter(GPSLongitude, GPSLongitudeRef);
          resolve({ latitude, longitude, error: false });
        }
        resolve({ error: true });
      })
      .catch(msg => reject({ error: true, type: msg }));
  });
};

export function* getExifData(action) {
  const { data } = action;
  const { source } = data;
  const firstTime = yield select(ExifDataSelectors.getFirstTime); // Is this the first picture, we only need one location
  if (firstTime) {
    let location = null;
    if (source === 'camera') {
      location = yield getLocationFromDevice();
    } else {
      location = yield extractGeolocationFromImage(data.uri);
    }
    const { error } = location;
    if (error) {
      yield put(ExifDataActions.exifDataFailure());
      DropdownAlertHolder.alert(
        'error',
        'Uh Oh...',
        'Something went wrong trying to get the location'
      );
    } else {
      yield put(ExifDataActions.exifDataSuccess({ location, uri: data.uri }));
      // calling the places near the location given
      yield put(
        PlacesActions.placesRequest({
          coords: { lat: location.latitude, lng: location.longitude },
          type: 'restaurant',
        })
      );
    }
  }
}
