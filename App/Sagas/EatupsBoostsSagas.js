/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { call, put, select, take, race, cancel, cancelled, all } from 'redux-saga/effects';
import EatupsBoostsActions from '../Redux/EatupsBoostsRedux';
import EatupBoostActions from '../Redux/EatupBoostRedux';
import { EatupBoostFilterSelectors } from '../Redux/EatupBoostFilterRedux';
import CurrentPageActions, { CurrentPageSelectors } from '../Redux/CurrentPageRedux';
import EatupsBoostsUpdatesActions, {
  EatupsBoostsUpdatesTypes,
} from '../Redux/EatupsBoostsUpdatesRedux';
import ConvertObjectToArray from '../Transforms/ConvertArrayToObject';
import { FILTERS_OBJECT as filters } from '../Utils/EatupBoostFilter';

function* openChannel(api) {
  // get current filter from Store
  const currentFilter = yield select(EatupBoostFilterSelectors.getPayload);
  // make the call to the api
  const channel = yield call(api.addedEatupsBoosts, currentFilter);
  try {
    while (true) {
      const { closed, response } = yield race({
        closed: take(EatupsBoostsUpdatesTypes.EATUPS_BOOSTS_UPDATES_CLOSE),
        response: take(channel),
      });
      if (closed) {
        yield cancel();
      } else if (!!response && response.ok) {
        const { data } = response;
        if (data.length !== 0 && data.length % 10 === 0) {
          yield put(EatupsBoostsUpdatesActions.eatupsBoostsUpdatesSuccess(true));
        }
      } else {
        yield put(EatupsBoostsUpdatesActions.eatupsBoostsUpdatesFailure());
      }
    }
  } finally {
    if (yield cancelled()) {
      try {
        channel.close();
      } catch (e) {}
    }
  }
}

function* closeChannel() {
  // Close previous channel
  yield put(EatupsBoostsUpdatesActions.eatupsBoostsUpdatesClose());
}

export function* fetchPage(api) {
  try {
    yield put(EatupsBoostsActions.eatupsBoostsRequest());
    const { page } = yield select(CurrentPageSelectors.getPagingInfo);
    if (page === 0) {
      yield put(EatupsBoostsActions.cleanEatupsBoosts());
    }
    const currentFilter = yield select(EatupBoostFilterSelectors.getPayload);
    const response = yield call(api.getEatupsBoosts, currentFilter);
    if (response.ok) {
      const { data } = response;
      const { field } = filters[currentFilter];
      const dataObject = ConvertObjectToArray(data, field, true);
      yield put(EatupsBoostsActions.eatupsBoostsSuccess(dataObject));
      if (data.length < 3) {
        yield put(EatupsBoostsActions.setHasMore());
      }
      if (page === 0) {
        yield put(EatupsBoostsUpdatesActions.eatupsBoostsUpdatesRequest());
      }
      // Dispatch action to fetch single items
      yield all(
        data.map(item =>
          put(
            EatupBoostActions.eatupBoostRequest({
              id: item[field],
              type: currentFilter,
            })
          )
        )
      );
    } else {
      yield put(EatupsBoostsActions.eatupsBoostsFailure());
    }
  } catch (e) {
    yield put(EatupsBoostsActions.eatupsBoostsFailure());
  }
}

export function* fetchData() {
  yield put(CurrentPageActions.setCurrentPage(0));
}

export function* fetchUpdates(api) {
  let sagas = [];
  sagas = [closeChannel, openChannel];
  // eslint-disable-next-line no-restricted-syntax
  for (const saga of sagas) {
    yield call(saga, api);
  }
}
