/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { call, put, take } from 'redux-saga/effects';
import { NavigationActions } from 'react-navigation';
import AuthActions from '../Redux/AuthRedux';

export function* checkAuth(api, action) {
  const channel = yield call(api.getAuthChannel);
  while (true) {
    try {
      const response = yield take(channel);
      if (response.ok) {
        yield put(AuthActions.authSuccess());
      } else {
        throw new Error('Something went wrong');
      }
    } catch (error) {
      yield put(NavigationActions.navigate({ routeName: 'IntroScreen' }));
      yield put(AuthActions.authFailure());
    }
  }
}
