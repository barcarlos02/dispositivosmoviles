import { takeLatest, takeEvery, all } from 'redux-saga/effects';
import API from '../Services/Api';
import FirebaseApi from '../Services/FirebaseApi';
import FixtureAPI from '../Services/FixtureApi';
import DebugConfig from '../Config/DebugConfig';

/* ------------- Types ------------- */

import { TermsTypes } from '../Redux/TermsRedux';
import { LoginTypes } from '../Redux/LoginRedux';
import { AuthTypes } from '../Redux/AuthRedux';
import { AppStatusTypes } from '../Redux/AppStatusRedux';
import { SubmitProfileTypes } from '../Redux/SubmitProfileRedux';
import { CurrentProfileTypes } from '../Redux/CurrentProfileRedux';
import { CurrentUserTypes } from '../Redux/CurrentUserRedux';
import { LoginStatusTypes } from '../Redux/LoginStatusRedux';
import { ProfileStatusTypes } from '../Redux/ProfileStatusRedux';
import { UserProfilesTypes } from '../Redux/UserProfilesRedux';
import { UpdateCurrentProfileTypes } from '../Redux/UpdateCurrentProfileRedux';
import { LogoutTypes } from '../Redux/LogoutRedux';
import { SubmitBoostTypes } from '../Redux/SubmitBoostRedux';
import { CreateEatupTypes } from '../Redux/CreateEatupRedux';
import { UploadImageTypes } from '../Redux/UploadImageRedux';
import { CameraTypes } from '../Redux/CameraRedux';
import { EatupBoostFilterTypes } from '../Redux/EatupBoostFilterRedux';
import { EatupBoostTypes } from '../Redux/EatupBoostRedux';
import { UsersTypes } from '../Redux/UsersRedux';
import { ProfilesTypes } from '../Redux/ProfilesRedux';
import { CurrentPageTypes } from '../Redux/CurrentPageRedux';
import { EatupsBoostsUpdatesTypes } from '../Redux/EatupsBoostsUpdatesRedux';
import { UploadLogoTypes } from '../Redux/UploadLogoRedux';
import { ImageUrlTypes } from '../Redux/ImageUrlRedux';
import { PermissionsTypes } from '../Redux/PermissionsRedux';
import { MarkersTypes } from '../Redux/MarkersRedux';
import { MarkerDetailTypes } from '../Redux/MarkerDetailRedux';
import { ExifDataTypes } from '../Redux/ExifDataRedux';
import { PlacesTypes } from '../Redux/PlacesRedux';

/* ------------- Sagas ------------- */

import { openTerms } from './TermsSagas';
import { login } from './LoginSagas';
import { checkAuth } from './AuthSagas';
import { updateActiveStatus } from './AppStatusSagas';
import { submitUser, submitBusiness } from './SubmitProfileSagas';
import { getCurrentProfile } from './CurrentProfileSagas';
import { getCurrentUser } from './CurrentUserSagas';
import { checkLoginStatus } from './LoginStatusSagas';
import { getProfileStatus } from './ProfileStatusSagas';
import { getProfiles } from './UserProfilesSagas';
import { updateCurrentProfile } from './UpdateCurrentProfileSagas';
import { logout } from './LogoutSagas';
import { submitBoost } from './SubmitBoostSagas';
import { createEatup } from './CreateEatupSagas';
import { uploadImage } from './UploadImageSagas';
import { requestCameraPermissions, requestLocationPermissions } from './RequestPermissionsSagas';
import { fetchData, fetchPage, fetchUpdates } from './EatupsBoostsSagas';
import { cleanCache } from './CameraCacheSagas';
import { getEatupBoost } from './EatupBoostSagas';
import { getUser } from './UsersSagas';
import { getProfile } from './ProfilesSagas';
import { setFilter } from './EatupBoostFilterSagas';
import { uploadLogo } from './UploadLogoSagas';
import { imageUrl } from './ImageUrlSagas';
import { getMarkers } from './MarkersSaga';
import { getMarkerDetail } from './MarkerDetailSaga';
import { getExifData } from './ExifDataSagas';
import { getPlaces } from './PlacesSagas';

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create();

/* ------------- Connect Types To Sagas ------------- */

export default function* root() {
  yield all([
    // opens terms and privacy with in-app browser
    takeLatest(TermsTypes.TERMS_REQUEST, openTerms, null),
    // allows user to login with google's account
    takeLatest(LoginTypes.LOGIN_REQUEST, login, FirebaseApi),
    // checks if auth status has changed
    takeLatest(AuthTypes.AUTH_REQUEST, checkAuth, FirebaseApi),
    // Checks the Login Status( if user is logged in)
    takeLatest(AuthTypes.AUTH_SUCCESS, checkLoginStatus, FirebaseApi),
    // Updates Active Status of the app into User's Data
    takeLatest(AppStatusTypes.APP_STATUS_UPDATE_ACTIVE, updateActiveStatus, FirebaseApi),
    // Submits Users's profile info
    takeLatest(SubmitProfileTypes.SUBMIT_PROFILE_USER, submitUser, FirebaseApi),
    // Submits Business's profile info
    takeLatest(SubmitProfileTypes.SUBMIT_PROFILE_BUSINESS, submitBusiness, FirebaseApi),
    // Gets Current profile's info from dispatched request
    takeLatest(CurrentProfileTypes.CURRENT_PROFILE_REQUEST, getCurrentProfile, FirebaseApi),
    // Gets Current profile's info from auth sagas sequence
    takeLatest(ProfileStatusTypes.PROFILE_STATUS_SUCCESS, getCurrentProfile, FirebaseApi),
    // Gets User's Data from dispatched request
    takeLatest(CurrentUserTypes.CURRENT_USER_REQUEST, getCurrentUser, FirebaseApi),
    // Gets User's Data from auth sagas sequence
    takeLatest(LoginStatusTypes.LOGIN_STATUS_SUCCESS, getCurrentUser, FirebaseApi),
    // If it's not logged in, perform a logout action
    takeLatest(LoginStatusTypes.LOGIN_STATUS_FAILURE, logout, FirebaseApi),
    // Gets the current profile status ( if it has already been setted up)
    takeLatest(ProfileStatusTypes.PROFILE_STATUS_REQUEST, getProfileStatus, FirebaseApi),
    // Gets all the profiles from the current user
    takeLatest(UserProfilesTypes.USER_PROFILES_REQUEST, getProfiles, FirebaseApi),
    // Updates the user's current profile
    takeLatest(
      UpdateCurrentProfileTypes.UPDATE_CURRENT_PROFILE_REQUEST,
      updateCurrentProfile,
      FirebaseApi
    ),
    // Logout from the app
    // Logoff the user from the session
    takeLatest(LogoutTypes.LOGOUT_REQUEST, logout, FirebaseApi),
    // Create a new Eatup
    takeLatest(CreateEatupTypes.CREATE_EATUP_REQUEST, createEatup, FirebaseApi),
    // Upload an image
    takeLatest(UploadImageTypes.UPLOAD_IMAGE_REQUEST, uploadImage, null),
    // Create a new boost
    takeLatest(SubmitBoostTypes.SUBMIT_BOOST_REQUEST, submitBoost, FirebaseApi),
    // Request basic permissions to use the camera
    takeLatest(PermissionsTypes.PERMISSIONS_CAMERA_REQUEST, requestCameraPermissions, null),
    // Request basic permissions to use the geolocation
    takeLatest(PermissionsTypes.PERMISSIONS_LOCATION_REQUEST, requestLocationPermissions, null),
    // Cleans camera cache after boost submit has been succesful or failure
    takeLatest(SubmitBoostTypes.SUBMIT_BOOST_REQUEST, cleanCache, null),
    // Cleans camere cache after eatup submit has been succesful or failure
    takeLatest(CreateEatupTypes.CREATE_EATUP_REQUEST, cleanCache, null),
    // Fetch Data based on Eatups Boosts Filter
    takeLatest(EatupBoostFilterTypes.SET_EATUP_BOOST_FILTER, fetchData, null),
    // Fetch data for single eatup/boost
    takeEvery(EatupBoostTypes.EATUP_BOOST_REQUEST, getEatupBoost, FirebaseApi),
    // Fetch user
    takeEvery(UsersTypes.USERS_REQUEST, getUser, FirebaseApi),
    // Fetch Profile
    takeEvery(ProfilesTypes.PROFILES_REQUEST, getProfile, FirebaseApi),
    // Set eatups/boosts filter from when user is set
    takeLatest(CurrentProfileTypes.CURRENT_PROFILE_SUCCESS, setFilter, null),
    // Updates when scroll reaches last item
    takeLatest(CurrentPageTypes.SET_CURRENT_PAGE, fetchPage, FirebaseApi),
    // Creates a new Channel once the EtupsBoosts feed has been updated
    takeLatest(EatupsBoostsUpdatesTypes.EATUPS_BOOSTS_UPDATES_REQUEST, fetchUpdates, FirebaseApi),
    // Upload a new logo for a business profile
    takeLatest(UploadLogoTypes.UPLOAD_LOGO_REQUEST, uploadLogo, FirebaseApi),
    // get an picture URL from the api
    takeEvery(ImageUrlTypes.IMAGE_URL_REQUEST, imageUrl, FirebaseApi),
    // Getting markers from api
    takeLatest(MarkersTypes.MARKERS_REQUEST, getMarkers, api),
    // get all the eatups and boost in a marker
    takeLatest(MarkerDetailTypes.MARKER_DETAIL_REQUEST, getMarkerDetail, api),
    // get Location data of the image
    takeLatest(ExifDataTypes.EXIF_DATA_REQUEST, getExifData),
    // fetch places near location given by device or image
    takeLatest(PlacesTypes.PLACES_REQUEST, getPlaces, null),
  ]);
}
