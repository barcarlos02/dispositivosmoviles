/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { call, put, select } from 'redux-saga/effects';
import ProfilesActions, { ProfilesSelectors } from '../Redux/ProfilesRedux';

export function* getProfile(api, { data: id }) {
  // get current data from Store
  const users = yield select(ProfilesSelectors.getPayload);
  let profileData = {};
  if (users[id]) {
    profileData = users[id];
    yield put(ProfilesActions.profilesSuccess(id, users[id]));
  } else {
    // make the call to the api
    const response = yield call(api.getProfileData, id);
    // success?
    if (response.ok) {
      profileData = response.data;
      yield put(ProfilesActions.profilesSuccess(id, response.data));
    } else {
      yield put(ProfilesActions.profilesFailure(id));
    }
  }
  return profileData;
}
