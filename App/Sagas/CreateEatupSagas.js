/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { call, put, race, take, select } from 'redux-saga/effects';
import CreateEatupActions from '../Redux/CreateEatupRedux';
import CurrentPageActions from '../Redux/CurrentPageRedux';
import UploadImageActions, { UploadImageTypes } from '../Redux/UploadImageRedux';
import { EatupBoostFilterSelectors } from '../Redux/EatupBoostFilterRedux';
import DropdownAlertHolder from '../Utils/DropdownAlertHolder';
import FILTER from '../Utils/EatupBoostFilter';
import CameraActions from '../Redux/CameraRedux';

function* uploadImages(images) {
  const pathArray = [];
  try {
    for (const i in images) {
      const payload = {
        photo: { uri: images[i] },
        source: 'camera',
      };
      yield put(CameraActions.setCameraPayload(payload));
      yield put(UploadImageActions.uploadImageRequest('eatups'));
      const { uploadImageSuccess, uploadImageFailure } = yield race({
        uploadImageSuccess: take(UploadImageTypes.UPLOAD_IMAGE_SUCCESS),
        uploadImageFailure: take(UploadImageTypes.UPLOAD_IMAGE_FAILURE),
      });
      const { payload: path } = uploadImageSuccess;
      pathArray.push(path);
    }
  } catch (err) {
    DropdownAlertHolder.alert(
      'error',
      'Uh Oh...',
      'Something went wrong uploading an image in for'
    );
  }
  return pathArray;
}

export function* createEatup(api, action) {
  const { data, resolve, reject } = action;
  try {
    const imagePaths = yield call(uploadImages, data.images); // upload all images
    const eatupData = {
      ...data,
      image: imagePaths[0],
      images: imagePaths,
    };
    // make the call to the api
    const response = yield call(api.createEatup, eatupData);
    // success?
    if (response.ok) {
      const currentFilter = yield select(EatupBoostFilterSelectors.getPayload);
      DropdownAlertHolder.alert('success', 'Done!', 'Your eatup has been created');
      yield call(resolve);
      yield put(CreateEatupActions.createEatupSuccess(response.data));
      if (FILTER.find(i => i.value === currentFilter).title === 'My eatups') {
        yield put(CurrentPageActions.setCurrentPage(0));
      }
    } else {
      throw new Error();
    }
  } catch (err) {
    DropdownAlertHolder.alert('error', 'Uh Oh...', 'Something went wrong');
    yield call(reject);
    yield put(CreateEatupActions.createEatupFailure());
  }
}
