/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { call, put, select, take } from 'redux-saga/effects';
import UploadImageActions from '../Redux/UploadImageRedux';
import { CameraSelectors } from '../Redux/CameraRedux';
import { storageChannel } from '../Services/FirebaseHelpers';
import DropdownAlertHolder from '../Utils/DropdownAlertHolder';

export function* uploadImage(api, { data: path }) {
  try {
    const { source, photo } = yield select(CameraSelectors.getPayload);
    let localFilePath;
    switch (source) {
      case 'camera':
        localFilePath = photo.uri;
        break;
      case 'gallery':
        localFilePath = photo.node.image.uri;
        break;
      default:
        throw new Error();
    }
    const channel = yield call(storageChannel, path, localFilePath);
    while (true) {
      const response = yield take(channel);
      const { ok, done } = response;
      if (ok) {
        if (done) {
          const { filePath } = response;
          yield put(UploadImageActions.uploadImageSuccess(filePath));
        } else {
          const { progress } = response;
          yield put(UploadImageActions.uploadImageProgress(progress));
        }
      } else {
        throw new Error();
      }
    }
  } catch (err) {
    DropdownAlertHolder.alert('error', 'Uh Oh...', 'Something went wrong uploading your image');
    yield put(UploadImageActions.uploadImageFailure());
  }
}
