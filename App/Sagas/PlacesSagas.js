/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { call, put, select } from 'redux-saga/effects';
import Secrets from 'react-native-config';
import PlacesActions, { PlacesSelectors } from '../Redux/PlacesRedux';
import AddDistance from '../Transforms/AddDistanceToPlaces';

const fetchPlaces = async ({ coords: { lat, lng }, type }) => {
  try {
    const key = `${Secrets.GOOGLE_MAPS_API_KEY}`;
    // eslint-disable-next-line no-undef
    const response = await fetch(
      `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${lat},${lng}&rankby=distance&type=${type}&key=${key}&language=es`
    );
    const data = await response.json();
    return {
      ok: true,
      data,
    };
  } catch (err) {
    return {
      ok: false,
    };
  }
};

const fetchPlacesWithRadius = async ({ coords: { lat, lng }, radius, type }) => {
  try {
    const key = `${Secrets.GOOGLE_MAPS_API_KEY}`;
    // eslint-disable-next-line no-undef
    const response = await fetch(
      `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${lat},${lng}&radius=${radius}&type=${type}&key=${key}&language=es`
    );
    const data = await response.json();
    return {
      ok: true,
      data,
    };
  } catch (err) {
    return {
      ok: false,
    };
  }
};

const fetchMorePlaces = async ({ token }) => {
  try {
    const key = `${Secrets.GOOGLE_MAPS_API_KEY}`;
    // eslint-disable-next-line no-undef
    const response = await fetch(
      `https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=${key}&pagetoken=${token}`
    );
    const data = await response.json();
    return {
      ok: true,
      data,
    };
  } catch (err) {
    return {
      ok: false,
    };
  }
};

async function fetchData(more, data) {
  let fetchFn = fetchPlaces;
  if (more) {
    fetchFn = fetchMorePlaces;
  }
  if (data.includeRadius) {
    fetchFn = fetchPlacesWithRadius;
  }
  try {
    const response = await fetchFn(data);
    if (response.ok) {
      const results = AddDistance(response.data.results || [], data);
      // const { results } = response.data;
      const token = response.data.next_page_token;
      return { results, token };
    }
    return { results: [] };
  } catch (err) {
    throw err;
  }
}

export function* getPlaces(api, action) {
  const { data } = action;
  // const token = yield select(PlacesSelectors.getToken);
  const token = null;
  try {
    const response = yield call(fetchData, token !== null, { ...data, token });
    if (response.token) {
      yield put(PlacesActions.placesTokenSuccess(response.token));
    }
    yield put(PlacesActions.placesSuccess(response.results));
  } catch (err) {
    yield put(PlacesActions.placesFailure());
  }
}
