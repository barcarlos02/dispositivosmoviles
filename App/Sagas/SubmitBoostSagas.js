/* eslint-disable require-yield */
/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { call, put, race, take, select } from 'redux-saga/effects';
import { forEach, omit, toNumber, pick } from 'lodash';
import SubmitBoostActions from '../Redux/SubmitBoostRedux';
import CurrentPageActions from '../Redux/CurrentPageRedux';
import UploadImageActions, { UploadImageTypes } from '../Redux/UploadImageRedux';
import { EatupBoostFilterSelectors } from '../Redux/EatupBoostFilterRedux';
import DropdownAlertHolder from '../Utils/DropdownAlertHolder';
import { CurrentProfileSelectors } from '../Redux/CurrentProfileRedux';
// import { SubmitBoostSelectors } from '../Redux/SubmitBoostRedux'
import FILTER from '../Utils/EatupBoostFilter';

const getPreferences = options => {
  let preferences = null;
  forEach(options, option => {
    preferences = { ...preferences, [option.value]: true };
  });
  return preferences;
};

// export function* submitBoost(api, action) {
//   const { data, resolve, reject } = action;
//   const { button, values } = data;

//   const atributes = getPreferences(values.atributes);
//   const options = getPreferences(values.options);

//   let Boost = {
//     ...omit(values, ['hasOptions']),
//     price: toNumber(values.price),
//     discount: toNumber(values.discount),
//   };
//   if (options) {
//     Boost = { ...Boost, options };
//   }
//   if (atributes) {
//     Boost = { ...Boost, atributes };
//   }
//   console.tron.log(Boost);
//   resolve(true);
// }

export function* submitBoost(api, action) {
  const { data, resolve, reject } = action;
  const { button, values } = data;

  try {
    yield put(UploadImageActions.uploadImageRequest('boosts'));
    // We check wich action comes first and we proceed accordingly
    const { uploadImageSuccess, uploadImageFailure } = yield race({
      uploadImageSuccess: take(UploadImageTypes.UPLOAD_IMAGE_SUCCESS),
      uploadImageFailure: take(UploadImageTypes.UPLOAD_IMAGE_FAILURE),
    });
    if (uploadImageFailure) {
      throw new Error();
    }
    const { payload: path } = uploadImageSuccess;

    const atributes = getPreferences(values.atributes);
    const options = getPreferences(values.options);
    const profile = yield select(CurrentProfileSelectors.getPayload);

    let Boost = {
      ...omit(values, ['hasOptions']),
      price: toNumber(values.price),
      discount: toNumber(values.discount),
      image: path,
      published: button,
    };
    if (options) {
      Boost = { ...Boost, options };
    } else {
      Boost = { ...Boost, options: { ...pick(profile, ['eatIn', 'takeaway']) } };
    }
    if (atributes) {
      Boost = { ...Boost, atributes };
    }

    // make the call to the api
    const response = yield call(api.createBoost, Boost);
    // success?
    if (response.ok) {
      const currentFilter = yield select(EatupBoostFilterSelectors.getPayload);
      DropdownAlertHolder.alert('success', 'Done!', 'Your boost has been created');
      yield put(SubmitBoostActions.submitBoostSuccess(response.data));
      yield call(resolve);
      if (FILTER.find(i => i.value === currentFilter).title === 'My boosts'){
        yield put(CurrentPageActions.setCurrentPage(0));
      }
    } else {
      throw new Error();
    }
    yield call(resolve);
  } catch (err) {
    DropdownAlertHolder.alert('error', 'Uh Oh...', 'Something went wrong');
    yield put(SubmitBoostActions.submitBoostFailure());
    yield call(reject);
  }
}
