/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { put } from 'redux-saga/effects';
import { NavigationActions } from 'react-navigation';
import { trim } from 'lodash';
import ProfileStatusActions from '../Redux/ProfileStatusRedux';
// import { ProfileStatusSelectors } from '../Redux/ProfileStatusRedux'

export function* getProfileStatus(api, action) {
  const { data } = action;
  // get current data from Store
  // const currentData = yield select(ProfileStatusSelectors.getData)
  // make the call to the api
  const accountReady = data.accountSetup;
  if (accountReady) {
    yield put(ProfileStatusActions.profileStatusSuccess(trim(data.currentProfile)));
    yield put(NavigationActions.navigate({ routeName: 'SignedIn' }));
  } else {
    yield put(ProfileStatusActions.profileStatusFailure());
    yield put(NavigationActions.navigate({ routeName: 'AccountSetup' }));
  }
}
