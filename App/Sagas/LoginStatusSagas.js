/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { put } from 'redux-saga/effects';
import { NavigationActions } from 'react-navigation';
import { GoogleSignin } from 'react-native-google-signin';
import LoginStatusActions from '../Redux/LoginStatusRedux';
// import { LoginStatusSelectors } from '../Redux/LoginStatusRedux'

const getSignStatus = () => {
  return GoogleSignin.isSignedIn();
};

export function* checkLoginStatus(api, action) {
  const isSignedIn = yield getSignStatus();

  if (isSignedIn) {
    yield put(LoginStatusActions.loginStatusSuccess());
  } else {
    yield put(NavigationActions.navigate({ routeName: 'IntroScreen' }));
    yield put(LoginStatusActions.loginStatusFailure());
  }
}
