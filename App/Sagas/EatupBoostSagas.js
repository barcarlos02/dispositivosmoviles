/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { call, put, select, all } from 'redux-saga/effects';
import EatupBoostActions from '../Redux/EatupBoostRedux';
import { EatupsBoostsSelectors } from '../Redux/EatupsBoostsRedux';
import { getUser } from './UsersSagas';
import { getProfile } from './ProfilesSagas';

export function* getEatupBoost(api, { data }) {
  const { id } = data;
  const allItems = yield select(EatupsBoostsSelectors.getPayload);
  // make the call to the api
  const response = yield call(api.getEatupBoost, data);
  // success?
  if (response.ok) {
    const { data: itemData } = response;
    const newData = {
      ...itemData,
      profile: allItems[id].profile,
    };
    // Fetch the owner of the item
    const results = yield all([
      yield call(getUser, api, { data: newData.created_by }),
      yield call(getProfile, api, { data: newData.profile }),
    ]);
    newData.ownerName = results[0].displayName;
    newData.profileName = results[1].name;
    yield put(EatupBoostActions.eatupBoostSuccess(id, newData));
  } else {
    yield put(EatupBoostActions.eatupBoostFailure(id));
  }
}
