/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { call, put } from 'redux-saga/effects';
import LoginActions from '../Redux/LoginRedux';
import DropdownAlertHolder from '../Utils/DropdownAlertHolder';

export function* login(api, action) {
  const response = yield call(api.googleLogin);
  // success?
  if (response) {
    DropdownAlertHolder.alert('success', "Heyya' Foodhero!", 'Welcome to Eatups');
    yield put(LoginActions.loginSuccess({ ...response.user.metadata, uid: response.user.uid }));
  } else {
    DropdownAlertHolder.alert('error', 'Uh Oh...', 'Something went wrong with your login');
    yield put(LoginActions.loginFailure());
  }
}
