/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { call, put, select } from 'redux-saga/effects';
import { forEach, omit, pick } from 'lodash';
import SubmitProfileActions from '../Redux/SubmitProfileRedux';
// import { SubmitProfileSelectors } from '../Redux/SubmitProfileRedux'
import CurrentUserActions, { CurrentUserSelectors } from '../Redux/CurrentUserRedux';
import CurrentProfileActions from '../Redux/CurrentProfileRedux';
import UserProfilesActions from '../Redux/UserProfilesRedux';
import FirstTimeActions from '../Redux/FirstTimeRedux';

const getPreferences = options => {
  let preferences = {};
  forEach(options, option => {
    preferences = { ...preferences, [option.value]: true };
  });
  return preferences;
};

export function* submitUser(api, action) {
  const { data, resolve, reject } = action;
  const user = yield select(CurrentUserSelectors.getUser);
  const preferences = getPreferences(data.options);
  const newFoodie = {
    owner: user.uid,
    type: 'foodie',
    ...omit(data, ['options']),
    ...preferences,
  };
  const response = yield call(api.addFoodie, newFoodie);
  if (response.ok) {
    const resp = yield call(api.getUserData);
    yield put(CurrentUserActions.currentUserSuccess(resp.data));
    yield put(CurrentProfileActions.currentProfileRequest(resp.data.currentProfile));
    yield put(SubmitProfileActions.submitProfileUserSuccess(response.data));
    yield put(UserProfilesActions.userProfilesRequest(user.uid));
    resolve(true);
  } else {
    yield put(SubmitProfileActions.submitProfileUserFailure());
    reject(false);
  }
}

export function* submitBusiness(api, action) {
  const { data, resolve, reject } = action;
  const { address } = data;
  let newAddress = {};
  if (address.geometry) {
    newAddress = {
      ...pick(address, ['formatted_address', 'place_id']),
      geometry: { ...pick(address.geometry.location, ['lat', 'lng']) },
    };
  } else {
    newAddress = { formated_address: address };
  }
  const user = yield select(CurrentUserSelectors.getUser);
  const preferences = getPreferences(data.options);
  const newBusiness = {
    owner: user.uid,
    type: 'Business',
    ...omit(data, ['options', 'address']),
    address: newAddress,
    ...preferences,
    imageURL: user.photoURL,
  };
  const response = yield call(api.addBusiness, newBusiness);
  if (response.ok) {
    const resp = yield call(api.getUserData);
    yield put(CurrentUserActions.currentUserSuccess(resp.data));
    yield put(CurrentProfileActions.currentProfileRequest(resp.data.currentProfile));
    yield put(SubmitProfileActions.submitProfileBusinessSuccess(response.data));
    yield put(UserProfilesActions.userProfilesRequest(user.uid));
    yield put(FirstTimeActions.firstTimeSuccess(resp.data.currentProfile));
    resolve(true);
  } else {
    yield put(SubmitProfileActions.submitProfileBusinessFailure());
    reject(false);
  }
}
