/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { call, put, select } from 'redux-saga/effects';
import { PermissionsAndroid, Platform } from 'react-native';
import { map } from 'lodash';
import CameraActions, { CameraSelectors } from '../Redux/CameraRedux';
import PermissionsActions, { PermissionsSelectors } from '../Redux/PermissionsRedux';
// import { RequestPermissionsSelectors } from '../Redux/RequestPermissionsRedux'

const cameraPermissions = [
  PermissionsAndroid.PERMISSIONS.CAMERA,
  PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
  PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
  PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
];
const locationPermissions = [
  PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
];

const checkCameraPermissions = () => {
  const promises = [];
  map(cameraPermissions, p => {
    promises.push(PermissionsAndroid.check(p));
  });
  return Promise.all(promises);
};

const requireCameraPermissions = () => {
  return PermissionsAndroid.requestMultiple(cameraPermissions);
};

export function* requestCameraPermissions() {
  if (Platform.OS === 'android') {
    const alreadyHasPermissions = yield select(PermissionsSelectors.hasCameraPermissions);
    if (!alreadyHasPermissions) {
      try {
        const granted = yield call(checkCameraPermissions);
        if (!granted.every(p => p)) {
          const request = yield call(requireCameraPermissions);
          if (map(request, r => r === PermissionsAndroid.RESULTS.GRANTED).every(r => r)) {
            yield put(PermissionsActions.permissionsCameraSuccess());
          } else {
            yield put(PermissionsActions.permissionsCameraFailure());
          }
        } else {
          yield put(PermissionsActions.permissionsCameraSuccess());
        }
      } catch (e) {
        yield put(PermissionsActions.permissionsCameraFailure());
      }
    } else {
      yield put(PermissionsActions.permissionsCameraSuccess());
    }
  } else {
    yield put(PermissionsActions.permissionsCameraSuccess());
  }
}

const checkLocationPermissions = () => {
  const promises = [];
  map(locationPermissions, p => {
    promises.push(PermissionsAndroid.check(p));
  });
  return Promise.all(promises);
};

const requireLocationPermissions = () => {
  return PermissionsAndroid.requestMultiple(locationPermissions);
};

export function* requestLocationPermissions() {
  if (Platform.OS === 'android') {
    const alreadyHasPermissions = yield select(PermissionsSelectors.hasLocationPermissions);
    if (!alreadyHasPermissions) {
      try {
        const granted = yield call(checkLocationPermissions);
        if (!granted.every(p => p)) {
          const request = yield call(requireLocationPermissions);
          if (map(request, r => r === PermissionsAndroid.RESULTS.GRANTED).every(r => r)) {
            yield put(PermissionsActions.permissionsLocationSuccess());
          } else {
            yield put(PermissionsActions.permissionsLocationFailure());
          }
        } else {
          yield put(PermissionsActions.permissionsLocationSuccess());
        }
      } catch (e) {
        yield put(PermissionsActions.permissionsLocationFailure());
      }
    } else {
      yield put(PermissionsActions.permissionsLocationSuccess());
    }
  } else {
    yield navigator.requestAuthorization();
    yield put(PermissionsActions.permissionsLocationSuccess());
  }
}
