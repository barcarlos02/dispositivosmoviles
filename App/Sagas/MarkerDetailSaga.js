/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { call, put } from 'redux-saga/effects';
import MarkerDetailActions from '../Redux/MarkerDetailRedux';

export function* getMarkerDetail(api, data) {
  // data is a Json with coordenate of the marker
  const coordenate = data.data;
  try {
    const response = yield call(api.getMarkerDetail, coordenate);
    if (response) {
      yield put(MarkerDetailActions.markerDetailSuccess(response.data)); // all the eatups boost fetched
    } else {
      yield put(MarkerDetailActions.markerDetailFailure());
    }
  } catch (error) {
    yield put(MarkerDetailActions.markerDetailFailure());
  }
}
