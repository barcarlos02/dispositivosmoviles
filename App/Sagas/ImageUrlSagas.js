/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { call, put } from 'redux-saga/effects';
import imageUrlActions from '../Redux/ImageUrlRedux';

export function* imageUrl(api, requestData) {
  const { data } = requestData;
  const { id } = data;
  const response = yield call(api.getUrl, data); // Data contains an uri and id
  if (response.ok) {
    const { payload } = response;
    const { imageURL } = payload;
    yield put(imageUrlActions.ImageUrlSuccess(id, imageURL));
  } else {
    yield put(imageUrlActions.ImageUrlFailure(data.id));
  }
}
