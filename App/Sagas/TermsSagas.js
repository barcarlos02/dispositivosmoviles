/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */
import Secrets from 'react-native-config';
import { call, put } from 'redux-saga/effects';
import { Alert, Linking } from 'react-native';
import InAppBrowser from 'react-native-inappbrowser-reborn';
import TermsActions from '../Redux/TermsRedux';
import { Colors } from '../Themes';

// import { TermsSelectors } from '../Redux/TermsRedux'

const OPTIONS = {
  // iOS Properties
  dismissButtonStyle: 'cancel',
  preferredBarTintColor: Colors.accent,
  preferredControlTintColor: 'white',
  readerMode: false,
  // Android Properties
  showTitle: true,
  toolbarColor: Colors.accent,
  secondaryToolbarColor: Colors.main,
  enableUrlBarHiding: true,
  enableDefaultShare: true,
  forceCloseOnRedirection: false,
  // Specify full animation resource identifier(package:anim/name)
  // or only resource name(in case of animation bundled with app).
  // animations: {
  //   startEnter: 'slide_in_right',
  //   startExit: 'slide_out_left',
  //   endEnter: 'slide_in_right',
  //   endExit: 'slide_out_left',
  // },
  // headers: {
  //   'my-custom-header': 'my custom header value',
  // },
};

const showTerms = () => {
  return new Promise(resolve => {
    const url = Secrets.TermsURL;
    Alert.alert(
      'Terms and Privacy',
      'You will be redirected to your browser to read our Terms and Privacy',
      [
        {
          text: 'Got It!',
          onPress: async () => {
            try {
              const inAppAvailable = await InAppBrowser.isAvailable();
              if (inAppAvailable) {
                await InAppBrowser.open(url, OPTIONS);
              } else {
                await Linking.openURL(url);
              }
              resolve(true);
            } catch (error) {
              Alert.alert(
                `We couldn't open the page`,
                `Please visit ${url} to read our Terms and Conditions`
              );
              resolve(false);
            }
          },
        },
        {
          text: 'Cancel',
          style: 'cancel',
          onPress: () => {
            resolve(false);
          },
        },
      ]
    );
  });
};

export function* openTerms() {
  const response = yield call(showTerms);
  if (response) {
    yield put(TermsActions.termsSuccess());
  } else {
    yield put(TermsActions.termsFailure());
  }
}
