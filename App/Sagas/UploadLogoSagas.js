/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 ************************************************************ */

import { call, put, race, take } from 'redux-saga/effects';
import UploadLogoActions from '../Redux/UploadLogoRedux';
import UploadImageActions, { UploadImageTypes } from '../Redux/UploadImageRedux';
import DropdownAlertHolder from '../Utils/DropdownAlertHolder';
import FirstTimeActions from '../Redux/FirstTimeRedux';
import CurrentProfileActions from '../Redux/CurrentProfileRedux';
import UserProfilesActions from '../Redux/UserProfilesRedux';
import { CurrentUserSelectors } from '../Redux/CurrentUserRedux';

export function* uploadLogo(api, action) {
  const { data, resolve, reject } = action;
  try {
    yield put(UploadImageActions.uploadImageRequest('eatups'));
    // We check wich action comes first and we proceed accordingly
    const { uploadImageSuccess, uploadImageFailure } = yield race({
      uploadImageSuccess: take(UploadImageTypes.UPLOAD_IMAGE_SUCCESS),
      uploadImageFailure: take(UploadImageTypes.UPLOAD_IMAGE_FAILURE),
    });
    if (uploadImageFailure) {
      throw new Error();
    }
    const { payload: path } = uploadImageSuccess;
    const profileData = {
      ...data,
      image: path,
    };
    // make the call to the api
    const response = yield call(api.updateBusiness, profileData);
    // success?
    if (response.ok) {
      yield call(resolve);
      DropdownAlertHolder.alert('success', 'Done!', 'Your logo has been updated');
      yield put(UploadLogoActions.uploadLogoSuccess(response.data));
      yield put(FirstTimeActions.firstTimeDisable(response.data.uid)); // We disable the upload Logo screen
      yield put(CurrentProfileActions.currentProfileRequest(response.data.uid)); // Refresh the current Profile for getting the new Image
      yield put(UserProfilesActions.userProfilesRequest(response.data.owner)); // Refresh the profiles
    } else {
      throw new Error();
    }
  } catch (err) {
    DropdownAlertHolder.alert('error', 'Uh Oh...', 'Something went wrong');
    yield call(reject);
    yield put(UploadLogoActions.uploadLogoFailure());
  }
}
