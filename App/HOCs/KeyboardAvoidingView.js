import React from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

function KeyboardAvoidingViewHOC(WrappedComponent) {
  return props => {
    return (
      <KeyboardAwareScrollView>
        <WrappedComponent {...props} />
      </KeyboardAwareScrollView>
    );
  };
}

export default KeyboardAvoidingViewHOC;
