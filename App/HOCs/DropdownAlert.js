import React from 'react';
import { DropdownAlertConsumer } from '../Context/DropdownAlert';

function DropdownAlertHOC(WrappedComponent) {
  return props => {
    return (
      <DropdownAlertConsumer>
        {context => <WrappedComponent {...props} dropdownAlert={context} />}
      </DropdownAlertConsumer>
    );
  };
}

export default DropdownAlertHOC;
