import React, { Component } from 'react';
import { connect } from 'react-redux';
import ImageUrlActions from '../Redux/ImageUrlRedux';

const imageUrl = (WrappedComponent, getComponentProps) => {
  class ImageUrl extends Component {
    constructor(props) {
      super(props);
      this.state = {
        ...getComponentProps(props),
        url: null,
        isUrl: false, // If a path is already an url we skip all the process and return the url
      };
    }

    componentDidMount() {
      if (typeof this.state.path !== 'undefined' && this.state.path !== null) {
        const { path } = this.state;
        const { id } = this.state;
        const valid = path.match(
          /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/gi
        ); // We check if it´s already and url
        if (valid !== null) {
          this.setState({ url: path, isUrl: true });
        } else {
          const req = {
            uri: path,
            id,
          };
          this.props.imageRequest(req);
        }
      }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
      if (nextProps.payload !== prevState.url) {
        if (prevState.isUrl) {
          return { url: prevState.url };
        }
        return { url: nextProps.payload };
      }
      return null;
    }

    render() {
      const { url } = this.state;
      if (url !== null) return <WrappedComponent {...this.state} imageUrl={{ uri: url }} />;
      return <WrappedComponent {...this.state} imageUrl={require('../Images/Avatar01.png')} />;
    }
  }

  const mapStateToProps = ({ imageUrl }, props) => {
    const { id } = props;
    const { payload, fetching, error } = imageUrl;
    return {
      payload: payload[id] || null,
      fetching: fetching[id] || false,
      error: error[id] || null,
    };
  };

  const mapDispatchToProps = dispatch => {
    return {
      imageRequest: data => dispatch(ImageUrlActions.ImageUrlRequest(data)),
    };
  };

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(ImageUrl);
};

export default imageUrl;
