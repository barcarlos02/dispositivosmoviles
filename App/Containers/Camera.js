import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { connect } from 'react-redux';
import { Icon } from 'react-native-elements';
import CameraActions from '../Redux/CameraRedux';
import PermissionsActions from '../Redux/PermissionsRedux';
import { Touchable, Spinner, Container } from '../Components/UI';
import Gallery from '../Components/Gallery';
import { Colors } from '../Themes';
import styles from './Styles/CameraStyle';

class Camera extends Component {
  static propTypes = {
    onPictureTaken: PropTypes.func,
    onClose: PropTypes.func,
  };

  static defaultProps = {
    onPictureTaken: () => {},
    onClose: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.props.requestPermissions();
    }, 0);
  }

  onGalleryOpened() {
    this.camera.pausePreview();
  }

  onGalleryClosed() {
    this.camera.resumePreview();
  }

  onPhotoSelected(photo) {
    const payload = {
      photo,
      source: 'gallery',
    };
    // eslint-disable-next-line react/prop-types
    this.props.setCameraPayload(payload);
    this.props.onPictureTaken(payload);
  }

  setRef(ref) {
    this.camera = ref;
  }

  async takePicture() {
    const options = {
      quality: 0.5,
      base64: true,
      pauseAfterCapture: true,
      doNotSave: false,
      exif: true,
    };
    let payload = null;
    try {
      const photo = await this.camera.takePictureAsync(options);
      payload = {
        photo,
        source: 'camera',
      };
      setTimeout(() => {
        this.camera.resumePreview();
      }, 500);
    } finally {
      // eslint-disable-next-line react/prop-types
      this.props.setCameraPayload(payload);
      this.setState({ loading: false });
      this.props.onPictureTaken(payload);
    }
  }

  handlePress() {
    this.setState({ loading: true }, this.takePicture.bind(this));
  }

  render() {
    const { hasPermissions } = this.props;
    const { loading } = this.state;
    return (
      <Container style={styles.container}>
        {hasPermissions && (
          <RNCamera
            ref={ref => this.setRef(ref)}
            type={RNCamera.Constants.Type.back}
            style={styles.preview}
          >
            <View style={styles.target} />
          </RNCamera>
        )}
        <View style={styles.takePictureContainer}>
          <Touchable
            onPress={this.handlePress.bind(this)}
            style={styles.takePicture}
            activeOpacity={0.85}
            disabled={loading}
          >
            {loading && <Spinner size="large" color={Colors.secondary} />}
          </Touchable>
        </View>
        {hasPermissions && (
          <Gallery
            onGalleryOpened={this.onGalleryOpened.bind(this)}
            onGalleryClosed={this.onGalleryClosed.bind(this)}
            onPhotoSelected={this.onPhotoSelected.bind(this)}
          />
        )}
        <View style={styles.closeBtnContainer}>
          <Touchable onPress={this.props.onClose} style={styles.closeBtn}>
            <Icon name="close" type="font-awesome" />
          </Touchable>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = ({ permissions }) => {
  return {
    hasPermissions: permissions.grantedCameraPermissions,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setCameraPayload: payload => dispatch(CameraActions.setCameraPayload(payload)),
    requestPermissions: () => dispatch(PermissionsActions.permissionsCameraRequest()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Camera);
