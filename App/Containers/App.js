import '../Config';
import React, { Component } from 'react';
import { AppState, SafeAreaView } from 'react-native';
import { GoogleSignin } from 'react-native-google-signin';
import { Provider } from 'react-redux';
import { MenuProvider } from 'react-native-popup-menu';
import DebugConfig from '../Config/DebugConfig';
import RootContainer from './RootContainer';
import createStore from '../Redux';
import AppStatusActions from '../Redux/AppStatusRedux';
import DropAlert from '../Components/DropAlert';
import UploadProgress from './UploadProgress';
import styles from './Styles/RootContainerStyles';

// create our store
const store = createStore();

/**
 * Provides an entry point into our application.  Both index.ios.js and index.android.js
 * call this component first.
 *
 * We create our Redux store here, put it into a provider and then bring in our
 * RootContainer.
 *
 * We separate like this to play nice with React Native's hot reloading.
 */
const initGoogleSignIn = async () => {
  return GoogleSignin.configure();
};

class App extends Component {
  constructor(props) {
    super(props);
    initGoogleSignIn();
    AppState.addEventListener('change', nextAppState => {
      this.handleStateChange(nextAppState);
    });
  }

  handleStateChange(nextAppState) {
    if (GoogleSignin.isSignedIn) {
      nextAppState === 'active'
        ? store.dispatch(AppStatusActions.appStatusUpdateActive(true))
        : store.dispatch(AppStatusActions.appStatusUpdateActive(false));
    }
  }

  render() {
    return (
      <DropAlert>
        <SafeAreaView style={styles.safeAreaViewStatus} />
        <SafeAreaView style={styles.safeAreaView}>
          <Provider store={store}>
            <React.Fragment>
              <MenuProvider backHandler>
                <RootContainer />
              </MenuProvider>
              <UploadProgress />
            </React.Fragment>
          </Provider>
        </SafeAreaView>
      </DropAlert>
    );
  }
}

// allow reactotron overlay for fast design in dev mode
export default (DebugConfig.useReactotron ? console.tron.overlay(App) : App);
