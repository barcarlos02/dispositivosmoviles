import React, { Component } from 'react';
import { ScrollView, View, KeyboardAvoidingView } from 'react-native';
import { connect } from 'react-redux';
import { Text, Content } from '../Components/UI';
import BusinessSetupForm from '../Components/BusinessSetupForm';
import SubmitProfileActions from '../Redux/SubmitProfileRedux';

// Styles
import styles from './Styles/BusinessSetupScreenStyle';

class BusinessSetupScreen extends Component {
  submit = values => {
    const { submitBusiness } = this.props;
    return submitBusiness(values).then(resp => {
      if (resp) {
        if (this.props.navigation.getParam('menu') === true) {
          this.props.navigation.dismiss();
        } else {
          this.props.navigation.navigate('SignedIn');
        }
      }
    });
  };

  render() {
    return (
      <Content style={styles.container}>
        <KeyboardAvoidingView>
          <View style={styles.wrapper}>
            <Text style={[styles.text, styles.textCenter]}>Let's set you up...</Text>
            <BusinessSetupForm onSubmit={this.submit.bind(this)} />
          </View>
        </KeyboardAvoidingView>
      </Content>
    );
  }
}
const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    submitBusiness: data =>
      new Promise((resolve, reject) => {
        dispatch(SubmitProfileActions.submitProfileBusiness(data, resolve, reject));
      }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BusinessSetupScreen);
