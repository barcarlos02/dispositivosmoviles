import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import uuidv4 from 'uuid/v4';
import { BackHandler } from 'react-native';
import NewBoostForm from './NewBoostForm';
import { Container } from '../Components/UI';
import SavingModal from '../Components/SavingModal';
import EatupBoostHeader from '../Components/EatupBoostHeader';

import SubmitBoostActions from '../Redux/SubmitBoostRedux';
import CameraActions from '../Redux/CameraRedux';
import { Colors } from '../Themes';
import SaveBoostActions from '../Redux/SaveBoostRedux';

// Styles
import styles from './Styles/NewBoostScreenStyle';

class NewBoostScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const {
      params: { card },
    } = navigation.state;

    let title = 'New Boost';
    if (card.id === 1) {
      title = 'Eat me up';
    }
    if (card.id === 2) {
      title = 'Ethical eatup';
    }
    if (card.id === 3) {
      title = 'Healty eatup';
    }
    const header = null;
    return {
      title,
      header,
    };
  };

  static propTypes = {
    profile: PropTypes.instanceOf(Object),
  };

  static defaultProps = {
    profile: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
    this.handleBackButtonClick = this.showModal.bind(this); // When user press back button, we save the eatup
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.handleBackButtonClick();
      return true;
    });
  }

  componentWillUnmount() {
    this.props.cleanCameraCache();
    BackHandler.removeEventListener('hardwareBackPress', () => {
      this.handleBackButtonClick();
      return true;
    });
  }

  getConfig() {
    const { card } = this.props.navigation.state.params;
    if (card.id === 1) {
      return {
        name: 'Eat me up',
        description: 'Dish name or short description',
        color: card.color,
      };
    }
    if (card.id === 2) {
      return {
        name: 'Ethical eatup',
        description:
          'What makes it ethical? e.g. Dinner for two, local and sustainable ingredients...',
        color: card.color,
      };
    }
    if (card.id === 3) {
      return {
        name: 'Healty eatup',
        description: 'What makes it mindfully good? e.g. No refined sugars, light, dairy-free...',
        color: card.color,
      };
    }
    return {
      name: 'Coming Soon',
      description: 'Coming Soon',
      color: Colors.facebook,
    };
  }

  getOptions() {
    const { profile } = this.props;
    if (profile.eatIn && profile.takeaway) {
      return true;
    }
    return false;
  }

  submit = values => {
    const { submitBoost } = this.props;
    const {
      params: { card },
    } = this.props.navigation.state;
    const boost = { button: values.button, values: { ...values.values, type: card.title } };
    return submitBoost(boost).then(() => {
      this.props.navigation.dismiss();
    });
  };

  save = values => {
    const { saveBoost, profile } = this.props;
    const { uid } = profile;
    const boost = { ...values, profileId: uid, boostId: uuidv4() };
    saveBoost(boost);
    this.props.navigation.dismiss();
  };

  openCamera = () => {
    this.props.navigation.navigate('PhotoScreen', {
      dismissable: false,
      targetScreen: null,
    });
  };

  onExit = () => {
    this.props.navigation.dismiss();
  };

  onCancel = () => {
    this.setState({ modalVisible: false });
  };

  showModal = () => {
    this.setState({ modalVisible: true });
  };

  render() {
    const { profile } = this.props;
    const formConfig = this.getConfig();
    return (
      <Container style={styles.container}>
        <EatupBoostHeader
          title="New Offer"
          onBackButtonPress={this.handleBackButtonClick}
          onPress={() => {}}
        />
        <NewBoostForm
          cardConfig={{ ...formConfig, owner: profile.name }}
          hasOptions={this.getOptions()}
          onSubmit={this.submit.bind(this)}
          onCameraPress={this.openCamera.bind(this)}
          onSave={this.save.bind(this)}
        />
        <SavingModal
          visible={this.state.modalVisible}
          onCancel={this.onCancel.bind(this)}
          onExit={this.onExit.bind(this)}
          text="Do you want to exit before saving your boost?"
          disableSave
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ currentProfile }) => {
  return {
    profile: currentProfile.payload,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    submitBoost: data =>
      new Promise((resolve, reject) =>
        dispatch(SubmitBoostActions.submitBoostRequest(data, resolve, reject))
      ),
    cleanCameraCache: () => dispatch(CameraActions.cleanCameraPayload()),
    saveBoost: data => dispatch(SaveBoostActions.saveBoostRequest(data)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewBoostScreen);
