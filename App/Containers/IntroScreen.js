import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Image } from 'react-native';
import { Icon } from 'react-native-elements';
import { Text, Button, Container } from '../Components/UI';
import { Images, Colors } from '../Themes';
import LoginActions from '../Redux/LoginRedux';

// Styles
import styles from './Styles/IntroScreenStyle';

class IntroScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  getLoading() {
    const {
      isLogingIn,
      isCheckingAuth,
      isCheckingLoginSession,
      isCheckingCurrentUserData,
      isCheckingProfileStatus,
      isCreatingUserData,
    } = this.props;

    const fetchingArray = [
      isLogingIn,
      isCheckingAuth,
      isCheckingLoginSession,
      isCheckingCurrentUserData,
      isCheckingProfileStatus,
      isCreatingUserData,
    ];

    return fetchingArray.some(i => i);
  }

  _handleSignIn() {
    const { login } = this.props;
    login();
  }

  render() {
    return (
      <Container style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.section}>
            <Text
              style={[styles.text, styles.introText]}
            >{`Save food, save money,\n feel better!`}</Text>
          </View>
          <View style={[styles.section, styles.sloganWrapper]}>
            <Text style={[styles.text, styles.sloganText]}>We share</Text>
            <Text style={[styles.text, styles.sloganText, styles.sloganTextAccent]}>mindful</Text>
            <Text style={[styles.text, styles.sloganText]}>eatups!</Text>
            <View style={styles.sloganImageWrapper}>
              <Image source={Images.foodHot} />
            </View>
          </View>
          <View>
            <Text style={[styles.text, styles.signinText]}>
              Share your eatups and inspire the world
            </Text>
            <View style={styles.signInButtonWrapper}>
              <Button
                title="Sign in with Google"
                buttonStyle={styles.signInButton}
                titleStyle={styles.signInButtonText}
                icon={<Icon name="google" type="font-awesome" color={Colors.secondary} />}
                onPress={this._handleSignIn.bind(this)}
                loading={this.getLoading()}
              />
            </View>
          </View>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = ({ login, auth, loginStatus, currentUser, profileStatus }) => {
  return {
    isLogingIn: login.fetching,
    isCheckingAuth: auth.fetching,
    isCheckingLoginSession: loginStatus.fetching,
    isCheckingCurrentUserData: currentUser.fetching,
    isCreatingUserData: currentUser.creating,
    isCheckingProfileStatus: profileStatus.fetching,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    login: () => dispatch(LoginActions.loginRequest()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IntroScreen);
