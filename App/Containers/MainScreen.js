import React, { Component } from 'react';
import { View, FlatList, RefreshControl, Text } from 'react-native';
import { connect } from 'react-redux';
import { values, debounce, isEqual } from 'lodash';
import { withNavigationFocus } from 'react-navigation';
import { SwipeListView } from 'react-native-swipe-list-view';
import { Spinner, Container } from '../Components/UI';
import EatupBoostCard from './EatupBoostCard';
import VanishingButton from '../Components/VanishingButton';
import FILTER from '../Utils/EatupBoostFilter';
import UploadLogo from './UploadLogoScreen';
import CurrentPageActions from '../Redux/CurrentPageRedux';
import EatupsBoostsUpdatesActions from '../Redux/EatupsBoostsUpdatesRedux';
import BackCard from '../Components/BackCard';

// Styles
import { Colors, Metrics } from '../Themes';
import styles from './Styles/MainScreenStyle';
import Jumbotron from '../Components/Jumbotron';

class MainScreen extends Component {
  constructor(props) {
    super(props);
    this.setNewPage = debounce(this.refresh.bind(this), 150);
    this.flatList = null;
  }

  onCardPress(uid) {
    this.props.navigation.navigate('Eatup');
  }

  _keyExtractor(item) {
    return item.uid;
  }

  _renderEmptyCard = () => {
    const { currentFilter, fetching } = this.props;
    const type = FILTER.find(i => i.value === currentFilter).field;
    const msg = type === 'eatup' ? "There's no eatups to show" : "There's no boosts to show";
    return (
      <Jumbotron
        title={msg}
        containerStyles={styles.jumbotron}
        titleStyles={styles.jumbotronTitle}
      />
    );
  };

  _renderFetchingIndicator = () => {
    const { hasMoreItems } = this.props;
    if (hasMoreItems) {
      return (
        <View style={[styles.updateItem]}>
          <Spinner size="large" color={Colors.accent} />
        </View>
      );
    }
    return null;
  };

  _renderItem({ item }) {
    return (
      <EatupBoostCard
        id={item.uid}
        onPress={() => {
          this.onCardPress(item.uid);
        }}
      />
    );
  }

  refresh = distanceFromEnd => {
    if (
      (this.onEndReachedCalledDuringMomentum || this.onEndReachedCalledByUser) &&
      !this.props.fetching
    ) {
      this.props.setPage(this.props.currentPage + 1);
      this.onEndReachedCalledByUser = false;
      this.onEndReachedCalledDuringMomentum = false;
    }
  };

  fetchOlder({ distanceFromEnd }) {
    const { hasMoreItems } = this.props;
    if (hasMoreItems) {
      this.setNewPage(distanceFromEnd);
    }
  }

  _renderList() {
    const { fetching, data, existNewItems, setUpdatesSeen, setPage } = this.props;
    const ids = values(data);
    if (fetching && !data) {
      return (
        <View style={[styles.containerInTabs, styles.loadingWrapper]}>
          <Spinner size="large" color={Colors.secondary} />
        </View>
      );
    }
    return (
      <Container style={[styles.containerInTabs]}>
        {/* <View style={styles.container}> */}
        {/* <FlatList
          ref={ref => {
            this.flatList = ref;
          }}
          data={ids}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem.bind(this)}
          onEndReachedThreshold={0.00001}
          onEndReached={this.fetchOlder.bind(this)}
          // extraData={fetching}
          onRefresh={() => {
            setPage(0);
          }}
          refreshing={fetching && this.currentPage === 0}
          ListEmptyComponent={this._renderEmptyCard.bind(this)}
          ListFooterComponent={this._renderFetchingIndicator}
          onMomentumScrollEnd={() => {
            this.onEndReachedCalledDuringMomentum = true;
          }}
          onScrollEndDrag={() => {
            this.onEndReachedCalledByUser = true;
          }}
        /> */}
        <SwipeListView
          data={ids}
          ref={ref => {
            this.flatList = ref;
          }}
          renderItem={this._renderItem.bind(this)}
          renderHiddenItem={(ids, rowMap) => <BackCard onEdit={() => {}} onDelete={() => {}} />}
          leftOpenValue={105}
          rightOpenValue={-105}
          onRefresh={() => {
            setPage(0);
          }}
          onEndReachedThreshold={0.00001}
          onEndReached={this.fetchOlder.bind(this)}
          keyExtractor={this._keyExtractor}
          refreshing={fetching && this.currentPage === 0}
          ListEmptyComponent={this._renderEmptyCard.bind(this)}
          ListFooterComponent={this._renderFetchingIndicator}
          onMomentumScrollEnd={() => {
            this.onEndReachedCalledDuringMomentum = true;
          }}
          onScrollEndDrag={() => {
            this.onEndReachedCalledByUser = true;
          }}
        />
        {/* </View> */}
        {existNewItems && (
          <VanishingButton
            title={FILTER.find(i => i.value === this.props.currentFilter).newItemsAlert}
            vanish={setUpdatesSeen.bind(this)}
            onPress={() => {
              setPage(0);
            }}
            shouldAnimate={existNewItems}
          />
        )}
      </Container>
    );
  }

  _renderLogo() {
    return <UploadLogo />;
  }

  render() {
    const { isFirstTime } = this.props;
    if (isFirstTime && typeof isFirstTime !== 'undefined' && isFirstTime !== null)
      return this._renderLogo(); // only happens when yu created a new business account
    return this._renderList();
  }
}

const mapStateToProps = ({
  eatupsBoosts,
  page,
  eatupBoostFilter,
  eatupsBoostsUpdates,
  firstTime,
  currentProfile,
}) => {
  const { payload, fetching, hasMore } = eatupsBoosts;
  return {
    data: payload,
    fetching,
    currentPage: page.page,
    currentFilter: eatupBoostFilter.payload,
    existNewItems: eatupsBoostsUpdates.payload,
    hasMoreItems: hasMore,
    isFirstTime: firstTime.payload[currentProfile.payload],
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setPage: data => dispatch(CurrentPageActions.setCurrentPage(data)),
    setUpdatesSeen: () => dispatch(EatupsBoostsUpdatesActions.eatupsBoostsUpdatesSeen()),
    closeChannel: () => dispatch(EatupsBoostsUpdatesActions.eatupsBoostsUpdatesClose()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainScreen);
