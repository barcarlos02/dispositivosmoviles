import React, { Component } from 'react';
import { connect } from 'react-redux';
import MapView, { Marker } from 'react-native-maps';
import { map } from 'lodash';
import { Subject } from 'rxjs';
import { throttleTime } from 'rxjs/operators';
import { Marker as CustomMarker, Container } from '../Components/UI';
import MarkersActions from '../Redux/MarkersRedux';
import MarkerDetailActions from '../Redux/MarkerDetailRedux';
import MarkerModal from '../Components/MarkerModal';
// Styles
import styles from './Styles/MapScreenStyle';
import { Metrics } from '../Themes';

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
const duration = 2000;
class MapScreen extends Component {
  static navigationOptions = {
    // header: null,
    title: 'Locate Eatups',
  };

  constructor(props) {
    super(props);
    this.state = {
      currentLatitude: 21.931773,
      currentLongitude: -102.3409818,
      currentLatitudeDelta: 0.0922,
      currentLongitudeDelta: 0.0421,
      showModal: false,
      relocate: false,
    };
    this.markerStream = new Subject();
  }

  componentDidMount() {
    this.markerStream.pipe(throttleTime(duration)).subscribe(coordenates => {
      this.props.getMarkers(coordenates);
    });
  }

  componentWillUnmount() {
    this.markerStream.unsubscribe();
  }

  _renderMarkers() {
    const { eatupBoostMarkers, getDetail } = this.props;
    if (eatupBoostMarkers)
      return map(Object.keys(eatupBoostMarkers), (marker, k) => {
        const coorConverted = marker.replace(/\,/g, '.');
        const coordenates = coorConverted.split('|');
        const coords = { latitude: Number(coordenates[0]), longitude: Number(coordenates[1]) };
        const type = eatupBoostMarkers[marker] === 'eatups' ? 'eatup' : 'boost';
        // const type=eatupBoostMarkers[marker]=='eatups'? 'eatup': eatupBoostMarkers[marker]=='boosts'?'boost': 'eatupboost'
        const markerType = { type };
        return (
          <Marker
            key={k}
            coordinate={coords}
            onPress={region => {
              this.setState({
                showModal: true,
                relocate: true,
                currentLatitude: region.nativeEvent.coordinate.latitude,
                currentLongitude: region.nativeEvent.coordinate.longitude,
                markerType,
              });
              getDetail({ coordenate: marker });
            }}
            style={{
              elevation: 20,
              zIndex: 10,
            }}
          >
            <CustomMarker marker={markerType} position={{ position: 'absolute' }} />
          </Marker>
        );
      });
    return null;
  }

  centerMapTo(currentLatitude, currentLongitude, currentLatitudeDelta, currentLongitudeDelta) {
    const regionProp = {};
    if (this.state.relocate) {
      regionProp.region = {
        latitude: currentLatitude + currentLatitudeDelta / 200,
        longitude: currentLongitude,
        latitudeDelta: currentLatitudeDelta / 20,
        longitudeDelta: currentLongitudeDelta / 20,
      };
    }
    return regionProp;
  }

  fetchMarker(region) {
    const coordenates = {
      latitude: region.latitude,
      longitude: region.longitude,
      delta: region.longitudeDelta,
    };
    this.markerStream.next(coordenates);
  }

  render() {
    const {
      currentLatitude,
      currentLatitudeDelta,
      currentLongitude,
      currentLongitudeDelta,
      showModal,
    } = this.state;
    const eatupBoost = this.props.eatupBoostDetail;
    const { fetchingDetail } = this.props;
    const propRegion = this.centerMapTo(
      currentLatitude,
      currentLongitude,
      currentLatitudeDelta,
      currentLongitudeDelta
    ); // Will receive a region prop to indicate when to relocate the map to show the modal
    return (
      <Container style={styles.container}>
        <MapView
          ref={map => (this.map = map)} // Using initial region instead of region , is a bug of react-native maps
          initialRegion={{
            latitude: currentLatitude,
            longitude: currentLongitude,
            latitudeDelta: currentLatitudeDelta,
            longitudeDelta: currentLongitudeDelta,
          }}
          {...propRegion}
          style={styles.map}
          onRegionChangeComplete={this.fetchMarker.bind(this)} // onPanDrag={newRegion => {
          maxZoomLevel={19} // If the value is 20 there´s a bug which start sending changes in the event onRegionChangeComplete
          showsUserLocation
          followsUserLocation
        >
          {this._renderMarkers()}
        </MapView>
        {!!showModal && (
          <MarkerModal
            dataSource={eatupBoost}
            isVisible={showModal}
            onClose={() => {
              this.setState({ showModal: false, relocate: false });
            }}
            fetching={fetchingDetail}
            markerType={this.state.markerType}
          />
        )}
      </Container>
    );
  }
}

const mapStateToProps = ({ markers, markerDetail, currentUser }) => {
  const eatupBoostMarkers = markers.payload;
  const eatupBoostDetail = markerDetail.payload;
  return {
    eatupBoostMarkers,
    eatupBoostDetail,
    currentUser: currentUser.payload.uid,
    fetchingDetail: markerDetail.fetching,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getMarkers: coordenates => dispatch(MarkersActions.markersRequest(coordenates)),
    getDetail: coordenate => dispatch(MarkerDetailActions.markerDetailRequest(coordenate)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MapScreen);

// showsUserLocation
// followsUserLocation
