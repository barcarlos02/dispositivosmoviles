import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Icon, Image } from 'react-native-elements';
import { Container } from '../Components/UI';
import CustomIcon from '../Components/CustomIcon';
import ImageUrl from '../HOCs/ImageUrl';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import { Colors, Metrics } from '../Themes';

// Styles
import styles from './Styles/AvatarStyle';

const width = Metrics.screenWidth;
const height = Metrics.screenHeight;

class Avatar extends Component {
  static propsTypes = {
    url: PropTypes.string,
    size: PropTypes.number,
    type: PropTypes.string,
    id: PropTypes.id,
  };

  static defaultProps = {
    size: Math.round(width * 0.25),
  };

  renderIcon() {
    const { size, url, type, id } = this.props;
    if (url) {
      return (
        <Image
          style={[{ width: size, height: size, borderRadius: size / 2 }]}
          source={{ uri: url }}
        />
      );
    }
    if (type === 'foodie') {
      return (
        <Icon
          name="user"
          type="evilicon"
          size={size}
          containerStyle={styles.avatar}
          iconStyle={styles.avatarContent}
        />
      );
    }
    return (
      <CustomIcon
        name="ic_avatar_business_default"
        size={size}
        style={{ color: Colors.windowTint }}
      />
    );

    // return <Icon name="user" type="evilicon" size={size} style={{ color: Colors.windowTint }} />;
  }

  render() {
    const { size } = this.props;
    return (
      <Container style={[styles.avatarContainer, { width: size, height: size }]}>
        {this.renderIcon()}
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

const AvatarWithUrl = ImageUrl(Avatar, props => {
  return {
    ...props,
    id: props.id,
    path: props.url,
  };
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AvatarWithUrl);
