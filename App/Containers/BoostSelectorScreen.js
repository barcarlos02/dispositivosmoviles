import React, { Component } from 'react';
import { ScrollView, View } from 'react-native';
import { connect } from 'react-redux';
import { Text, Container, Content } from '../Components/UI';
import BoostCard from '../Components/BoostCard';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/BoostSelectorScreenStyle';

const cards = [
  {
    id: 1,
    title: 'EAT ME UP',
    description:
      'Perfect if you still have food that cant be stored for tomorrow.Make a last-minute offer to get customers before you close and reduce or avoid any food going to waste.',
    buttonTitle: 'NEW BOOST',
    color: 'rgb(84, 104, 102)',
  },
  {
    id: 2,
    title: 'ETHICAL EATUP',
    description:
      'Create a promo for a special meal that includes sustainable and ethically responsible sources , low greenhouse footprint, and no single-use plastic.',
    buttonTitle: 'NEW BOOST',
    color: 'rgb(158, 185, 134)',
  },
  {
    id: 3,
    title: 'HEALTY EATUP',
    description:
      'Make an offer for healthy and enjoyable eatup experience.This can include sugar-free, GMO-free, smaller and healthy portions, slow food, appreciate food experiences or similar.',
    buttonTitle: 'NEW BOOST',
    color: 'rgb(118, 173, 176)',
  },
];

class BoostSelectorScreen extends Component {
  _renderCards() {
    const { navigation } = this.props;
    return cards.map(card => {
      return (
        <BoostCard
          key={card.id}
          {...card}
          onPressed={() => navigation.navigate({ routeName: 'NewBoost', params: { card } })}
        />
      );
    });
  }

  render() {
    return (
      <Container style={styles.container}>
        <Text style={styles.headerText}>
          What part of eating mindfully suits your brand? ...you can have it all
        </Text>
        <Content>
          {this._renderCards()}
          <Text style={styles.note}>
            To reboost an old post straight from your gallery, just swipe right to edit, update any
            info and republish.
          </Text>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BoostSelectorScreen);
