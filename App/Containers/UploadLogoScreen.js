/* import React, { Component } from 'react';
import { View, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { values } from 'lodash';
import { Spinner, Container } from '../Components/UI';
import EatupBoostCard from './EatupBoostCard'; */

// Styles
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image } from 'react-native';
import styles from './Styles/MainScreenStyle';
import Container from '../Components/UI/Container';
import Text from '../Components/UI/Text';
import Touchable from '../Components/Touchable';
import CustomIcon from '../Components/CustomIcon';
import { Metrics, Colors } from '../Themes';
import IconButton from '../Components/IconButton';
import Gallery from '../Components/Gallery';
import UploadLogoActions from '../Redux/UploadLogoRedux';
import CameraActions from '../Redux/CameraRedux';
import PermissionsActions from '../Redux/PermissionsRedux';
// Styles
import style from './Styles/UploadLogoStyle';

class MainScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uploadImage: {},
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.props.requestPermissions();
    }, 0);
  }

  onPhotoSelected(photo) {
    const payload = {
      photo,
      source: 'gallery',
    };
    this.props.setCameraPayload(payload);
    const newLogo = this.props.uploadLogo(payload);
    const { uri } = newLogo;
    this.setState({ uploadImage: uri });
  }

  render() {
    const width = Metrics.screenWidth;
    const size = Math.round(width * 0.25);
    const { hasPermissions } = this.props;
    return (
      <Container style={style.mainContainer}>
        <Container style={style.topContainer}>
          <Text style={style.mainText}>welcome</Text>
          <Text style={style.mainText}>{`${this.props.name}!`} </Text>
          {hasPermissions && (
            <Gallery
              trigger={
                <IconButton
                  size={size}
                  icon="ic_avatar_business_default"
                  onPress={() => {}}
                  background={Colors.background}
                />
              }
              onPhotoSelected={this.onPhotoSelected.bind(this)}
              onGalleryClosed={() => {}}
            />
          )}
          <Text style={style.centerText}>upload logo</Text>
        </Container>
        <Container />
        <Container style={style.downContainer}>
          <Text style={style.centerText}>or get started right away with</Text>
          <Text style={style.centerText}>your first mindful eatup!</Text>
          <CustomIcon
            name="ic_avatar_business_default"
            size={Math.round(width * 0.1)}
            style={{ alignSelf: 'center' }}
          />
        </Container>
      </Container>
    );
  }
}

const mapStateToProps = ({ camera, currentProfile, permissions }) => {
  const { payload } = camera;
  const { name } = currentProfile.payload;
  return { payload, name, hasPermissions: permissions.grantedCameraPermissions };
};

const mapDispatchToProps = dispatch => {
  return {
    uploadLogo: data =>
      new Promise((resolve, reject) => {
        dispatch(UploadLogoActions.uploadLogoRequest(data, resolve, reject));
      }),
    setCameraPayload: payload => dispatch(CameraActions.setCameraPayload(payload)),
    requestPermissions: () => dispatch(PermissionsActions.permissionsCameraRequest()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainScreen);
