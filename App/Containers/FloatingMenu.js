import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { View, Animated, Easing } from 'react-native';
import Modal from 'react-native-modal';
import { Text, Touchable, Container } from '../Components/UI';
import Icon from '../Components/CustomIcon';

import styles from './Styles/FloatingMenuStyle';
import { Colors, Metrics } from '../Themes';

const size = Math.round(Metrics.navBarHeight * 0.7);
const plusIconSize = Math.round(Metrics.navBarHeight * 0.45);

const height = Metrics.navBarHeight;

class FloatingMenu extends Component {
  // Prop type warnings
  static propTypes = {
    openMenu: PropTypes.bool.isRequired,
    onBack: PropTypes.func.isRequired,
    eatupPress: PropTypes.func.isRequired,
    boostPress: PropTypes.func.isRequired,
    position: PropTypes.instanceOf(Object).isRequired,
  };

  // Defaults for props
  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      slide: new Animated.Value(0),
      rotate: new Animated.Value(0),
    };
  }

  runCloseAnimation() {
    Animated.parallel(
      [
        Animated.timing(this.state.slide, {
          duration: 400,
          toValue: 0,
          easing: Easing.out(Easing.cubic),
          useNativeDriver: true,
        }),
        Animated.timing(this.state.rotate, {
          duration: 400,
          toValue: 0,
          easing: Easing.out(Easing.cubic),
          useNativeDriver: true,
        }),
      ],
      { stopTogether: false }
    ).start();
  }

  runOpenAnimation() {
    Animated.parallel(
      [
        Animated.timing(this.state.slide, {
          duration: 400,
          toValue: 1,
          easing: Easing.in(Easing.cubic),
          useNativeDriver: true,
        }),
        Animated.timing(this.state.rotate, {
          duration: 400,
          toValue: 1,
          easing: Easing.in(Easing.cubic),
          useNativeDriver: true,
        }),
      ],
      { stopTogether: false }
    ).start();
  }

  navigate(callback) {
    this.props.onBack();
    if (callback) {
      setTimeout(callback, 100);
    }
  }

  renderBoostButton() {
    const { position } = this.props;
    return (
      <Animated.View
        style={[
          styles.boostBtn,
          { top: position.pageY - 100 },
          {
            transform: [
              {
                translateY: this.state.slide.interpolate({
                  inputRange: [0, 1],
                  outputRange: [height + Math.round((height * 1.7) / 2), 0],
                }),
              },
              {
                translateX: this.state.slide.interpolate({
                  inputRange: [0, 1],
                  outputRange: [Math.round(height * -1.3), 0],
                }),
              },
            ],
            opacity: this.state.slide.interpolate({
              inputRange: [0, 1],
              outputRange: [0.2, 1],
            }),
          },
        ]}
      >
        <Touchable type="opacity" onPress={this.navigate.bind(this, this.props.boostPress)}>
          <Container style={styles.boostContainer}>
            <Icon name="ic_offer_label_business_eatups" size={size} style={[styles.icon]} />
          </Container>
        </Touchable>
        <Text style={styles.title}>boost</Text>
      </Animated.View>
    );
  }

  renderCloseButton() {
    const { onBack, position } = this.props;
    const positionStyle = {
      top: position.pageY - position.height / 2,
      left: position.pageX + position.width / 2,
    };
    return (
      <Animated.View
        style={[
          styles.closeButton,
          positionStyle,
          {
            transform: [
              {
                translateX: -1 * (Math.round(Metrics.navBarHeight * 1.1) / 2),
              },
              {
                translateY: 1 * (Math.round(Metrics.navBarHeight * 1.1) / 2),
              },
              {
                rotate: this.state.rotate.interpolate({
                  inputRange: [0, 1],
                  outputRange: ['0deg', '45deg'],
                }),
              },
            ],
          },
        ]}
      >
        <Touchable
          type="opacity"
          activeOpacity={1}
          onPress={() => {
            onBack();
          }}
        >
          <Container style={styles.touchableWrapper}>
            <View style={styles.closeContainer}>
              <Icon name="ic_fab_plus" size={plusIconSize} style={[{ color: Colors.snow }]} />
            </View>
          </Container>
        </Touchable>
      </Animated.View>
    );
  }

  renderEatupButton() {
    const { position } = this.props;
    return (
      <Animated.View
        style={[
          styles.eatupBtn,
          { top: position.pageY - 100 },
          {
            // left: Metrics.screenWidth / 2,
            transform: [
              {
                translateY: this.state.slide.interpolate({
                  inputRange: [0, 1],
                  outputRange: [height + Math.round((height * 1.7) / 2), 0],
                }),
              },
              {
                translateX: this.state.slide.interpolate({
                  inputRange: [0, 1],
                  outputRange: [Math.round(height * 1.3), 0],
                }),
              },
            ],
            opacity: this.state.slide.interpolate({
              inputRange: [0, 1],
              outputRange: [0.2, 1],
            }),
          },
        ]}
      >
        <Touchable type="opacity" onPress={this.navigate.bind(this, this.props.eatupPress)}>
          <Container style={styles.touchableWrapper}>
            <View style={styles.eatupContainer}>
              <Icon name="ic_rating_full" size={size} style={[styles.icon]} />
            </View>
          </Container>
        </Touchable>
        <Text style={styles.title}>eatup</Text>
      </Animated.View>
    );
  }

  render() {
    const { openMenu, onBack } = this.props;
    return (
      <Modal
        style={{ margin: 0 }}
        isVisible={openMenu}
        useNativeDriver
        hideModalContentWhileAnimating
        onBackButtonPress={() => {
          onBack();
        }}
        onBackdropPress={() => {
          onBack();
        }}
        backdropColor={Colors.windowTint}
        animationIn="fadeIn"
        animationInTiming={400}
        animationOut="fadeOut"
        onModalWillShow={() => {
          this.runOpenAnimation();
        }}
        onModalWillHide={() => {
          this.runCloseAnimation();
        }}
      >
        {this.renderEatupButton()}
        {this.renderBoostButton()}
        {this.renderCloseButton()}
      </Modal>
    );
  }
}

const mapStateToProps = ({ currentProfile }) => {
  return {
    profile: currentProfile.payload,
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FloatingMenu);
