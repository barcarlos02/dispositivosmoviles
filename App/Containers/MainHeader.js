import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Alert } from 'react-native';
import { connect } from 'react-redux';
import { map, forEach, reduce } from 'lodash';
import { Text, Touchable, Container } from '../Components/UI';
import EatupBoostFilterActions from '../Redux/EatupBoostFilterRedux';
import Icon from '../Components/CustomIcon';
import HeaderAvatar from './HeaderAvatar';
import { Metrics } from '../Themes';
import UserProfilesActions from '../Redux/UserProfilesRedux';
import UpdateCurrentProfileActions from '../Redux/UpdateCurrentProfileRedux';
import LogoutActions from '../Redux/LogoutRedux';
import EatupsBoostsActions from '../Redux/EatupsBoostsRedux';
import CommonHeader from '../Components/CommonHeader';
import HeaderMenu from '../Components/HeaderMenu';
import HeaderIcon from '../Components/HeaderIcon';
import FILTER from '../Utils/EatupBoostFilter';

// Styles
import styles from './Styles/MainHeaderStyle';

const height = Metrics.navBarHeight;
const iconSize = height * 0.5;
const imageSize = height * 0.75;

class MainHeader extends Component {
  // Prop type warnings
  static propTypes = {};

  // // Defaults for props
  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      menu: 'left',
    };
  }

  componentDidMount() {
    const { user, getProfiles } = this.props;
    getProfiles(user);
  }

  componentWillUnmount() {
    this.props.closeChannel();
  }

  onRef = r => {
    this.menu = r;
  };

  getProfileItems() {
    const { userProfiles, currentProfile, updateCurrentProfile } = this.props;
    if (userProfiles) {
      const items = map(userProfiles, (value, key) => {
        const item = {
          title: value.type === 'foodie' ? value.displayName : value.name,
          value: value.uid,
          image: value.imageURL || null,
          profileType: value.type,
          onPress: actualProfile => {
            updateCurrentProfile(actualProfile);
            this.menu.close();
          },
        };
        if (value.uid === currentProfile) {
          return {
            ...item,
            selected: true,
          };
        }
        return item;
      });
      items.push({
        title: 'Create new account',
        value: 'new',
        icon: true,
        onPress: () => {
          this.props.navigation.navigate('NewAccount');
        },
      });
      return items;
    }
    return [];
  }

  getOptions() {
    const { menu } = this.state;
    const { currentFilter } = this.props;
    let items;
    switch (menu) {
      case 'left':
        items = this.getProfileItems();
        break;
      case 'center':
        items = FILTER.map(i => {
          return {
            ...i,
            selected: currentFilter === i.value,
            onPress: this.setFilter.bind(this),
          };
        });
        break;
      default:
        items = [
          { title: 'Settings', value: 'nein' },
          {
            title: 'Log out',
            value: 'niet',
            onPress: () => {
              this.logout();
            },
          },
        ];
    }
    return items;
  }

  setFilter(value) {
    this.props.setFilter(value);
  }

  logout() {
    const { logOut } = this.props;
    Alert.alert('Log Out', 'Do you wish to continue?', [
      {
        text: 'Ok',
        onPress: () => {
          logOut();
        },
      },
      {
        text: 'Cancel',
        onPress: () => {},
        style: 'cancel',
      },
    ]);
  }

  toggleMenu(menu) {
    this.setState(
      {
        menu,
      },
      () => {
        setTimeout(() => this.menu.open(), 1);
      }
    );
  }

  renderCenter() {
    return (
      <Touchable
        onPress={() => {
          this.toggleMenu('center');
        }}
        style={{ flex: 1 }}
      >
        <Container style={styles.centerButton}>
          <View style={styles.btnTitle}>
            <Text style={styles.title}>
              {FILTER.find(i => i.value === this.props.currentFilter).title}
            </Text>
          </View>
          <View stye={styles.centerBtnIcon}>
            <Icon name="ic_map_pointer_background" size={iconSize} style={[styles.icon]} />
          </View>
        </Container>
      </Touchable>
    );
  }

  renderLeft() {
    return (
      <Touchable
        type="opacity"
        style={{ flex: 1 }}
        onPress={() => {
          this.toggleMenu('left');
        }}
      >
        <HeaderAvatar size={imageSize} />
      </Touchable>
    );
  }

  renderRight() {
    return [
      <HeaderIcon
        name="ic_ChevronDown"
        onPress={() => {
          this.toggleMenu('right');
        }}
      />,
    ];
  }

  render() {
    const options = this.getOptions();
    const { menu } = this.state;

    return (
      <CommonHeader
        left={this.renderLeft()}
        center={this.renderCenter()}
        right={this.renderRight()}
      >
        <HeaderMenu position={menu} items={options} onRef={this.onRef} />
      </CommonHeader>
    );
  }
}
const mapStateToProps = ({ currentUser, currentProfile, userProfiles, eatupBoostFilter }) => {
  return {
    userData: currentUser.payload,
    user: currentUser.payload.uid,
    currentProfile: currentProfile.data,
    userProfiles: userProfiles.payload,
    currentFilter: eatupBoostFilter.payload,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getProfiles: data => dispatch(UserProfilesActions.userProfilesRequest(data)),
    updateCurrentProfile: data =>
      dispatch(UpdateCurrentProfileActions.updateCurrentProfileRequest(data)),
    logOut: () => dispatch(LogoutActions.logoutRequest()),
    setFilter: value => dispatch(EatupBoostFilterActions.setEatupBoostFilter(value)),
    closeChannel: () => dispatch(EatupsBoostsActions.eatupsBoostsClose()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainHeader);
