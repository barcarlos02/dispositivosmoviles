import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { Colors } from '../Themes';
import Jumbotron from '../Components/Jumbotron';
import CommonCard from '../Components/CommonCard';
import { Touchable } from '../Components/UI';

// Styles
import styles from './Styles/EatupBoostCardStyle';

class EatupBoostCard extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
  };

  static defaultProps = {};

  _handlePress() {
    this.props.onPress(this.props.id);
  }

  _renderContent() {
    const { data = {}, fetching, currentUser, id, imageUrl } = this.props;
    const Wrapper = fetching ? View : Touchable;
    const wrapperProps = fetching
      ? {}
      : {
          activeOpacity: 0.8,
          onPress: this._handlePress.bind(this),
        };
    const owner = currentUser === data.created_by ? 'by me' : data.ownerName;
    const { geolocation = {} } = data;
    const { type = '' } = data;
    const { atributes = {} } = data;
    return (
      <Wrapper style={styles.cardWrapper} {...wrapperProps}>
        <CommonCard
          owner={owner}
          name={geolocation.placeName || data.profileName}
          description={data.description}
          loading={fetching}
          color={Colors.darkMain}
          id={id}
          image={data.image}
          amount={data.amount}
          type={type}
          atributes={atributes}
        />
      </Wrapper>
    );
  }

  render() {
    const { error } = this.props;
    return (
      <View style={[styles.container, styles.cardWrapper]}>
        {error ? <Jumbotron /> : this._renderContent()}
      </View>
    );
  }
}

const mapStateToProps = ({ eatupBoost, currentUser }, props) => {
  const { id } = props;
  const { fetching, payload, error } = eatupBoost;
  return {
    fetching: fetching[id],
    data: payload[id] || {},
    error: error[id],
    currentUser: currentUser.payload.uid,
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EatupBoostCard);
