import React from 'react';
import { ScrollView, View } from 'react-native';
import { connect } from 'react-redux';
import { Text, Button, Container, Content } from '../Components/UI';
import Banner from '../Components/Banner';
import IconLabelWrapper from '../Components/IconLabelWrapper';

import TermsActions from '../Redux/TermsRedux';

// Styles
import styles from './Styles/AccountSetupScreenStyle';

const items = [
  { icon: 'ic_camera_simplified', label: 'Take nice shots to appreciate more' },
  { icon: 'ic_rating_full', label: 'Track the impact of food choices' },
  { icon: 'ic_heart', label: 'Feel better in body and mind' },
];

const AccountSetupScreen = props => {
  const _openTerms = () => {
    const { openBrowser } = props;
    openBrowser();
  };

  const _goToAccept = () => {
    props.navigation.navigate('BusinessAcceptScreen');
  };

  const _goToUserSetup = () => {
    props.navigation.navigate('UserSetupScreen');
  };

  return (
    <Container style={{ flex: 1 }}>
      <Content style={styles.container}>
        <View style={styles.wrapper}>
          <View>
            <Text style={[styles.text, styles.greetingText]}>{`Welcome\n${
              props.user.displayName
            }!`}</Text>
            <Text style={[styles.text, styles.sloganText]}>Eating mindfully is easy</Text>
          </View>
          <Banner style={styles.banner} color="#9FB985">
            <IconLabelWrapper icons={items} />
          </Banner>
          <View style={styles.content}>
            <Text style={[styles.text, styles.textMargin]}>
              Please read about our{' '}
              <Text style={styles.textAccent} onPress={_openTerms}>
                Terms and Privacy
              </Text>
              . By continuing you are accepting all that's in there
            </Text>
            <Button
              title="LET'S EAT MINDFULLY!"
              containerStyle={styles.buttonStartWrapper}
              buttonStyle={styles.buttonStart}
              titleStyle={styles.buttonStartText}
              onPress={_goToUserSetup}
              upperCase
            />
            <Text style={[styles.text, styles.textMargin]}>
              Are you a food business and want to see what's in there for you?
            </Text>
            <Button title="I'M A BUSINESS" onPress={_goToAccept} />
          </View>
        </View>
      </Content>
    </Container>
  );
};

const mapStateToProps = ({ currentUser }) => {
  return {
    user: currentUser.payload,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    openBrowser: () => dispatch(TermsActions.termsRequest()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountSetupScreen);
