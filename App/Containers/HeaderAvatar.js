import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Icon, Image, Header } from 'react-native-elements';
import { Spinner, Container } from '../Components/UI';
import CustomIcon from '../Components/CustomIcon';
import Imagen from '../Components/ImageWithUrl';

import { Colors, Metrics } from '../Themes';

import CurrentProfileActions from '../Redux/CurrentProfileRedux';

// Styles
import styles from './Styles/HeaderAvatarStyle';

const width = Metrics.screenWidth;
const height = Metrics.screenHeight;

class HeaderAvatar extends Component {
  static propsTypes = {
    url: PropTypes.string,
    size: PropTypes.number,
  };

  static defaultProps = {
    size: Math.round(width * 0.25),
  };

  componentDidMount() {
    const { currentUser, updateProfile } = this.props;
    updateProfile(currentUser.currentProfile);
  }

  renderIcon() {
    const { size, url, profile, fetching } = this.props;
    if (fetching) {
      return <Spinner size="small" color={Colors.accent} />;
    }
    if (url) {
      return (
        <Image
          style={[{ width: size, height: size, borderRadius: size / 2 }]}
          source={{ uri: url }}
        />
      );
    }
    if (!!profile && !!profile.imageURL) {
      return (
        <Imagen // this is a component wit a HOC
          id={profile.place_id}
          path={profile.imageURL}
          styles={[{ width: size, height: size, borderRadius: size / 2 }]}
        />
      );
    }
    if (!!profile && !!profile.type) {
      if (profile.type === 'foodie') {
        return (
          <Icon
            name="user"
            type="evilicon"
            size={size}
            containerStyle={styles.avatar}
            iconStyle={styles.avatarContent}
          />
        );
      }
      return (
        <CustomIcon name="ic_avatar_business_default" size={size} style={styles.avatarContent} />
      );
    }
    return (
      <Icon
        name="user"
        type="evilicon"
        size={size}
        containerStyle={styles.avatar}
        iconStyle={styles.avatarContent}
      />
    );
  }

  render() {
    const { size } = this.props;
    return (
      <Container style={[styles.avatarContainer, { width: size, height: size }]}>
        {this.renderIcon()}
      </Container>
    );
  }
}

const mapStateToProps = ({ currentProfile, currentUser }) => {
  return {
    currentUser: currentUser.payload,
    profile: currentProfile.payload,
    fetching: currentProfile.fetching,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateProfile: payload => dispatch(CurrentProfileActions.currentProfileRequest(payload)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderAvatar);
