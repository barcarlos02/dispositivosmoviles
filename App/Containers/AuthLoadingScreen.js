import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { Spinner, Container } from '../Components/UI';
import { Colors } from '../Themes';

import AuthActions from '../Redux/AuthRedux';

// Styles
import styles from './Styles/AuthLoadingScreenStyle';

class AuthLoadingScreen extends Component {
  componentDidMount() {
    const { getStatus } = this.props;
    getStatus();
  }

  render() {
    return (
      <Container style={styles.container}>
        <Spinner color={Colors.secondary} size="large" />
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    getStatus: () => dispatch(AuthActions.authRequest()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthLoadingScreen);
