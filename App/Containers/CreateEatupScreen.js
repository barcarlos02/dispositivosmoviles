import React, { Component } from 'react';
import { connect } from 'react-redux';
import uuidv4 from 'uuid/v4';
import { BackHandler } from 'react-native';
import SaveEatupActions from '../Redux/SaveEatupRedux';
import PermissionsActions from '../Redux/PermissionsRedux';
import EatupForm from '../Components/EatupForm';
import { Content } from '../Components/UI';
import CameraActions from '../Redux/CameraRedux';
import CurrentEatupActions from '../Redux/CurrentEatupRedux';
import ExifDataActions from '../Redux/ExifDataRedux';
import PlaceHeader from '../Components/PlaceHeader';
import PlacesActions from '../Redux/PlacesRedux';
import CreateEatupActions from '../Redux/CreateEatupRedux';
import PlacesModal from '../Components/PlacesModal';
import SavingModal from '../Components/SavingModal';

// Styles
import styles from './Styles/CreateEatupScreenStyle';
import EatupBoostHeader from '../Components/EatupBoostHeader';

class CreateEatupScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      savingModalVisible: false,
      selectedPlace: null,
      uploading: false,
    };
    this.handleBackButtonClick = this.showSavingModal.bind(this); // When user press back button, we save the eatup
  }

  componentDidMount() {
    const { requestPermissions, hasPermissions, payload } = this.props;
    setTimeout(() => {
      requestPermissions();
    }, 0);
    if (hasPermissions && payload !== null) this.saveImage(); // save each taken image in currentEatup adn gets location
    this.handler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.handleBackButtonClick();
      return true;
    });
    if (!this.props.fetchingPlace) this.setState({ selectedPlace: this.props.places[0] });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.hasPermissions !== this.props.hasPermissions) {
      this.saveImage();
    }
    if (prevProps.fetchingPlace !== this.props.fetchingPlace) {
      this.setState({ selectedPlace: this.props.places[0] });
    }
    if (prevProps.payload !== this.props.payload) {
      if (this.state.uploading === false) {
        this.saveImage();
        this.setState({ state: this.state });
      }
    }
  }

  componentWillUnmount() {
    const { cleanCameraCache } = this.props;
    cleanCameraCache();
    this.handler.remove();
  }

  onExit() {
    this.props.navigation.navigate('MainScreen');
    this.props.cleanEatup();
    this.props.cleanLocation();
    this.props.cleanExifData();
  }

  onSelectPlace(id) {
    const place = this.props.places[id];
    this.setState({ selectedPlace: place });
    this.hideModal();
  }

  saveImage() {
    const { currentEatup, currentEatupRequest, exifData, payload } = this.props;
    let image = null;
    const uri = this.picture();
    if (currentEatup.hasOwnProperty('images')) {
      image = currentEatup.images;
      currentEatupRequest({ images: [...image, uri] });
    } else {
      currentEatupRequest({ images: [uri] });
    }
    exifData({ uri, source: payload.source });
  }

  handleSubmit(values) {
    const { geolocation } = this.props;
    this.setState({ uploading: true });
    return this.props
      .createEatup({
        ...values,
        geolocation: {
          latitude: geolocation.location.latitude,
          longitude: geolocation.location.longitude,
          placeName: this.state.selectedPlace.name,
        },
      })
      .then(() => {
        this.props.cleanEatup();
        this.props.cleanLocation();
        this.props.cleanExifData();
        this.props.navigation.dismiss();
      });
  }

  handleSave() {
    const { currentProfile, currentEatup } = this.props;
    const data = {
      ...currentEatup,
      profileId: currentProfile,
      eatupId: uuidv4(),
    };
    this.props.navigation.dismiss();
    this.props.cleanEatup();
    this.props.cleanLocation();
    this.props.cleanExifData();
    return this.props.saveEatup(data);
  }

  picture() {
    const { payload } = this.props;
    if (payload) {
      const { source, photo } = payload;
      if (source === 'camera') {
        return photo.uri;
      }
      if (source === 'gallery') {
        return photo.node.image.uri;
      }
      return '';
    }
    return '';
  }

  handlePhotoAdded() {
    // this.props.navigation.navigate('MultiplePhoto', {
    //   dismissable: true,
    //   targetScreen: 'CreateEatupScreen',
    // });
    this.props.navigation.navigate('EatupPhoto', { dismissTarget: 'CreateEatupScreen' });
  }

  handleChange(values) {
    const { currentEatupRequest } = this.props;
    const eatup = {
      description: values.nativeEvent.text,
    };
    currentEatupRequest(eatup);
  }

  hanldeMindfulnessChange(ingredients) {
    const { currentEatupRequest } = this.props;
    const values = { ingredients };
    const data = {
      mindfulness: values,
    };
    currentEatupRequest(data);
  }

  handleMindfulnessPressed() {
    const { navigation } = this.props;
    navigation.navigate('MindfulnessWizard');
  }

  showModal() {
    this.setState({ modalVisible: true });
  }

  hideModal() {
    this.setState({ modalVisible: false });
  }

  showSavingModal() {
    this.setState({ savingModalVisible: true });
  }

  hideSavingModal() {
    this.setState({ savingModalVisible: false });
  }

  render() {
    const { currentEatup, places, fetchingPlace, hasPermissions } = this.props;
    return (
      <Content style={styles.container}>
        {!!hasPermissions && (
          <EatupBoostHeader
            // title="Dinner"
            onBackButtonPress={this.handleBackButtonClick}
            onPress={() => {}}
            mealTime
          />
        )}
        <PlaceHeader
          places={places}
          fetching={fetchingPlace}
          onPress={this.showModal.bind(this)}
          selectedPlace={this.state.selectedPlace}
        />
        <EatupForm
          onSubmit={this.handleSubmit.bind(this)}
          images={currentEatup.images}
          onPhotoAdded={this.handlePhotoAdded.bind(this)}
          onChange={this.handleChange.bind(this)}
          initialValues={currentEatup}
          onMindfulnessWizardChange={this.hanldeMindfulnessChange.bind(this)}
          onMindfulnessButtonPressed={this.handleMindfulnessPressed.bind(this)}
          onSave={this.handleSave.bind(this)}
          uploading={this.state.uploading}
        />
        <PlacesModal
          places={places}
          visible={this.state.modalVisible}
          onClose={this.hideModal.bind(this)}
          fetching={this.fetchingPlace}
          onSelect={this.onSelectPlace.bind(this)}
        />
        <SavingModal
          visible={this.state.savingModalVisible}
          onSave={this.handleSave.bind(this)}
          onExit={this.onExit.bind(this)}
          onCancel={this.hideSavingModal.bind(this)}
          text="Do you want to save changes?"
        />
      </Content>
    );
  }
}

const mapStateToProps = ({
  camera,
  permissions,
  currentProfile,
  currentEatup,
  places,
  exifData,
  createEatup,
}) => {
  const { payload } = camera;
  return {
    payload,
    hasPermissions: permissions.grantedLocationPermissions,
    currentProfile: currentProfile.payload.uid,
    currentEatup: currentEatup.data,
    places: places.payload,
    fetchingPlace: places.fetching,
    geolocation: exifData.payload,
    uploadingEatup: createEatup.fetching,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    currentEatupRequest: data => dispatch(CurrentEatupActions.currentEatupRequest(data)),
    cleanEatup: () => dispatch(CurrentEatupActions.cleanEatup()),
    saveEatup: data => dispatch(SaveEatupActions.saveEatupRequest(data)),
    cleanCameraCache: () => dispatch(CameraActions.cleanCameraPayload()),
    requestPermissions: () => dispatch(PermissionsActions.permissionsLocationRequest()),
    exifData: data => dispatch(ExifDataActions.exifDataRequest(data)),
    cleanLocation: () => dispatch(PlacesActions.placesClean()),
    cleanExifData: () => dispatch(ExifDataActions.cleanPayload()),
    createEatup: data =>
      new Promise((resolve, reject) => {
        dispatch(CreateEatupActions.createEatupRequest(data, resolve, reject));
      }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateEatupScreen);
