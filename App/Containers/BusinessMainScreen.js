import React, { Component } from 'react';
import { ScrollView, KeyboardAvoidingView } from 'react-native';
import { connect } from 'react-redux';
import { Text, Content } from '../Components/UI';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/BusinessMainScreenStyle';

class BusinessMainScreen extends Component {
  render() {
    return (
      <Content style={styles.container}>
        <KeyboardAvoidingView behavior="position">
          <Text>BusinessMainScreen</Text>
        </KeyboardAvoidingView>
      </Content>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BusinessMainScreen);
