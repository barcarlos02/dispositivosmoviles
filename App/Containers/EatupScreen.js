import React, { Component } from 'react';
import { ScrollView, KeyboardAvoidingView } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Text, Content, Container } from '../Components/UI';

// Styles
import styles from './Styles/EatupScreenStyle';

class EatupScreen extends Component {
  static propTypes = {
    eatup: PropTypes.instanceOf(Object).isRequired,
  };

  // Default Props
  // static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container style={styles.container}>
        <Container />
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EatupScreen);
