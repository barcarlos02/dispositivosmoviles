import { StyleSheet } from 'react-native';
import { ApplicationStyles, Colors, Fonts, Metrics } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  image: {
    resizeMode: 'cover',
    height: Metrics.screenHeight / 8,
    width: Metrics.screenWidth / 2.5,
    borderRadius: 5,
    marginBottom: 10,
    marginLeft: 5,
  },
  imageContainer: {
    margin: 20,
    alignItems: 'center',
  },
});
