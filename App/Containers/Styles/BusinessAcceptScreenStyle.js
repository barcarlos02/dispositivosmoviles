import { StyleSheet, Platform } from 'react-native';
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../Themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  text: {
    ...ApplicationStyles.screen.text,
    textAlign: 'center',
  },
  wrapper: {
    flex: 1,
    alignSelf: 'center',
    paddingBottom: 30,
    flexDirection: 'column',
    justifyContent: 'space-between',
    ...Platform.select({
      ios: {
        paddingTop: 90,
      },
      android: {
        paddingTop: 45,
      },
    }),
  },
  greetingText: {
    fontSize: Metrics.screenWidth / 8,
    marginBottom: 15,
  },
  sloganText: {
    fontSize: Fonts.size.h5,
  },
  banner: {
    marginTop: 15,
    marginBottom: 30,
    paddingVertical: 0,
    paddingTop: 10,
    paddingBottom: 25,
  },
  content: {
    paddingHorizontal: 60,
  },
  textAccent: {
    color: Colors.accent,
  },
  buttonStartWrapper: {
    marginBottom: 65,
  },
  buttonStart: {
    backgroundColor: Colors.accent,
  },
  buttonStartText: {
    color: Colors.snow,
  },
  textMargin: {
    marginBottom: 30,
  },
  tryText: {
    fontSize: Fonts.size.h5,
  },
});
