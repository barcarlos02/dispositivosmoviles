import { StyleSheet } from 'react-native';
import { Colors, Metrics, Fonts } from '../../Themes';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    // justifyContent: 'center',
    backgroundColor: Colors.background,
  },
  mainText: {
    textAlign: 'center',
    color: 'red',
    fontFamily: Fonts.customFonts.accent,
    fontSize: Fonts.size.h1,
    paddingBottom: 15,
  },
  centerText: {
    textAlign: 'center',
    fontFamily: Fonts.customFonts.light,
    fontSize: Fonts.size.h6,
  },
  topContainer: {
    paddingTop: 30,
    overflow: 'hidden',
  },
  logoContainer: {
    textAlign: 'center',
  },
  downContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    paddingBottom: 100,
  },
});
