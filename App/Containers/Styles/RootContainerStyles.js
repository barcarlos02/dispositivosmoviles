import { StyleSheet } from 'react-native';
import { Fonts, Metrics, Colors } from '../../Themes';

export default StyleSheet.create({
  applicationView: {
    flex: 1,
  },
  safeAreaView: {
    flex: 1,
    backgroundColor: Colors.main,
  },
  safeAreaViewStatus: {
    flex: 0,
    backgroundColor: Colors.main,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.background,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    fontFamily: Fonts.type.base,
    margin: Metrics.baseMargin,
  },
  myImage: {
    width: 200,
    height: 200,
    alignSelf: 'center',
  },
});
