import { StyleSheet, Platform } from 'react-native';
import { ApplicationStyles, Fonts } from '../../Themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  wrapper: {
    flex: 1,
    paddingBottom: 55,
    paddingHorizontal: 30,
    ...Platform.select({
      ios: {
        paddingTop: 90,
      },
      android: {
        paddingTop: 45,
      },
    }),
  },
  text: {
    ...ApplicationStyles.screen.text,
    fontSize: Fonts.size.h3,
  },
});
