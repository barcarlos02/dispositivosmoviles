import { StyleSheet, Platform } from 'react-native';
import { ApplicationStyles, Metrics, Colors } from '../../Themes';

export default StyleSheet.create({
  container: {
    ...ApplicationStyles.screen.container,
    paddingHorizontal: 0,
  },
  // modal: {
  //   height: Metrics.screenHeight,
  //   position: 'absolute',
  //   left: 0,
  //   right: 0,
  //   alignContent: 'stretch',
  // },
});
