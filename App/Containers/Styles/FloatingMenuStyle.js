import { StyleSheet } from 'react-native';
import { Colors, Metrics, Fonts } from '../../Themes';

const height = Metrics.navBarHeight;
const width = Metrics.screenWidth;

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.bloodOrange,
  },
  touchableWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  icon: {
    color: Colors.accent,
  },
  title: {
    color: Colors.accent,
    fontFamily: Fonts.customFonts.semiBold,
    fontSize: 16, // or Fonts.size.medium,
    textAlign: 'center',
    textAlignVertical: 'center',
    // fontWeight: 'bold',
  },
  boostBtn: {
    position: 'absolute',
    right: width / 4,
  },
  boostContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: Math.round(height * 1.1),
    width: Math.round(height * 1.1),
    borderRadius: (height * 1.1) / 2,
    backgroundColor: Colors.main,
  },
  eatupBtn: {
    position: 'absolute',
    left: width / 4,
  },
  eatupContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: Math.round(height * 1.1),
    width: Math.round(height * 1.1),
    borderRadius: (height * 1.1) / 2,
    backgroundColor: Colors.main,
  },
  closeButton: {
    position: 'absolute',
    /* bottom: Math.round(height * 0.8),
    left: width / 2, */
    transform: [
      {
        translateX: -1 * (Math.round(height * 1.1) / 2),
      },
      {
        translateY: -1 * (Math.round(height * 1.1) / 2),
      },
    ],
  },
  closeContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
    height: Math.round(height * 1.1),
    width: Math.round(height * 1.1),
    borderRadius: (height * 1.1) / 2,
    backgroundColor: Colors.accent,
  },
});
