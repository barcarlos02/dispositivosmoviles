import { StyleSheet } from 'react-native';
import { Colors, Fonts } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.background,
    paddingTop: 30,
  },
  mainText: {
    fontFamily: Fonts.customFonts.medium,
    fontSize: Fonts.size.h2,
  },
  centerText: {
    textAlign: 'center',
    fontFamily: Fonts.customFonts.light,
    fontSize: Fonts.size.regular,
  },
  secondaryContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  buttonContainer: {
    paddingRight: 60,
    paddingLeft: 60,
    paddingTop: 10,
    paddingBottom: 8,
  },
  redText: {
    color: Colors.fire,
  },
  bottomContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
});
