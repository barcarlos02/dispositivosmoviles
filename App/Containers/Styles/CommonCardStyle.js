import { StyleSheet } from 'react-native';
import Color from 'color';
import { Colors, Metrics, Fonts } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.text,
    borderRadius: 5,
    padding: 0,
    overflow: 'hidden',
  },
  imageContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  image: {
    // resizeMode: 'contain',
  },
  imageContent: {
    flex: 1,
    backgroundColor: Color(Colors.panther).fade(0.6),
  },
  infoContainer: {
    alignSelf: 'stretch',
    flex: 2,
  },
  btnContainer: {
    flex: 3,
    justifyContent: 'flex-start',
    alignItems: 'center',
    position: 'relative',
    top: -20,
  },
  iconContainer: {
    backgroundColor: Colors.background,
  },
  content: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  description: {
    backgroundColor: Color(Colors.panther).alpha(0.45),
    paddingVertical: 10,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  descpriptionText: {
    ...Fonts.style.normal,
    color: Colors.snow,
  },
  icon: {
    position: 'absolute',
    zIndex: 16,
    top: -2,
    right: -2,
    color: Colors.accent,
  },
  loadingWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inForm: {
    padding: 7,
  },
});
