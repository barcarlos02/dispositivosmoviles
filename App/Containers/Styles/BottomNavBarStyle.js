import { StyleSheet } from 'react-native';
import { Colors, Metrics } from '../../Themes';

// const height = Metrics.screenHeight;
const width = Metrics.screenWidth;
const height = Metrics.navBarHeight;

export default StyleSheet.create({
  container: {
    flex: 0,
    width,
    height: Math.round(height * 1.35),
    backgroundColor: Colors.transparent,
    position: 'absolute',

    bottom: 0,
    left: 0,
  },
  centerButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: Math.round(height * 0.55),
    left: width / 2,
    zIndex: 17,
    transform: [
      {
        translateX: -1 * (Math.round(height * 1.1) / 2),
      },
      {
        translateY: -1 * (Math.round(height * 1.1) / 2),
      },
    ],
  },
  buttonContent: {
    alignSelf: 'stretch',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
    height: Math.round(height * 1.1),
    width: Math.round(height * 1.1),
    borderRadius: (height * 1.1) / 2,
    backgroundColor: Colors.accent,
  },
  tabRow: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.transparent,
    zIndex: 16,
    padding: 0,
    height: Math.round(height * 0.8),
    width,
    maxHeight: Math.round(height * 0.8),
  },
  tabElement: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    backgroundColor: Colors.main,
  },
  icon: {
    textAlign: 'center',
    textAlignVertical: 'center',
    alignSelf: 'stretch',
  },
});
