import { StyleSheet } from 'react-native';
import { Colors, Metrics, Fonts } from '../../Themes';

export default StyleSheet.create({
  logo: {
    width: Metrics.screenWidth / 1.6,
  },
  title: {
    color: Colors.accent,
    fontSize: Fonts.size.h4,
    fontFamily: Fonts.customFonts.semiBold,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  icon: {
    color: Colors.accent,
  },
  centerButton: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 5,
  },
  centerBtnIcon: {
    flex: 2,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnTitle: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 5,
  },
});
