import { StyleSheet } from 'react-native';
import { ApplicationStyles, Metrics } from '../../Themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  map: {
    alignSelf: 'stretch',
    position: 'absolute',
    top: 0,
    left: 0,
    width: Metrics.screenWidth,
    height: Metrics.screenHeight - Metrics.navBarHeight * 1.3,
  },
});
