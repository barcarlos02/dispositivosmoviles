import { StyleSheet } from 'react-native';
import { ApplicationStyles, Colors, Fonts, Metrics } from '../../Themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  loadingWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  jumbotron: {
    borderRadius: 5,
  },
  jumbotronTitle: {
    // color: Colors.accent,
    fontFamily: Fonts.customFonts.regular,
  },
  updateItem: {
    flex: 0,
    justifyContent: 'flex-start',
    alignItems: 'center',
    // paddingRight: Metrics.screenWidth / 4.5,
    paddingBottom: 0,
    paddingTop: 5,
    height: Metrics.navBarHeight * 1.5,
    // maxHeight: Metrics.navBarHeight * 0.65,
    backgroundColor: Colors.transparent,
  },
  list: {
    paddingBottom: Metrics.navBarHeight * 1.5,
  },
});
