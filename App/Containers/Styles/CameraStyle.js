import Color from 'color';
import { StyleSheet } from 'react-native';
import { Colors } from '../../Themes';

const SIZE = 80;
const CLOSE_SIZE = 40;
const targetWidth = 180;

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.panther,
    position: 'relative',
  },
  preview: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  takePictureContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
  },
  takePicture: {
    flex: 0,
    width: SIZE,
    height: SIZE,
    borderRadius: SIZE / 2,
    backgroundColor: Colors.main,
    borderColor: Colors.darkMain,
    borderWidth: 3,
    alignSelf: 'center',
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  target: {
    width: targetWidth,
    height: targetWidth,
    borderRadius: 10,
    borderColor: Color(Colors.snow).alpha(0.3),
    borderWidth: 4,
    backgroundColor: Colors.transparent,
  },
  closeBtnContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  closeBtn: {
    flex: 0,
    width: CLOSE_SIZE,
    height: CLOSE_SIZE,
    borderRadius: CLOSE_SIZE / 2,
    backgroundColor: Colors.main,
    borderColor: Colors.darkMain,
    borderWidth: 3,
    alignSelf: 'center',
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
