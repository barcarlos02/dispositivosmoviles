import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  cardWrapper: {
    flex: 1,
    marginVertical: 2,
    marginHorizontal: 3,
    height: 275,
  },
});
