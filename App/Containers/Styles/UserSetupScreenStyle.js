import { StyleSheet, Platform } from 'react-native';
import { ApplicationStyles, Fonts, Colors } from '../../Themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    paddingBottom: 55,
    paddingHorizontal: 20,
    ...Platform.select({
      ios: {
        paddingTop: 50,
      },
      android: {
        paddingTop: 25,
      },
    }),
  },
  text: {
    ...ApplicationStyles.screen.text,
    fontSize: Fonts.size.h2,
    fontFamily: Fonts.customFonts.accent,
    color: Colors.accent,
    lineHeight: 70,
  },
  textCenter: {
    textAlign: 'center',
  },
  textWrapper: {
    flex: 1,
  },
  formWrapper: {
    flex: 1,
    justifyContent: 'space-evenly',
  },
});
