import { StyleSheet } from 'react-native';
import { Fonts, Colors } from '../../Themes';

export default StyleSheet.create({
  headerText: {
    textAlign: 'center',
    padding: 20,
    color: Colors.text,
    marginTop: 20,
    fontFamily: Fonts.customFonts.regular,
    fontSize: Fonts.size.h5,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: Colors.background,
  },
  note: {
    textAlign: 'center',
    padding: 20,
    color: Colors.text,
    marginTop: 20,
    fontFamily: Fonts.customFonts.regular,
    fontSize: Fonts.size.medium,
  },
});
