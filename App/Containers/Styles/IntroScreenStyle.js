import { StyleSheet, Platform } from 'react-native';
import { ApplicationStyles, Metrics, Fonts, Colors } from '../../Themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  text: {
    ...ApplicationStyles.screen.text,
    textAlign: 'center',
  },
  wrapper: {
    flex: 1,
    width: Metrics.screenWidth / 1.3,
    alignSelf: 'center',
    paddingBottom: 30,
    flexDirection: 'column',
    justifyContent: 'space-between',
    ...Platform.select({
      ios: {
        paddingTop: 90,
      },
      android: {
        paddingTop: 45,
      },
    }),
  },
  section: {
    flex: 1,
  },
  introText: {
    fontSize: Fonts.size.h6,
  },
  sloganText: {
    fontSize: Fonts.size.h1,
  },
  sloganTextAccent: {
    color: Colors.accent,
    fontSize: Fonts.size.h2,
    fontFamily: Fonts.customFonts.accent,
  },
  signinText: {
    fontSize: Fonts.size.small,
  },
  sloganWrapper: {
    position: 'relative',
    flex: 3,
  },
  sloganImageWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    position: 'absolute',
    top: 70,
    left: 0,
    right: 0,
  },
  signInButtonWrapper: {
    marginTop: 5,
  },
  signInButton: {
    backgroundColor: Colors.snow,
  },
  signInButtonText: {
    color: Colors.secondary,
  },
});
