import React, { Component } from 'react';
import { ScrollView, View, KeyboardAvoidingView } from 'react-native';
import { connect } from 'react-redux';
import { Text, Container, Content } from '../Components/UI';
import UserSetupForm from '../Components/UserSetupForm';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import SubmitProfileActions from '../Redux/SubmitProfileRedux';

// Styles
import styles from './Styles/UserSetupScreenStyle';

class UserSetupScreen extends Component {
  submit(values) {
    const { submitFoodie } = this.props;
    return submitFoodie(values).then(resp => {
      if (resp) {
        if (this.props.navigation.getParam('menu') === true) {
          this.props.navigation.dismiss();
        } else {
          this.props.navigation.navigate('SignedIn');
        }
      }
    });
  }

  render() {
    const { user } = this.props;
    return (
      <Container style={styles.container}>
        <Content style={styles.container}>
          <KeyboardAvoidingView enabled>
            <View style={styles.wrapper}>
              <View style={styles.textWrapper}>
                <Text style={[styles.text, styles.textCenter]}>{`Hello mindful\nfoodie!!!`}</Text>
              </View>
              <UserSetupForm
                style={styles.formWrapper}
                actualName={user.displayName}
                imgUrl={user.photoURL}
                onSubmit={this.submit.bind(this)}
              />
            </View>
          </KeyboardAvoidingView>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({ currentUser, submitProfile }) => {
  return {
    user: currentUser.payload,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    submitFoodie: data =>
      new Promise((resolve, reject) => {
        dispatch(SubmitProfileActions.submitProfileUser(data, resolve, reject));
      }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserSetupScreen);
