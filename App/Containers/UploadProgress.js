import React, { Component } from 'react';
import { View } from 'react-native';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { connect } from 'react-redux';
import { Text } from '../Components/UI';
import { Colors } from '../Themes';
import Fade from '../Components/Fade';

// Styles
import styles from './Styles/UploadProgressStyle';

const SIZE = 35;

class UploadProgress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      progress: 0,
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.progress !== prevProps.progress) {
      this.updateProgress(this.props.progress);
    }
  }

  updateProgress(progress = 0) {
    this.setState({ progress }, () => {
      if (this.state.progress === 100) {
        setTimeout(() => {
          this.setState({ progress: 0 });
        }, 1500);
      }
    });
  }

  render() {
    const { progress } = this.state;

    const width = SIZE / 8;
    const fontSize = SIZE / 4;

    return (
      <Fade visible={progress !== 0}>
        <View style={styles.container}>
          <AnimatedCircularProgress
            size={SIZE}
            width={width}
            fill={progress}
            tintColor={Colors.success}
            backgroundColor={Colors.darkMain}
            lineCap="round"
          >
            {fill => <Text style={{ color: Colors.success, fontSize }}>{Math.round(fill)}%</Text>}
          </AnimatedCircularProgress>
          <Text style={{ color: Colors.success, fontSize }}>Uploading</Text>
        </View>
      </Fade>
    );
  }
}

const mapStateToProps = ({ uploadImage }) => {
  const { progress } = uploadImage;
  return { progress };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UploadProgress);
