import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, ImageBackground } from 'react-native';
import { connect } from 'react-redux';
import { isEqual } from 'lodash';
import Icon from '../Components/CustomIcon';
import IconButton from '../Components/IconButton';
import CommonBanner from '../Components/CommonBanner';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import { Colors, Metrics } from '../Themes';
import styles from './Styles/CommonCardStyle';
import { Container } from '../Components/UI';

// eslint-disable-next-line react/prefer-stateless-function
class CommonCard extends Component {
  // Prop type warnings
  static propTypes = {
    label: PropTypes.string,
    showCamera: PropTypes.bool,
    children: PropTypes.node,
    form: PropTypes.bool,
    boost: PropTypes.object,
    amount: PropTypes.string,
    discount: PropTypes.string,
    owner: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    color: PropTypes.string,
    onCameraPress: PropTypes.func,
    onCameraPressCallback: PropTypes.func,
  };

  // // Defaults for props
  static defaultProps = {
    label: null,
    showCamera: false,
    children: null,
    form: false,
    boost: null,
    amount: null,
    discount: null,
    color: Colors.text,
    onCameraPress: () => {},
    onCameraPressCallback: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidUpdate(prevProps) {
    const { picture } = this.props;
    if (!isEqual(prevProps.picture, picture)) {
      this.handleCamera();
    }
  }

  handleCamera = () => {
    const { onCameraPressCallback } = this.props;
    const photo = this.picture();
    onCameraPressCallback(photo);
  };

  picture() {
    const { picture } = this.props;
    if (picture) {
      const { source, photo } = picture;
      if (source === 'camera') {
        return photo.uri;
      }
      if (source === 'gallery') {
        return photo.node.image.uri;
      }
      return '';
    }
    return '';
  }

  render() {
    const {
      label,
      showCamera,
      children,
      form,
      amount,
      discount,
      owner,
      name,
      color,
      onCameraPress,
    } = this.props;
    const image = this.picture();
    return (
      <Container
        style={[styles.container, form && { padding: 7 }, color && { backgroundColor: color }]}
      >
        <ImageBackground
          source={image ? { uri: image } : require('../Images/boostCardImg.jpg')}
          style={styles.imageContainer}
          imageStyle={styles.image}
        >
          <View style={styles.imageContent}>
            {!!label && <Icon name={label} size={40} style={styles.icon} />}
            <View style={styles.infoContainer}>
              <CommonBanner name={name} owner={owner} amount={amount} discount={discount} />
            </View>
            {!!children && <View style={styles.content}>{children}</View>}
            {showCamera && (
              <View style={[styles.btnContainer]}>
                <IconButton
                  icon="ic_camera_simplified"
                  size={Metrics.screenWidth / 4.5}
                  title="it works much better with a pretty photo"
                  onPress={onCameraPress}
                  containerStyle={styles.iconContainer}
                />
              </View>
            )}
          </View>
        </ImageBackground>
      </Container>
    );
  }
}

const mapStateToProps = ({ camera }) => {
  return {
    picture: camera.payload,
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CommonCard);
