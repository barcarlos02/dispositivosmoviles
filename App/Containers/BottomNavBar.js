import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Platform, Keyboard, Animated } from 'react-native';
import { map } from 'lodash';
import Icon from '../Components/CustomIcon';
import styles from './Styles/BottomNavBarStyle';
import { Colors, Metrics } from '../Themes';
import { Touchable, Container } from '../Components/UI';
import FloatingMenu from './FloatingMenu';
import FirstTimeActions, { disable } from '../Redux/FirstTimeRedux';

const size = Math.round(Metrics.navBarHeight * 0.45);

class BottomNavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyboard: false,
      visible: new Animated.Value(1),
      open: false,
      centerButtonPosition: {},
    };
  }

  componentDidMount() {
    if (Platform.OS === 'ios') {
      Keyboard.addListener('keyboardWillShow', this._handleKeyboardShow);
      Keyboard.addListener('keyboardWillHide', this._handleKeyboardHide);
    } else {
      Keyboard.addListener('keyboardDidShow', this._handleKeyboardShow);
      Keyboard.addListener('keyboardDidHide', this._handleKeyboardHide);
    }
  }

  getButtonPostion() {
    if (this.centerButton) {
      this.centerButton.measure((x, y, width, height, pageX, pageY) => {
        this.setState({
          centerButtonPosition: { x, y, width, height, pageX, pageY },
        });
      });
    }
  }

  _handleKeyboardShow = () =>
    this.setState({ keyboard: true }, () =>
      Animated.timing(this.state.visible, {
        toValue: 0,
        duration: 150,
        useNativeDriver: true,
      }).start()
    );

  _handleKeyboardHide = () =>
    Animated.timing(this.state.visible, {
      toValue: 1,
      duration: 100,
      useNativeDriver: true,
    }).start(() => {
      this.setState({ keyboard: false });
    });

  handleClick(route) {
    const { onTabPress } = this.props;
    onTabPress({ route });
  }

  toogleMenu() {
    this.setState(prevState => ({ ...prevState, open: !prevState.open }));
  }

  hideUploadScreen() {
    const { disableUpload, currentProfile } = this.props;
    disableUpload(currentProfile.uid);
  }

  openBoostNavigation() {
    this.props.navigation.navigate('CreateBoost');
  }

  openEatupNavigation() {
    this.props.navigation.navigate('CreateEatup', {
      dismissable: true,
      targetScreen: 'CreateEatupScreen',
    });
  }

  renderFirstLine() {
    const { navigation } = this.props;
    const activeLink = map(navigation.state.routes, (value, index) => {
      return { active: index === navigation.state.index, routeName: value };
    });

    return (
      <View style={styles.tabRow}>
        {this.renderTabElement('ic_view_card', activeLink[0])}
        {this.renderTabElement('ic_location', activeLink[1])}
      </View>
    );
  }

  renderTabElement(iconName, { routeName, active }) {
    const { activeTintColor, inactiveTintColor } = this.props;
    return (
      <Touchable style={{ flex: 1 }} onPress={this.handleClick.bind(this, routeName)}>
        <Container style={styles.tabElement}>
          <Icon
            name={iconName}
            size={size}
            style={[{ color: active ? activeTintColor : inactiveTintColor }, styles.icon]}
          />
        </Container>
      </Touchable>
    );
  }

  renderCenterElement() {
    const { currentProfile } = this.props;
    return (
      <View
        style={styles.centerButton}
        ref={ref => {
          this.centerButton = ref;
        }}
        onLayout={this.getButtonPostion.bind(this)}
      >
        <Touchable
          type="opacity"
          activeOpacity={0.95}
          onPress={() => {
            // this.toogleMenu();
            if (currentProfile.type === 'foodie') {
              this.openEatupNavigation();
            } else {
              this.openBoostNavigation();
            }
            this.hideUploadScreen();
          }}
        >
          <Container style={styles.buttonContent}>
            <Icon name="ic_fab_plus" size={size} style={[{ color: Colors.snow }, styles.icon]} />
          </Container>
        </Touchable>
      </View>
    );
  }

  render() {
    const { open } = this.state;
    return (
      <Animated.View
        pointerEvents="box-none"
        style={[
          styles.container,
          this.state.keyboard && {
            // When the keyboard is shown, slide down the tab bar
            transform: [
              {
                translateY: this.state.visible.interpolate({
                  inputRange: [0, 1],
                  outputRange: [Metrics.screenHeight * 0.14, 0],
                }),
              },
              {
                scale: this.state.visible.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, 1],
                }),
              },
            ],
            // Absolutely position the tab bar so that the content is below it
            // This is needed to avoid gap at bottom when the tab bar is hidden
            position: this.state.keyboard ? 'absolute' : 'relative',
          },
        ]}
      >
        {this.renderCenterElement()}
        <FloatingMenu
          openMenu={open}
          onBack={this.toogleMenu.bind(this)}
          eatupPress={this.openEatupNavigation.bind(this)}
          boostPress={this.openBoostNavigation.bind(this)}
          position={this.state.centerButtonPosition}
        />
        <Container
          style={{
            position: 'absolute',
            bottom: 0,
            left: 0,
          }}
        >
          {this.renderFirstLine()}
        </Container>
      </Animated.View>
    );
  }
}

const mapStateToProps = ({ currentProfile }) => {
  return { currentProfile: currentProfile.payload };
};

const mapDispatchToProps = dispatch => {
  return {
    disableUpload: payload => dispatch(FirstTimeActions.firstTimeDisable(payload)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BottomNavBar);
