import React, { Component } from 'react';
import { connect } from 'react-redux';
import Container from '../Components/UI/Container';
import Text from '../Components/UI/Text';
import Button from '../Components/UI/Button';
import Colors from '../Themes/Colors';

// Styles
import style from './Styles/NewAccountScreenStyle';

class NewAccountScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container style={style.container}>
        <Container>
          <Text style={[style.centerText, style.mainText]}>Create a new</Text>
          <Text style={[style.centerText, style.mainText]}>account</Text>
        </Container>
        <Container style={style.secondaryContainer}>
          <Text style={style.centerText}>I&apos;m a mindful foodie!...or wanna be one</Text>
          <Container style={[style.buttonContainer]}>
            <Button
              title="FOODIE ACCOUNT"
              onPress={() => {
                this.props.navigation.navigate('FoodieAccount', { menu: true });
              }}
            />
          </Container>
        </Container>
        <Container style={[style.bottomContainer]}>
          <Text style={style.centerText}>I have a businnes and provide mindfull options?</Text>
          <Container style={style.buttonContainer}>
            <Button
              title="BUSINESS ACCOUNT"
              variant="secondary"
              type="outline"
              textColor={Colors.fire}
              onPress={() => {
                this.props.navigation.navigate('BusinessSetUp', { menu: true });
              }}
            />
          </Container>
          <Text style={[style.centerText, style.redText]}>Find out about businnes accounts</Text>
        </Container>
      </Container>
    );
  }
}
const mapStateToProps = () => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewAccountScreen);
