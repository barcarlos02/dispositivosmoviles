import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Formik, Field, FastField } from 'formik';
import * as Yup from 'yup';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { Text, Button } from '../Components/UI';
import { TextInput, IconsSelector, BookingSelector, BoostCard } from '../Components/Forms';
import { Colors, Metrics } from '../Themes';
import CurrentBoostActions from '../Redux/CurrentBoostRedux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/NewBoostFormStyle';

const options = [
  { image: 'ic_logo_eatups', label: 'takeaway', value: 'takeaway' },
  { image: 'ic_logo_eatups', label: 'eat in', value: 'eatIn' },
];

const atributes = [
  { image: 'ic_logo_eatups', label: 'vegetarian', value: 'vegetarian' },
  { image: 'ic_logo_eatups', label: 'diary free', value: 'diary free' },
  { image: 'ic_logo_eatups', label: 'nut free', value: 'nut free' },
  { image: 'ic_logo_eatups', label: 'glutein free', value: 'glutein free' },
];

class NewBoostForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    initialValues: PropTypes.instanceOf(Object),
    hasOptions: PropTypes.bool,
    cardConfig: PropTypes.instanceOf(Object).isRequired,
    onCameraPress: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
  };

  static defaultProps = {
    initialValues: {
      description: '',
      price: '0',
      discount: '0',
      amount: '1',
      booking: false,
      atributes: [],
      image: '',
    },
    hasOptions: false,
  };

  constructor(props) {
    super(props);
    this.state = { button: null };
  }

  render() {
    const {
      initialValues,
      onSubmit,
      hasOptions,
      cardConfig,
      onCameraPress,
      onSave,
      currentBoost,
    } = this.props;
    const { button } = this.state;
    return (
      <Formik
        initialValues={initialValues}
        onSubmit={(values, actions) => {
          onSubmit({ button, values })
            .then(() => actions.setSubmitting(false))
            .catch(e => {
              actions.setSubmitting(false);
            });
        }}
        validationSchema={Yup.object().shape(
          {
            description: Yup.string()
              .trim()
              .lowercase()
              .required('You need to add a description!'),
            price: Yup.number().when('discount', {
              is: d => !d,
              then: Yup.number()
                .min(1, 'The minimum is $1')
                .required('A price is needed'),
            }),
            discount: Yup.number().when('price', {
              is: p => !p,
              then: Yup.number()
                .min(1, 'The minimum is 1%')
                .lessThan(100, "Can't be a 100%")
                .required('A discount is needed'),
            }),
            // discount: Yup.number().required('You need to add a discount'),
            amount: Yup.number()
              .min(1, 'Minimum amount is 1')
              .max(50, 'Maximum amount is 50')
              .round(),
            options: hasOptions
              ? Yup.array().required('You need to select at least one option!')
              : Yup.array(),
            atributes: Yup.array().required('You need to select at least one option!'),
            image: Yup.string().required('You need to add a photo'),
          },
          ['price', 'discount']
        )}
        render={({ values, handleSubmit, isSubmitting, isValid, handleChange }) => (
          <React.Fragment>
            <KeyboardAwareScrollView>
              <View style={styles.cardContainer}>
                <Field
                  component={BoostCard}
                  name="image"
                  boostName={cardConfig.name}
                  owner={cardConfig.owner}
                  amount={values.price === '0' ? null : values.price}
                  discount={values.discount === '0' ? null : values.discount}
                  color={cardConfig.color}
                  onPress={onCameraPress}
                />
              </View>

              <View style={styles.inputsContainer}>
                <View style={styles.formContainer}>
                  <Field
                    component={TextInput}
                    name="description"
                    placeholder={cardConfig.description || 'Dish name or short description'}
                    multiline
                    numberOfLines={11}
                    inputContainerStyle={{
                      borderRadius: 5,
                      overflow: 'hidden',
                    }}
                    inputStyle={styles.description}
                    // formGroupStyle={{ marginBottom: 15 }}
                    placeholderTextColor={Colors.text}
                  />

                  <View style={styles.row}>
                    <View style={styles.priceContainer}>
                      <Text style={styles.priceLabel}>$</Text>
                      <Field
                        component={TextInput}
                        title="Price"
                        name="price"
                        placeholder="--"
                        containerStyle={styles.smallInput}
                        inputStyle={styles.price}
                        placeholderTextColor={Colors.text}
                        keyboardType="numeric"
                      />
                    </View>
                    <View style={styles.discountContainer}>
                      <Field
                        component={TextInput}
                        title="Discount"
                        name="discount"
                        placeholder="--"
                        containerStyle={styles.smallInput}
                        inputStyle={styles.discount}
                        rightIcon={() => <Text style={styles.discountIcon}>%</Text>}
                        rightIconContainerStyle={styles.discountIconContainer}
                        placeholderTextColor={Colors.accent}
                        keyboardType="numeric"
                      />
                    </View>
                  </View>

                  <View style={styles.row}>
                    <View style={styles.amountContainer}>
                      <Field
                        component={TextInput}
                        title="Amount available"
                        name="amount"
                        placeholder={initialValues.amount}
                        containerStyle={styles.smallInput}
                        inputStyle={styles.amount}
                        placeholderTextColor={Colors.text}
                        keyboardType="numeric"
                      />
                    </View>
                    <View style={styles.bookingContainer}>
                      <FastField
                        component={BookingSelector}
                        title="Requires Booking"
                        name="booking"
                      />
                    </View>
                  </View>

                  <View style={styles.selectorRow}>
                    <Field
                      component={IconsSelector}
                      name="atributes"
                      options={atributes}
                      size={Metrics.navBarHeight}
                      inverse
                      formGroupStyle={{ marginBottom: 15 }}
                    />
                  </View>

                  {!!hasOptions && (
                    <View style={[styles.selectorRow]}>
                      <Field
                        component={IconsSelector}
                        name="options"
                        options={options}
                        size={Metrics.navBarHeight}
                        formGroupStyle={{ marginBottom: 15 }}
                      />
                    </View>
                  )}
                </View>
              </View>
            </KeyboardAwareScrollView>

            <View style={styles.submitContainer}>
              <View style={[styles.btnLeftWrapper]}>
                <Button
                  title="SAVE"
                  disabledStyle={styles.btnDisabled}
                  disabledTitleStyle={styles.btnDisabledTitle}
                  titleStyle={styles.btnFontLeft}
                  type="clear"
                  raised
                  onPress={() => {
                    onSave(values);
                  }}
                  disabled={isSubmitting}
                />
              </View>
              <View style={[styles.btnRightWrapper]}>
                <Button
                  title="PUBLISH"
                  disabledStyle={styles.btnDisabled}
                  disabledTitleStyle={styles.btnDisabledTitle}
                  buttonStyle={styles.btnRight}
                  titleStyle={styles.btnFontRight}
                  raised
                  onPress={() => {
                    this.setState(prevProps => {
                      return { ...prevProps, button: true };
                    }, handleSubmit);
                  }}
                  disabled={isSubmitting}
                />
              </View>
            </View>
          </React.Fragment>
        )}
      />
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewBoostForm);
