import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image } from 'react-native-elements';
import { FlatList } from 'react-native';
import { Container, Spinner, Content } from '../Components/UI';
import MindfulnessWizard from '../Components/MindfulnessWizard';
import CurrentEatupActions from '../Redux/CurrentEatupRedux';

// Styles
import styles from './Styles/MindfulnessWizardScreenStyle';

class MindfulnessWizardScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { eatup } = this.props;
    return (
      <Content style={styles.container}>
        <Container style={styles.imageContainer}>
          <FlatList
            data={eatup.images}
            horizontal
            onEndThreshold={0}
            keyExtractor={image => ''.concat(Math.random())}
            renderItem={({ image, index }) => {
              return (
                <Image
                  style={styles.image}
                  source={
                    eatup.images
                      ? { uri: eatup.images[index] }
                      : require('../Images/boostCardImg.jpg')
                  }
                  PlaceholderContent={<Spinner />}
                />
              );
            }}
          />
        </Container>
        <MindfulnessWizard
          initialValues={eatup}
          onDone={() => {
            this.props.navigation.goBack();
          }}
        />
      </Content>
    );
  }
}

const mapStateToProps = ({ currentEatup }) => {
  return { eatup: currentEatup.data };
};

const mapDispatchToProps = dispatch => {
  return {
    saveMindfulnessData: data => dispatch(CurrentEatupActions.currentEatupRequest(data)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MindfulnessWizardScreen);
