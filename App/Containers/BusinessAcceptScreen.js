import React from 'react';
import { ScrollView, View } from 'react-native';
import { connect } from 'react-redux';
import { Text, Button, Content } from '../Components/UI';
import IconLabelWrapper from '../Components/IconLabelWrapper';
import Banner from '../Components/Banner';
import { Colors } from '../Themes';

import TermsActions from '../Redux/TermsRedux';
// Styles
import styles from './Styles/BusinessAcceptScreenStyle';

const items = [
  { icon: 'ic_avatar_business_default', label: 'Choose your mindful promo' },
  { icon: 'ic_camera_simplified', label: 'Personalise it' },
  { icon: 'ic_offer_label_business_eatups', label: 'Make deals and help the world' },
];

const BusinessAcceptScreen = props => {
  const _openTerms = () => {
    const { openBrowser } = props;
    openBrowser();
  };

  const _goToSetup = () => {
    props.navigation.navigate('BusinessSetupScreen');
  };

  return (
    <Content style={styles.container}>
      <View style={styles.wrapper}>
        <View>
          <Text style={[styles.text, styles.greetingText]}>Heyya' foodhero!</Text>
          <Text style={[styles.text, styles.sloganText, styles.textMargin]}>
            As a food business you can help people eat better: healthier and more sustainable.
          </Text>
        </View>
        <Banner style={styles.banner} color={Colors.secondary} title="IT'S THIS SIMPLE">
          <IconLabelWrapper icons={items} textColor={Colors.main} />
        </Banner>
        <View style={styles.content}>
          <Text style={[styles.text, styles.textMargin]}>
            Please read our{' '}
            <Text style={styles.textAccent} onPress={_openTerms}>
              Terms of Service and Privacy
            </Text>
            . By clicking "I'm in" you are accepting all that's in there. Let us know if you have
            any questions
          </Text>
          <Text style={[styles.text, styles.textMargin, styles.tryText]}>Try it out for free!</Text>
          <Button
            title="I'M IN!"
            containerStyle={styles.buttonStartWrapper}
            buttonStyle={styles.buttonStart}
            titleStyle={styles.buttonStartText}
            onPress={_goToSetup}
          />
        </View>
      </View>
    </Content>
  );
};

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    openBrowser: () => dispatch(TermsActions.termsRequest()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BusinessAcceptScreen);
