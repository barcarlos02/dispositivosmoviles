// leave off @2x/@3x
const images = {
  foodHot: require('../Images/illustration_food_hot.png'),
  ingredients: {
    plants: require('../Images/ingredients/plants.png'),
    grains: require('../Images/ingredients/grains.png'),
    eggs: require('../Images/ingredients/eggs.png'),
    birds: require('../Images/ingredients/birds.png'),
    fish: require('../Images/ingredients/fish.png'),
    pork: require('../Images/ingredients/pork.png'),
    dairy: require('../Images/ingredients/dairy.png'),
    beef: require('../Images/ingredients/beef.png'),
    lamb: require('../Images/ingredients/lamb.png'),
  },
};

export default images;
