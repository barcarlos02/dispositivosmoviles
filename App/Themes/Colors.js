import Color from 'color';

const colors = {
  main: '#E0E1CB',
  secondary: '#546966',
  accent: '#EE4B50',
  success: '#00B983',
  leaf: '#A3CD4B',
  background: '#ECECE0',
  clear: 'rgba(0,0,0,0)',
  facebook: '#3b5998',
  transparent: 'rgba(0,0,0,0)',
  silver: '#F7F7F7',
  steel: '#CCCCCC',
  error: 'rgba(200, 0, 0, 0.8)',
  ricePaper: 'rgba(255,255,255, 0.75)',
  frost: '#D8D8D8',
  cloud: 'rgba(200,200,200, 0.35)',
  windowTint: 'rgba(0, 0, 0, 0.4)',
  panther: '#161616',
  charcoal: '#595959',
  coal: '#2d2d2d',
  bloodOrange: '#fb5f26',
  snow: 'white',
  ember: 'rgba(164, 0, 48, 0.5)',
  fire: '#e73536',
  drawer: 'rgba(30, 30, 29, 0.95)',
  eggplant: '#251a34',
  border: '#483F53',
  banner: '#5F3E63',
  text: '#425C59',
  boostMarkerBackground: '#425c59',
  boostMarker: '#548355',
  green: '#9fb985',
  blue: '#75AEAF',
  brown: '#D0B16F',
};

export default {
  ...colors,
  darkMain: Color(colors.main)
    .darken(0.1)
    .toString(),
  darkBackground: Color(colors.background)
    .darken(0.1)
    .toString(),
  inactive: Color(colors.text)
    .lighten(0.95)
    .toString(),
};
