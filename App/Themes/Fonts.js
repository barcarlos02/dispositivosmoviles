const customFonts = {
  regular: 'Dosis-Regular',
  bold: 'Dosis-Bold',
  extraBold: 'Dosis-ExtraBold',
  semiBold: 'Dosis-SemiBold',
  medium: 'Dosis-Medium',
  light: 'Dosis-Light',
  extraLight: 'Dosis-ExtraLight',
  accent: 'PetitFormalScript-Regular',
};

const type = {
  base: customFonts.regular,
  bold: customFonts.bold,
  emphasis: customFonts.extraBold,
};

const size = {
  h1: 46,
  h2: 40,
  h3: 30,
  h4: 26,
  h5: 20,
  h6: 18,
  input: 18,
  regular: 17,
  medium: 14,
  small: 12,
  tiny: 10,
};

const style = {
  h1: {
    fontFamily: type.base,
    fontSize: size.h1,
  },
  h2: {
    fontWeight: 'bold',
    fontSize: size.h2,
  },
  h3: {
    fontFamily: type.emphasis,
    fontSize: size.h3,
  },
  h4: {
    fontFamily: type.base,
    fontSize: size.h4,
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5,
  },
  h6: {
    fontFamily: type.emphasis,
    fontSize: size.h6,
  },
  normal: {
    fontFamily: type.base,
    fontSize: size.regular,
  },
  description: {
    fontFamily: type.base,
    fontSize: size.medium,
  },
};

export default {
  type,
  size,
  style,
  customFonts,
};
