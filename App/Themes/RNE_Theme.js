import { Colors, Fonts } from '.';

export default {
  Button: {
    raised: true,
    buttonStyle: {
      borderRadius: 20,
      backgroundColor: Colors.main,
    },
    containerStyle: {
      overflow: 'hidden',
    },
    titleStyle: {
      color: Colors.accent,
    },
    loadingProps: {
      color: Colors.accent,
    },
  },
  Icon: {
    size: 20,
    color: Colors.accent,
    containerStyle: {
      paddingHorizontal: 5,
    },
  },
  Input: {
    containerStyle: {
      paddingHorizontal: 0,
    },
    inputContainerStyle: {
      borderColor: Colors.darkMain,
      borderWidth: 1,
      borderRadius: 5,
      backgroundColor: Colors.main,
    },
    inputStyle: {
      backgroundColor: Colors.main,
      paddingHorizontal: 10,
      color: Colors.text,
      textAlign: 'center',
    },
    errorStyle: {
      textAlign: 'center',
      fontSize: Fonts.size.small,
      color: Colors.error,
    },
  },
};
