import React from 'react';
import DismissableStackNavigator from './DismissableStackNavigator';

import CommonHeader from '../Components/CommonHeader';
import BoostSelectorScreen from '../Containers/BoostSelectorScreen';
import NewBoostScreen from '../Containers/NewBoostScreen';
import PhotoScreen from '../Components/PhotoScreen';

const screens = {
  BoostSelector: {
    screen: BoostSelectorScreen,
  },
  NewBoost: {
    screen: NewBoostScreen,
  },
  PhotoScreen: {
    screen: PhotoScreen,
  },
};

const config = {
  initialRouteName: 'BoostSelector',
  defaultNavigationOptions: {
    header: props => <CommonHeader {...props} />,
    title: 'Create new',
  },
  headerMode: 'screen',
};

const stack = DismissableStackNavigator(screens, config);

export default stack;
