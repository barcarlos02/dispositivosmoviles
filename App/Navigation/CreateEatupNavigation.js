import React from 'react';
import DismissableStackNavigator from './DismissableStackNavigator';

import CommonHeader from '../Components/CommonHeader';
import PhotoScreen from '../Components/PhotoScreen';
import CreateEatupScreen from '../Containers/CreateEatupScreen';
import MindfulnessWizardScreen from '../Containers/MindfulnessWizardScreen';
import MultiplePhotoNavigation from './MultiplePhotoNavigation';

const screens = {
  CreateEatupScreen: {
    screen: CreateEatupScreen,
  },
  MindfulnessWizard: {
    screen: MindfulnessWizardScreen,
  },
  EatupPhoto: {
    screen: PhotoScreen,
  },
  MultiplePhoto: {
    screen: MultiplePhotoNavigation,
    navigationOptions: {
      header: null,
    },
  },
};

const config = {
  initialRouteName: 'EatupPhoto',
  defaultNavigationOptions: {
    header: props => <CommonHeader {...props} />,
    title: 'Eatup',
  },
  headerMode: 'screen',
};

const stack = DismissableStackNavigator(screens, config);

export default stack;
