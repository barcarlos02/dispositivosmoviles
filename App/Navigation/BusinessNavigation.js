import { createStackNavigator } from 'react-navigation';

import styles from './Styles/NavigationStyles';
import BusinessMainScreen from '../Containers/BusinessMainScreen';

const screens = {
  MainScreen: {
    screen: BusinessMainScreen,
  },
};

const config = {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'MainScreen',
  navigationOptions: {
    headerStyle: styles.header,
  },
};

const stack = createStackNavigator(screens, config);

export default stack;
