import React from 'react';
import { createStackNavigator } from 'react-navigation';
import MainHeader from '../Containers/MainHeader';
import MainScreen from '../Containers/MainScreen';
import UploadLogoScreen from '../Containers/UploadLogoScreen';

const screens = {
  MainScreen: {
    screen: MainScreen,
  },
};

const config = {
  // Default config for all screens
  initialRouteName: 'MainScreen',
  defaultNavigationOptions: {
    header: props => <MainHeader {...props} />,
  },
};

const stack = createStackNavigator(screens, config);

export default stack;
