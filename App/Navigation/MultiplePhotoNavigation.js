import React from 'react';
import DismissableStackNavigator from './DismissableStackNavigator';
import PhotoScreen from '../Components/PhotoScreen';

const screens = {
  Photo: {
    screen: PhotoScreen,
  },
};

const config = {
  initialRouteName: 'Photo',
  defaultNavigationOptions: {
    header: null,
    dismissableTarget: 'CreateEatupScreen',
  },
  headerMode: 'screen',
};

const stack = DismissableStackNavigator(screens, config);

export default stack;
