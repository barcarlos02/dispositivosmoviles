import React from 'react';
import { createStackNavigator } from 'react-navigation';
import CommonHeader from '../Components/CommonHeader';
import styles from './Styles/NavigationStyles';
import MapScreen from '../Containers/MapScreen';

const screens = {
  MapScreen: {
    screen: MapScreen,
  },
};

const config = {
  // Default config for all screens
  initialRouteName: 'MapScreen',
  defaultNavigationOptions: {
    title: 'Locate Eatups',
    header: props => <CommonHeader {...props} />,
  },
};

const stack = createStackNavigator(screens, config);

export default stack;
