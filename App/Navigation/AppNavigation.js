import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import SignedInStack from './SignedIn';
import SignedOutStack from './SignedOut';
import AccountSetupStack from './AccountSetup';
import AuthLoadingScreen from '../Containers/AuthLoadingScreen';

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      SignedIn: SignedInStack,
      SignedOut: SignedOutStack,
      AccountSetup: AccountSetupStack,
    },
    {
      initialRouteName: 'AuthLoading',
    }
  )
);
