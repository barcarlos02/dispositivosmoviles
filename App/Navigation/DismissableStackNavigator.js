import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import CommonHeader from '../Components/CommonHeader';

export default function DismissableStackNavigator(routes, options) {
  const StackNav = createStackNavigator(routes, {
    ...options,
    dismissable: true,
    headerMode: 'screen',
  });

  return class DismissableStackNav extends Component {
    static router = StackNav.router;

    render() {
      const { state, goBack } = this.props.navigation;
      const nav = {
        ...this.props.navigation,
        dismiss: () => goBack(state.key),
        dismissable: true,
      };
      return <StackNav navigation={nav} />;
    }
  };
}
