import React from 'react';
import DismissableStackNavigator from './DismissableStackNavigator';

import CommonHeader from '../Components/CommonHeader';
import NewAccount from '../Containers/NewAccountScreen';
import UserSetup from '../Containers/UserSetupScreen';
import BusinessSetUp from '../Containers/BusinessSetupScreen';

const screens = {
  NewAccount: {
    screen: NewAccount,
  },
  FoodieAccount: {
    screen: UserSetup,
  },
  BusinessSetUp: {
    screen: BusinessSetUp,
  },
};

const config = {
  initialRouteName: 'NewAccount',
  defaultNavigationOptions: {
    header: props => <CommonHeader {...props} />,
    title: 'Create new',
  },
  headerMode: 'screen',
};

const stack = DismissableStackNavigator(screens, config);

export default stack;
