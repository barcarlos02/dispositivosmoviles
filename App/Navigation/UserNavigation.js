import { createStackNavigator } from 'react-navigation';

import styles from './Styles/NavigationStyles';
import UserMainScreen from '../Containers/UserMainScreen';

const screens = {
  MainScreen: {
    screen: UserMainScreen,
  },
};

const config = {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'MainScreen',
  navigationOptions: {
    headerStyle: styles.header,
  },
};

const stack = createStackNavigator(screens, config);

export default stack;
