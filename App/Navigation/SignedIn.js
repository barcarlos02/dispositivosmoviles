import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import BottomNavBar from '../Containers/BottomNavBar';
import styles from './Styles/NavigationStyles';
import { Colors } from '../Themes';

import MainStack from './MainNavigation';
import MapStack from './MapNavigation';
import CreateEatupStack from './CreateEatupNavigation';
import CreateBoostStack from './CreateBoostNavigation';
import CreateNewAccountStack from './CreateNewAccountNavigation';

const tabsScreens = {
  HomeScreen: {
    screen: MainStack,
  },
  MapScreen: {
    screen: MapStack,
  },
};

const tabsConfig = {
  // Default config for all screens
  initialRouteName: 'HomeScreen',
  tabBarOptions: {
    showLabel: false,
    activeTintColor: Colors.secondary,
    inactiveTintColor: Colors.inactive,
  },
  tabBarComponent: BottomNavBar,
};

const mainStack = createBottomTabNavigator(tabsScreens, tabsConfig);

// Create this stack as a modal, so the content appears from the bottom
const modalStack = createStackNavigator(
  {
    Main: {
      screen: mainStack,
    },
    CreateEatup: {
      screen: CreateEatupStack,
    },
    CreateBoost: {
      screen: CreateBoostStack,
    },
    CreateNewAccount: {
      screen: CreateNewAccountStack,
    },
  },
  {
    mode: 'modal',
    headerMode: 'none',
  }
);

export default modalStack;
