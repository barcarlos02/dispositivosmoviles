import { createStackNavigator } from 'react-navigation';

import styles from './Styles/NavigationStyles';
import AccountSetupScreen from '../Containers/AccountSetupScreen';
import BusinessAcceptScreen from '../Containers/BusinessAcceptScreen';
import BusinessSetupScreen from '../Containers/BusinessSetupScreen';
import UserSetupScreen from '../Containers/UserSetupScreen';

const screens = {
  AccountSetupScreen: {
    screen: AccountSetupScreen,
  },
  BusinessAcceptScreen: {
    screen: BusinessAcceptScreen,
  },
  BusinessSetupScreen: {
    screen: BusinessSetupScreen,
  },
  UserSetupScreen: {
    screen: UserSetupScreen,
  },
};

const config = {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'AccountSetupScreen',
  navigationOptions: {
    headerStyle: styles.header,
  },
};

const stack = createStackNavigator(screens, config);

export default stack;
