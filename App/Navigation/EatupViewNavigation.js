import React from 'react';
// import DismissableStackNavigator from './DismissableStackNavigator';
import { createStackNavigator } from 'react-navigation';
import styles from './Styles/NavigationStyles';
import EatupScreen from '../Containers/EatupScreen';
import CommonHeader from '../Components/CommonHeader';

const screens = {
  Eatup: {
    screen: EatupScreen,
  },
};

const config = {
  initialRouteName: 'Eatup',
  defaultNavigationOptions: {
    header: props => <CommonHeader {...props} />,
    title: 'Create new',
  },
  headerMode: 'screen',
};

const stack = createStackNavigator(screens, config);

export default stack;
