import { createStackNavigator } from 'react-navigation';

import styles from './Styles/NavigationStyles';
import IntroScreen from '../Containers/IntroScreen';

const screens = {
  IntroScreen: {
    screen: IntroScreen,
  },
};

const config = {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'IntroScreen',
  navigationOptions: {
    headerStyle: styles.header,
  },
};

const stack = createStackNavigator(screens, config);

export default stack;
