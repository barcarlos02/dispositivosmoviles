import React from 'react';

const DropdownAlertContext = React.createContext({});

export const DropdownAlertProvider = DropdownAlertContext.Provider;
export const DropdownAlertConsumer = DropdownAlertContext.Consumer;
